package fr.afpa.metier.entites;

import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of= {"id","nom","prenom","mail","tel","adresse","dateDeNaissance","role","fonction","authentification"})

public class Personne {

	int id;
	String nom;
	String prenom;
	String mail;
	String tel;
	String adresse;
	LocalDate dateDeNaissance;
	Role role;
	Fonction fonction;
	Authentification authentification;
	boolean actif;
}
