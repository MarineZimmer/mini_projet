package fr.afpa.metier.services;

import java.util.List;

import fr.afpa.dto.ServiceTypeMaterielDto;
import fr.afpa.metier.entites.TypeMateriel;

public class ServiceTypeMateriel {
	
	public List<TypeMateriel> getAll() {
		return new ServiceTypeMaterielDto().getAll();
	}

}
