package fr.afpa.constantes;

public class Parametrage {
	public static final int ADMIN_ID= 1;
	public static final int UTILISATEUR_ID= 2;
	
	public static final String URI_AUTHENTIFICATION_ADMIN= "admin";
	public static final String URI_DECONNEXION = "/gestionsalleafpa2/admin/deconnexion";
	public static final String URI_MODIFICATION = "/gestionsalleafpa2/admin/accueil/recherche/modifier";
	public static final String URI_ACCUEIL = "/gestionsalleafpa2/admin/accueil";
	public static final String URI_NEW_USER = "/gestionsalleafpa2/admin/accueil/creation";
	public static final String URI_RECHERCHE = "/gestionsalleafpa2/admin/accueil/recherche";
	public static final String URI_AUTHENTIFICATION = "/gestionsalle/authentification";
	
	
	public static final String URI_VISUALISER_SALLE = "visualisation";
	public static final String URI_MODIFIER_SALLE = "/gestionsalleafpa2/modifier";
	public static final String URI_AJOUTER_SALLE = "creation";
	public static final String URI_RESERVER_SALLE = "/gestionsalleafpa2/reserver";
	public static final String URI_RESERVER_UNE_SALLE  = "/gestionsalleafpa2/reserver/salle";
	public static final String URI_VISUALISER_UNE_SALLE = "/gestionsalleafpa2/visualiser/salle";
	
	
}
