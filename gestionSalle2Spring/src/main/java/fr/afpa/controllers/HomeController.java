package fr.afpa.controllers;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import fr.afpa.constantes.Parametrage;
import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServiceAuthentification;

/**
 * Handles requests for the application home page.
 * 
 * @param <T>
 */
@Controller
@SessionAttributes("persAuthSalle")
public class HomeController<T> {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Controller page index
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		return "index";
	}

	/**
	 * Controller page authentification generale
	 */
	/*@RequestMapping(value = "/authentification", method = RequestMethod.POST, params = { "login", "mdp" })
	public String authentificationGenerale(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp) {

		String uri;
		ServiceAuthentification servAuth = new ServiceAuthentification();
		Personne persAuth;
		uri = "index";

		// recuperation de la personne authentifier, null si l'authenfication a �chou�
		persAuth = servAuth.authentification(login, mdp);
		if (persAuth != null) { // la pers auth est un admin ou utilisateur
			uri = "visualisationSalle";
			
			model.addAttribute("persAuthSalle", persAuth);
		} else { 
			model.addAttribute("KO", "Erreur login ou mot de passe incorrects ");
			model.addAttribute("persAuthSalle", null);
		}
		return uri;
	}*/
	
	/**
	 * Controller page authentification generale
	 */
	@RequestMapping(value = "/authentification", method = RequestMethod.POST, params = { "login", "mdp" })
	public String authentificationGenerale(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "mdp") String mdp, HttpSession session) {
		System.out.println("session");
		String uri;
		ServiceAuthentification servAuth = new ServiceAuthentification();
		Personne persAuth;
		uri = "index";

		// recuperation de la personne authentifier, null si l'authenfication a �chou�
		persAuth = servAuth.authentification(login, mdp);
		if (persAuth != null) { // la pers auth est un admin ou utilisateur
			uri = "visualisationSalle";
			session.setAttribute("persAuthSalle", persAuth);
			//model.addAttribute("persAuthSalle", persAuth);
		} else { 
			model.addAttribute("KO", "Erreur login ou mot de passe incorrects ");
			//model.addAttribute("persAuthSalle", null);
		}
		return uri;
	}
	
	
	
	
	@RequestMapping(value = "/creation", method = RequestMethod.GET)
	public String creationSalleGet(Locale locale, Model model,HttpSession s) {
		System.out.println(s.getAttribute("persAuthSalle"));
		return "creationSalle";
	}
	
	@RequestMapping(value = "/visualisation", method = RequestMethod.GET)
	public String visualisationSalleGet(Locale locale, Model model) {
		return "visualisationSalle";
	}

}
