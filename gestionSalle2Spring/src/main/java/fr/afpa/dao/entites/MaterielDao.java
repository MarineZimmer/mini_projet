package fr.afpa.dao.entites;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table (name="materiel")
public class MaterielDao  {

	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "materiel_generator")
	@SequenceGenerator(name = "materiel_generator", sequenceName = "seq_materiel",allocationSize = 1)
	@Column (name="id_materiel", updatable = false, nullable = false )
	int id;
	
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="id_salle")
	SalleDao salleDao;
	
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="id_type_materiel")
	TypeMaterielDao typeMaterielDao;
	
	@Column (name="quantite", updatable = true, nullable = false )
	int quantite;

	public MaterielDao(int id, int quantite) {
		super();
		this.id = id;
		this.quantite = quantite;
	}
	
	
	

}
