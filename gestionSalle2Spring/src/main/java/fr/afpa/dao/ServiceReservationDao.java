package fr.afpa.dao;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.dao.entites.ReservationDao;
import fr.afpa.dao.persistance.ServiceSalleDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceReservationDao {

	public boolean createReservation(ReservationDao reservationDao) {
		Session s = null;
		Transaction tx=null;
		boolean retour=false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.save(reservationDao);
			tx.commit();
			retour=true;
		}catch (Exception e) {
		Logger.getLogger(ServiceSalleDao.class.getName()).log(Level.SEVERE, null, e);
			
		}finally {
			if(s!=null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceSalleDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return retour;
	}


}
