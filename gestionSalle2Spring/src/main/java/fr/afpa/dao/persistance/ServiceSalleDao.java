package fr.afpa.dao.persistance;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.dao.entites.AuthentificationDao;
import fr.afpa.dao.entites.MaterielDao;
import fr.afpa.dao.entites.ReservationDao;
import fr.afpa.dao.entites.SalleDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceSalleDao {
	
	
	public boolean createSalle(SalleDao salleDao) {
		Session s = null;
		Transaction tx=null;
		boolean retour=false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			salleDao.getListeMaterielDao().forEach(s::save);
			s.save(salleDao);
			tx.commit();
			retour=true;
		}catch (Exception e) {
		Logger.getLogger(ServiceSalleDao.class.getName()).log(Level.SEVERE, null, e);
			
		}finally {
			if(s!=null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceSalleDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return retour;
	}
	
	
	
	
	
	
	
	
	

	public List<SalleDao> getAllSalles() {
		Session s = null;
		List<SalleDao> listeSalleDao = null;
		List<SalleDao> retour = new ArrayList<SalleDao>();

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("getAllSalle");
			if (q != null) {
				listeSalleDao = q.list();
			listeSalleDao.forEach(e -> {
				SalleDao salle= new SalleDao(e.getIdSalle(), e.getNumero(), e.getNom(), e.getSurface(), e.getCapacite(), e.getEtage(), e.isActif());
				
				List<MaterielDao> liste = new  ArrayList<MaterielDao>();
				
				e.getListeMaterielDao().forEach(m -> {
					MaterielDao ma = new MaterielDao(m.getId(), m.getQuantite());
					//ma.setSalleDao(salle);
					ma.setTypeMaterielDao(m.getTypeMaterielDao());
					liste.add(ma);
				});
				
				List<ReservationDao> listeResa = e.getListeReservationDao().stream().map(r -> new ReservationDao(r.getIdReservation(),r.getDateDebut(),r.getDateFin(),r.getNomReservation())).collect(Collectors.toList());
				
				salle.setListeMaterielDao(liste);
				salle.setListeReservationDao(listeResa);
				salle.setBatimentDao(e.getBatimentDao());
				salle.setTypeSalleDao(e.getTypeSalleDao());
			//	salle.setListeReservationDao(e.getListeReservationDao());
				retour.add(salle);
			});
			}
			
			//return listeSalleDao;
		} catch (Exception e) {
			Logger.getLogger(ServiceSalleDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceSalleDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return retour;
	}










	public SalleDao getSalle(int id) {
		Session s = null;
		SalleDao salleDao = null;
		SalleDao salle=null;
		
		try {
			s = HibernateUtils.getSession();
			Query q = s.getNamedQuery("getSalleId");
			q.setParameter("id", id);
			salleDao = (SalleDao) q.getSingleResult();
			 salle= new SalleDao(salleDao.getIdSalle(), salleDao.getNumero(), salleDao.getNom(), salleDao.getSurface(), salleDao.getCapacite(), salleDao.getEtage(), salleDao.isActif());
			
			List<MaterielDao> liste = new  ArrayList<MaterielDao>();
			
			salleDao.getListeMaterielDao().forEach(m -> {
				MaterielDao ma = new MaterielDao(m.getId(), m.getQuantite());
				//ma.setSalleDao(salle);
				ma.setTypeMaterielDao(m.getTypeMaterielDao());
				liste.add(ma);
			});
			
			List<ReservationDao> listeResa = salleDao.getListeReservationDao().stream().map(r -> new ReservationDao(r.getIdReservation(),r.getDateDebut(),r.getDateFin(),r.getNomReservation())).collect(Collectors.toList());
			
			salle.setListeMaterielDao(liste);
			salle.setListeReservationDao(listeResa);
			salle.setBatimentDao(salleDao.getBatimentDao());
			salle.setTypeSalleDao(salleDao.getTypeSalleDao());
			
		}catch (Exception e) {
		Logger.getLogger(ServiceAuthentificationDao.class.getName()).log(Level.SEVERE, null, e);
			
		}finally {
			if(s!=null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceAuthentificationDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return salle;
	}

	
}
