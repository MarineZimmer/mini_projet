package fr.afpa.dto;

import fr.afpa.dao.entites.PersonneDao;
import fr.afpa.dao.persistance.ServicePersonneDao;
import fr.afpa.metier.entites.Personne;

public class ServicePersonneDto {

	
	/**
	 * lien entre le service de suppression d'une personne et le service Dao qui supprime la personne
	 * @param personne : une entite metier personne
	* @return : true si la suppresion à été effectué, false sinon
	 */
	public boolean deletePersonne(Personne personne) {
		return new ServicePersonneDao().deletePersonne(personneMetierToPersonneDao(personne));
	}
	
	
	/**
	 * Lien entre le service de modification d'une personne et le service dao qui va 
	 * mettre à jour la personne Dao dans la base de données
	 * @param personne : l'entite metier personne initialisé auparavant
	 * @return : true si l'update s'est à été effectué, false sinon
	 */
	public boolean updatePersonne(Personne personne) {
		return new ServicePersonneDao().updatePerson(personneMetierToPersonneDao(personne));
	}
	
	
	
	/**
	 * méthode qui permet de transformer une entité personne metier en entité
	 * personneDao
	 * 
	 * @param personne : entité personne metier à transformer
	 * @return : une entité personneDao correspondante à l'entite personne metier
	 */
	public static PersonneDao personneMetierToPersonneDao(Personne personne) {
		PersonneDao personneDao = new PersonneDao();
		personneDao.setId(personne.getId());
		personneDao.setNom(personne.getNom());
		personneDao.setActif(personne.isActif());
		personneDao.setAdresse(personne.getAdresse());
		personneDao.setAuthentification(ServiceAuthentificationDto.authMetierToAuthDao(personne.getAuthentification()));
		personneDao.setDateDeNaissance(personne.getDateDeNaissance());
		personneDao.setFonction(ServiceFonctionDto.fonctionMetierToFonctionDao(personne.getFonction()));
		personneDao.setMail(personne.getMail());
		personneDao.setPrenom(personne.getPrenom());
		personneDao.setRole(ServiceRoleDto.roleMetierToRoleDao(personne.getRole()));
		personneDao.setTel(personne.getTel());
		return personneDao;
	}

	/**
	 * méthode qui permet de transformer une entité personneDao en entité personne
	 * metier
	 * 
	 * @param personneDao : entité personneDao à transformer
	 * @return : une entité personne métier correspondante à l'entité personneDao
	 */
	public static Personne personneDaoToPersonneMetier(PersonneDao personneDao) {
		Personne personne = new Personne();
		personne.setId(personneDao.getId());
		personne.setNom(personneDao.getNom());
		personne.setActif(personneDao.isActif());
		personne.setAdresse(personneDao.getAdresse());
		personne.setAuthentification(ServiceAuthentificationDto.authDaoToAuthMetier(personneDao.getAuthentification()));
		personne.setDateDeNaissance(personneDao.getDateDeNaissance());
		personne.setFonction(ServiceFonctionDto.fonctionDaoToFonctionMetier(personneDao.getFonction()));
		personne.setMail(personneDao.getMail());
		personne.setPrenom(personneDao.getPrenom());
		personne.setRole(ServiceRoleDto.roleDaoToRoleMetier(personneDao.getRole()));
		personne.setTel(personneDao.getTel());

		return personne;
	}

	/**
	 * Lien entre le service de création d'une personne et le service dao qui va 
	 * créé la personne Dao dans la base de données
	 * @param personne
	 * @return
	 */
	public boolean savePersonne(Personne personne) {
		PersonneDao personneDao = personneMetierToPersonneDao(personne);
		return new ServicePersonneDao().savePersonne(personneDao);
		
	}
	
	

}
