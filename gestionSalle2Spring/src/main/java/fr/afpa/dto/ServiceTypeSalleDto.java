package fr.afpa.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.afpa.dao.entites.BatimentDao;
import fr.afpa.dao.entites.SalleDao;
import fr.afpa.dao.entites.TypeSalleDao;
import fr.afpa.dao.persistance.ServiceBatimentDao;
import fr.afpa.dao.persistance.ServiceTypeSalleDao;
import fr.afpa.metier.entites.Batiment;
import fr.afpa.metier.entites.TypeSalle;

public class ServiceTypeSalleDto {
	
	
	public List<TypeSalle> getAll() {
		List<TypeSalleDao> listeTypeSalleDao = new ServiceTypeSalleDao().getAll();
		List<TypeSalle> listeTypeSalle = listeTypeSalleDao.stream().map(ServiceTypeSalleDto::typeSalleDaoToTypeSalle).collect(Collectors.toList());
		return listeTypeSalle;
	}

	public static TypeSalle typeSalleDaoToTypeSalle(TypeSalleDao typeSalleDao) {
		return new TypeSalle(typeSalleDao.getId(), typeSalleDao.getLibelle());

	}

	public static TypeSalleDao typeSalleToTypeSalleDao(TypeSalle typeSalle) {
		return new TypeSalleDao(typeSalle.getId(), typeSalle.getLibelle(), new ArrayList<SalleDao>());

	}

	
		
}
