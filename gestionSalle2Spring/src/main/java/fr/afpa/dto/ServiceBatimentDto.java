package fr.afpa.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.afpa.dao.entites.BatimentDao;
import fr.afpa.dao.entites.SalleDao;
import fr.afpa.dao.persistance.ServiceBatimentDao;
import fr.afpa.metier.entites.Batiment;

public class ServiceBatimentDto {

	public List<Batiment> getAll() {
		List<BatimentDao> listeBatimentDao = new ServiceBatimentDao().getAll();
		List<Batiment> listeBatiment = listeBatimentDao.stream().map(ServiceBatimentDto::batimentDaoToBatiment).collect(Collectors.toList());
		return listeBatiment;
	}
	
	
	
	
	
	
	public static Batiment batimentDaoToBatiment(BatimentDao batimentDao) {
		Batiment batiment = new Batiment();
		batiment.setId(batimentDao.getId());
		batiment.setNom(batimentDao.getNom());

		// List<Salle> listeSalle =
		// batimentDao.getListeSalle().stream().map(ServiceSalleDto::salleDaoToSalle).collect(Collectors.toList());
		// batiment.setListeSalle(listeSalle );

		return batiment;

	}

	public static BatimentDao batimentToBatimentDao(Batiment batiment) {
		BatimentDao batimentDao = new BatimentDao();
		batimentDao.setId(batiment.getId());
		batimentDao.setNom(batiment.getNom());

		// List<SalleDao> listeSalleDao =
		// batiment.getListeSalle().stream().map(ServiceSalleDto::salleToSalleDao).collect(Collectors.toList());
		List<SalleDao> listeSalleDao = new ArrayList<SalleDao>();
		batimentDao.setListeSalle(listeSalleDao);

		return batimentDao;

	}

	
}
