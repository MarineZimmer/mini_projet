package fr.afpa.dto;

import fr.afpa.dao.entites.MaterielDao;
import fr.afpa.metier.entites.Materiel;

public class ServiceMaterielDto {

	public static Materiel materielDaoToMateriel(MaterielDao materielDao) {
		Materiel materiel = new Materiel(materielDao.getId(), materielDao.getQuantite());
		//materiel.setSalle(ServiceSalleDto.salleDaoToSalle(materielDao.getSalleDao()));// boucle infini stream
		materiel.setTypeMateriel(
				ServiceTypeMaterielDto.typeMaterielDaoToTypeMateriel(materielDao.getTypeMaterielDao()));

		return materiel;
	}

	public static MaterielDao materielToMaterielDao(Materiel materiel) {
		MaterielDao materielDao = new MaterielDao(materiel.getId(), materiel.getQuantite());
		//materielDao.setSalleDao(ServiceSalleDto.salleToSalleDao(materiel.getSalle()));
		materielDao
				.setTypeMaterielDao(ServiceTypeMaterielDto.typeMaterielToTypeMaterielDao(materiel.getTypeMateriel()));
		return materielDao;
	}

}
