<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.constantes.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="resources/css/cssVisualisation.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Visualisation</title>
</head>
<body>
<label id="labelInfo">Bonjour ${sessionScope.persAuthSalle.prenom } ${sessionScope.persAuthSalle.nom}</label>
	<c:choose>
		<c:when test="${not empty sessionScope.persAuthSalle  }">
			<nav>
				<ul>
					<li><a href="${ Parametrage.URI_DECONNEXION}">Se
							deconnecter</a></li>
					<li><a class="courant"
						href="${ Parametrage.URI_VISUALISER_SALLE}"> Visualisation
							salle </a></li>
					<c:if
						test="${sessionScope.persAuthSalle.role.id eq Parametrage.ADMIN_ID }">
						</li>
						<li><a href="${ Parametrage.URI_RESERVER_SALLE}">
								Reserver salle </a></li>
						<li><a href="${ Parametrage.URI_AJOUTER_SALLE}"> Ajouter
								salle </a></li>
						<li><a href="${ Parametrage.URI_MODIFIER_SALLE}">
								Modifier salle </a></li>
					</c:if>
				</ul>
			</nav>
			<div class="container">
				<div class="colonne">
				
						<table class="colonne">

							<c:forEach var="salle" items="${listeSalle }" varStatus="status">
								<c:if test="${ status.index % 4 == 0}">
									<tr>
								</c:if>
								<td><a href="${Parametrage.URI_VISUALISER_UNE_SALLE }?id=${salle.idSalle}">${ salle.nom } <br> Batiment : ${ salle.batiment.nom },
										n� ${ salle.numero }<br> type : ${ salle.typeSalle.libelle }<br>
										etage ${ salle.etage }<br> capacit� : ${ salle.capacite }
								</a></td>
								<c:if test="${status.index+1 % 4 == 0 || status.last}">
									</tr>
								</c:if>
							</c:forEach>

						</table>
				</div>
				
				<div class="colonne2">
				<h2>Recherche</h2>
				<form method="post"  action="${ Parametrage.URI_VISUALISER_SALLE}">
				
				<!-- filtre date -->
				<div>
				<input id="cbReserve" name="date" value= "reserve" type="radio">
				<label >reserv�     </label>
				<input id="cblibre" name="date" value= "libre" type="radio">
				<label>libre      </label>
				<input id="cblibre" name="date" value= "nonFiltre" type="radio">
				<label>libre/reserv�</label>
				
				<br>
				</div>
				<div>
				<label >Date debut : </label>
				<input id="dateDebut" name="dateDebut" type="date">
				<br>
				<label >Date fin : </label>
				<input id="dateFin" name="dateFin" type="date">
				<br>
					<!-- Capacite min -->
				<label >Capacit� min : </label>
				<input id="capacite" name="capacite" type="number">
				<br>
				
				<!-- filtre type salle -->
				<c:forEach var="typeSalle" items="${listeTypeSalle }">
				<input type="checkbox" value="${typeSalle.id }" name="typeSalle">
                              <label>${typeSalle.libelle }</label>
                              </c:forEach>
                        <br>
                         <!--filtre type materiel-->
                        <c:forEach var= "typeMateriel" items="${listeTypeMateriel }">
                       
                            <label>${typeMateriel.libelle } :</label>
                             <input id ="${typeMateriel.idTypeMateriel}" name="tm${typeMateriel.idTypeMateriel}" value="" type="number" size="3">
                          <br>
                           </c:forEach>
                           
				<br>
				
				<input type="submit" value="Recherche">
				</div>
				</form>
				</div>
					
		</c:when>
		
	</c:choose>

</body>
</html>