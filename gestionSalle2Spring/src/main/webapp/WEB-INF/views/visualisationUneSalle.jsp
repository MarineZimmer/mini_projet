<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.constantes.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link
	href="http://localhost:8080/gestionsalleafpa2/css/cssVisualisationSalle.css"
	rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Visualisation</title>
</head>
<body>

<body>
	<c:choose>
		<c:when test="${not empty sessionScope.persAuthSalle  }">
			<nav>
				<ul>
					<li><a href="${ Parametrage.URI_DECONNEXION}">Se
							deconnecter</a></li>
					<li><a class="courant"
						href="${ Parametrage.URI_VISUALISER_SALLE}"> Visualisation
							salle </a></li>
					<c:if
						test="${sessionScope.persAuthSalle.role.id eq Parametrage.ADMIN_ID }">
						</li>
						<li><a href="${ Parametrage.URI_RESERVER_SALLE}">
								Reserver salle </a></li>
						<li><a href="${ Parametrage.URI_AJOUTER_SALLE}"> Ajouter
								salle </a></li>
						<li><a href="${ Parametrage.URI_MODIFIER_SALLE}">
								Modifier salle </a></li>
					</c:if>
				</ul>
			</nav>
			<div class="container">
		
		<!--Ligne UNE-->
		<div class="ligne">
			<form action="gestion" method="post">
				<!--COLONNE GAUCHE-->
				<div class="colonne">
					<!--partie nom-->
					<div class="colonneGauche">
						<label>Nom de la salle:</label>
					</div>
					<div class="colonneDroite">
						<label name="nom">${salle.nom }</label>
					</div>
					<!--partie prenom-->
					<div class="colonneGauche">
						<label>Type de salle :</label>
					</div>
					<div class="colonneDroite">
						<label prenom="prenom">${salle.typeSalle.libelle }</label>
					</div>
					<!--partie mail-->
					<div class="colonneGauche">
						<label>Batiment :</label>
					</div>
					<div class="colonneDroite">
						<label mail="mail">${salle.batiment.nom }</label>
					</div>
					<!--partie telephone-->
					<div class="colonneGauche">
						<label>Numero :</label>
					</div>
					<div class="colonneDroite">
						<label name="telephone">${salle.numero }</label>
					</div>
					<!--partie adresse-->
					<div class="colonneGauche">
						<label>Etage :</label>
					</div>
					<div class="colonneDroite">
						<label name="adresse">${salle.etage }</label>
					</div>
					<!--partie telephone-->
					<div class="colonneGauche">
						<label>Capacite :</label>
					</div>
					<div class="colonneDroite">
						<label name="telephone">${salle.capacite } personnes</label>
					</div>
					<!--partie telephone-->
					<div class="colonneGauche">
						<label>Superficie :</label>
					</div>
					<div class="colonneDroite">
						<label name="telephone">${salle.surface } m�tres carr�es</label>
					</div>
					<!--partie date-->
					<div class="colonneGauche">
						<label for="naissance">Actif :</label>
					</div>
					<div class="colonneDroite">
						<label name="date">${salle.actif }</label>
					</div>
				</div>
				<!--COLONNE DROITE-->
				<div class="colonne">
					<!--partie login-->
					 <div class="colonneGauche">
						<label>liste mat�riaux </label>
					</div>
					<div class="colonneDroite">
						<label name="login"> : </label>
					</div>
					<c:forEach var= "materiel" items="${salle.listeMateriel }">
                       <div class="colonneGauche">
						<label>${materiel.typeMateriel.libelle } :</label>
					</div>
					<div class="colonneDroite">
						<label name="login">${materiel.quantite }</label>
					</div>
                           
                           </c:forEach>
                            <div class="colonneGauche">
						<label>liste r�servations</label>
					</div>
					<div class="colonneDroite">
						<label name="login"> :</label>
					</div>
                           <c:forEach var= "reserv" items="${salle.listeReservation }">
                       <div class="colonneGauche">
						<label>${reserv.nomReservation } :</label>
					</div>
					<div class="colonneDroite">
						<label name="login">du ${reserv.dateDebut } au ${reserv.dateFin }</label>
					</div>
                           
                           </c:forEach>
					
					
					
					
					
	

					


					
				</div>
			</form>
		</div>
	</div>
				
					
		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>

</body>
</html>