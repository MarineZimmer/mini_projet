<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <%@page import="fr.afpa.constantes.Parametrage"%>
  <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>admin</title>
<link href ="http://localhost:8080/gestionsalleafpa2/css/cssPageAccueilChoix.css" rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when
			test="${sessionScope.persAuth.role.id eq Parametrage.ADMIN_ID }">
<flex-container>
          
            <form action="${Parametrage.URI_ACCUEIL}">
                <label for="message">${message }</label>
                <br> 
                
             
              
                <br> 
                <br> 
                <br> 
                <button type="submit" value="ValiderB" >Retour page accueil</button>
            </form>
 </flex-container>
		</c:when>
		<c:otherwise>
			<c:redirect url="${ Parametrage.URI_AUTHENTIFICATION_ADMIN }"></c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>