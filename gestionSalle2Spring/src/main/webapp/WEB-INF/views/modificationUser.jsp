<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"  %>
<%@page import="fr.afpa.constantes.Parametrage"%>
<%@page import="fr.afpa.metier.entites.Personne"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modification</title>
<link href ="http://localhost:8080/gestionsalleafpa2/css/cssUser.css" rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when
			test="${sessionScope.persAuth.role.id eq Parametrage.ADMIN_ID }">


   <div class="container">
            <div class="entete">
                <!--Lien pour la deconnexion ici-->
               
                <a href="<c:url value="${ Parametrage.URI_DECONNEXION} " />">Se deconnecter</a>
                <h2>Modification de l'utilisateur</h2>
         
            </div>
            <div class="ligne">
                <!--Debut de FORM -->  
                               
                <form action="${ Parametrage.URI_MODIFICATION}" method="post">
                    <!--COLONNE GAUCHE-->
                    <div class="colonne">
                        <!--partie nom-->
                        <div class="colonneGauche"> 
                            <label>Nom :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="nom" type="text" value = "${pers.nom}" required> 
                        </div>
                        <!--partie prenom-->
                        <div class="colonneGauche">
                            <label>Prenom :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="prenom" type="text" value = "${pers.prenom}" required> 
                        </div>
                        <!--partie mail-->
                        <div class="colonneGauche">
                            <label>Mail :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="mail" type="text" value ="${pers.mail}" required> 
                        </div>
                        <!--partie telephone-->
                        <div class="colonneGauche">
                            <label>Telephone :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="telephone" type="text" value = "${pers.tel}" required> 
                        </div>
                        <!--partie adresse-->
                        <div class="colonneGauche">
                            <label>Adresse :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="adresse" type="text" value="${pers.adresse}"required> 
                        </div>
                        <!--partie date-->
                        <div class="colonneGauche">
                            <label for="naissance">Date de naissance :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input type="date" id="naissance" name="date"  value="${pers.dateDeNaissance}" min="1900-01-01" max="" required>
                        </div>
                    </div>
                    <!--COLONNE DROITE-->
                    <div class="colonne">
                        <!--partie login-->
                        <div class="colonneGauche"> 
                            <label>Login :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <label>${pers.authentification.login}</label>
                        </div>
                        <!--partie mot de passe-->
                        <div class="colonneGauche"> 
                            <label>Mot de passe :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="mdp" type="text" required value="${pers.authentification.mdp}"> 
                        </div>
                        <!--partie role-->
                        <div class="colonneGauche"> 
                            <label>Role :</label>
                        </div>
                        
                        <!-- Script ICI POUR SELECTIONNER LE BON ROLE -->
                        <div class="colonneDroite"> 
                        
                       
                            <input type="radio" name="role" value="1"  <c:if test="${pers.role.id eq Parametrage.ADMIN_ID }"> checked </c:if>  >
                            <label>administrateur</label>
                            <br>
                            <input type="radio" name="role" value="2"  <c:if test="${pers.role.id eq Parametrage.UTILISATEUR_ID }"> checked </c:if> >
                            <label>utilisateur simple</label>
                            <br>
                        </div>
                             <div class="colonneGauche"> 
                            <label>Actif :</label>
                        </div>
                        
                         <!-- Script ICI POUR SELECTIONNER si personne active -->
                        <div class="colonneDroite"> 
                            <input type="radio" name="actif" value="true" <c:if test="${pers.actif }"> checked </c:if>>
                            <label>oui</label>
                            <br>
                            <input type="radio" name="actif" value="false" <c:if test="${!pers.actif }"> checked </c:if>>
                            <label>non</label>
                            <br>
                        </div>
                        
                        <!--partie Fonction a voir comment integrer d'autres fonction via java -->
                        <div class="colonneGauche"> 
                            <label>Fonction :</label>
                        </div>
                        <div class="colonneDroite"> 
                        <!-- faire boucle recherche bdd -->
                            <select name="fonction" id="selectFonction" required>
                                <option value="${pers.fonction.id}">${pers.fonction.libelle}</option>
                                <option value="1">Directeur</option>
                                <option value="4">Secretaire</option>
                                <option value="2">Formatteur</option>
                                <option value="3">Stagiaire</option>
                                <option value="6">Intervenant</option>
                                <option value="5">Visiteur</option>
                            </select>                             
                        </div>
                        
                        <!--colonnes vides pour le positionnement-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <label></label>
                        </div>
                        <!--partie BOUTTON VALIDER-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <button type="submit" name="log" value="${pers.authentification.login}">Valider</button>
                        </div>
                        <!--partie BOUTTON ANNULER retour vers l'accueil � integrer-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                     <a href="recherche"> <button type="button" value="Annuler">Annuler</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
		</c:when>
		<c:otherwise>
			<c:redirect url="${ Parametrage.URI_AUTHENTIFICATION_ADMIN }"></c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>