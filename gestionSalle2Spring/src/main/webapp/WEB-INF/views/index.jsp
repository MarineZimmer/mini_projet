<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"  %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="fr.afpa.constantes.Parametrage"%>
<html>
 <title>Connexion</title>
        <link href="resources/css/cssConnexionGenerale.css" rel="stylesheet">
<body>
 <flex-container>
            <label id="labelInfo">Connexion</label>
            
             <em class="erreur"> ${KO}</em>
            <form action = "${Parametrage.URI_AUTHENTIFICATION}" method="post" style="">
                <label for="LoginLabel">Login :</label>
                <br> 
                <input name="login" type="text"/> 
                <br> 
                <br> 
                <br> 
                <label for="MdpLabel">Mot de passe :</label>
                <br> 
                <input name="mdp" type="password"/> 
                <br> 
                <br> 
                <br> 
                <button type="submit" value="ValiderB">Valider</button>
            </form>
            <a href="${ Parametrage.URI_AUTHENTIFICATION_ADMIN}">Connexion administrateur</a>
  </flex-container>
</body>
</html>
