<%@page import="fr.afpa.metier.entites.Personne"%>
<%@page import="fr.afpa.constantes.Parametrage"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"  %>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Recherche Utilisateur</title>
<link
	href="http://localhost:8080/gestionsalleafpa2/css/cssRecherche.css"
	rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when
			test="${sessionScope.persAuth.role.id eq Parametrage.ADMIN_ID }">
   <div class="container">
  
   
             <a href="${ Parametrage.URI_DECONNEXION}">Se deconnecter</a>
            <a href="${ Parametrage.URI_ACCUEIL}">| Retour vers la page d'accueil |</a>
            
            <h2> Rechercher Utilisateur </h2>
            
            <form action="${Parametrage.URI_RECHERCHE}" method="post">
            
                <input type="text" class="champRecherche" list="users" name= "login" required>
                
                <datalist id="users">
                
      				
                    <c:forEach items="${listePers }" var="personne">
                    <option value="${personne.authentification.login }">${personne.nom } ${personne.prenom }, login : ${personne.authentification.login }, ${personne.fonction.libelle }, ${personne.role.libelle }</option>
	
                    </c:forEach>
                    
                    
                </datalist>
                
                <button type="submit" class="searchButton" value="Rechercher">
                Voir
                </button>
                
            </form>
        </div>
		</c:when>
		<c:otherwise>
			<c:redirect url="${ Parametrage.URI_AUTHENTIFICATION_ADMIN }"></c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>