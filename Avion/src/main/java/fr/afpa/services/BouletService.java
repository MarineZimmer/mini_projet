package fr.afpa.services;

import java.awt.Rectangle;
import java.util.Iterator;

import fr.afpa.controle.ControleVie;
import fr.afpa.entites.Boulet;
import fr.afpa.entites.Meteorite;
import fr.afpa.ihms.FenetrePrincipale;

public class BouletService implements Runnable {

	private FenetrePrincipale fenetre;

	public BouletService(FenetrePrincipale fenetre) {
		super();
		this.fenetre = fenetre;
	}

	@Override
	public void run() {
		while (ControleVie.estVivant(fenetre.getAvion())) {
			synchronized (fenetre) {
				Iterator<Boulet> it = fenetre.getListeBoulet().iterator();
				while (it.hasNext()) {
					Boulet boulet = (Boulet) it.next();
					DeplacementService.deplacer(fenetre, boulet, 0, -5 * 2);
					if (boulet.getY() < 0 || collisionMeteorite(boulet.getBounds())) {

						fenetre.getJPanelFond().remove(boulet);
						it.remove();
					}

				}
			}
			try {
				Thread.sleep(0, fenetre.getJPanelFond().getVitesseJeu() * 2);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private boolean collisionMeteorite(Rectangle boulet) {
		Iterator<Meteorite> it = fenetre.getListeMeteorites().iterator();
		while (it.hasNext()) {
			Meteorite meteorite = (Meteorite) it.next();
			if (boulet.intersects(meteorite.getBounds())) {
				meteorite.setLocation(0, 1000);
				return true;
			}

		}
		return false;
	}

}
