package fr.afpa.services;

import java.awt.Color;
import java.util.Random;

import fr.afpa.controle.ControleVie;
import fr.afpa.entites.Meteorite;
import fr.afpa.entites.MeteoriteBase;
import fr.afpa.entites.MeteoriteFeu;
import fr.afpa.entites.MeteoriteGlace;
import fr.afpa.entites.MeteoriteIceberg;
import fr.afpa.entites.MeteoriteZigZag;
import fr.afpa.ihms.FenetrePrincipale;

public class CreationMeteoriteService implements Runnable {

	private FenetrePrincipale fenetre;

	public CreationMeteoriteService(FenetrePrincipale fenetre) {
		super();
		this.fenetre = fenetre;
	}

	/**
	 * service de creation d'une meteorite aléatoire
	 * 
	 * @return la météorite créé
	 */
	public Meteorite creationMeteorite() {
		Meteorite meteorite = null;
		switch (new Random().nextInt(5)) {
		case 0:
			meteorite = new MeteoriteBase("meteoriteBase", "ressources/base.jpg", 2, 1, 2, 20, 0, 1);
			meteorite.setBackground(Color.DARK_GRAY);
			break;
		case 1:
			meteorite = new MeteoriteFeu("meteoriteFeu", "ressources/feu.jpg", 1, 2, 1, 30, 0, 1);
			meteorite.setBackground(Color.RED);
			break;
		case 2:
			meteorite = new MeteoriteGlace("meteoriteGlace", "ressources/glace.jpg", 2, 2, 3, 25, 0, 1);
			meteorite.setBackground(Color.BLUE);
			break;
		case 3:
			meteorite = new MeteoriteZigZag("meteoriteZZ", "ressources/image.png", 1, 2, 5, 30, 1, 2);// metre dy à 1
			meteorite.setBackground(Color.YELLOW);
			break;
		case 4:
			meteorite = new MeteoriteIceberg("meteoriteIceberg", "ressources/iceberg.jpg", 2, 4, 5, 50, 0, 1);
			meteorite.setBackground(Color.CYAN);
			break;

		default:
			break;
		}
		return meteorite;
	}

	/**
	 * Création d'une nouvelle météorite si le nombre max de météorites n'est pas
	 * atteint
	 * 
	 * @param fenetre : la fenetre principale
	 * @param nb      : le nombre max de météorites
	 */
	public void creationNbMeteorite(int nb) {

		synchronized (fenetre) {
			if (fenetre.getListeMeteorites().size() < nb) {
				Meteorite meteorite = creationMeteorite();
				// ajout de la météorite à une position aléatoire en haut
				meteorite.setLocation(new Random().nextInt(fenetre.getWidth() - meteorite.getWidth() - 3), 0);
				// ajout de météorite au jpanel
				fenetre.getJPanelFond().add(meteorite);
				// ajout de la météorite à la liste de météorite
				fenetre.getListeMeteorites().add(meteorite);
			}
		}
	}

	@Override
	public void run() {
		while (ControleVie.estVivant(fenetre.getAvion())) {
			creationNbMeteorite(3);
			try {
				Thread.sleep(fenetre.getJPanelFond().getVitesseJeu()*5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("ERRERER");
				e.printStackTrace();
			}
		}
		System.out.println("fin");
	}

}
