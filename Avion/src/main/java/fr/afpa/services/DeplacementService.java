package fr.afpa.services;

import javax.swing.JPanel;

import fr.afpa.controle.ControleBordures;
import fr.afpa.entites.MeteoriteZigZag;
import fr.afpa.ihms.FenetrePrincipale;

public class DeplacementService {

	/**
	 * Permet de bouger la position d'un JPanel 
	 * @param fenetre : la fenetre qui contient le JPanel
	 * @param JPanel : le panel a bouger
	 * @param dx : l'abscisse a ajouter
	 * @param dy : l'ordonnee a ajouter
	 */
	public static void deplacer(FenetrePrincipale fenetre, JPanel panel, int dx, int dy) {
		
		if(panel  instanceof MeteoriteZigZag) {
			if(((MeteoriteZigZag)panel).isDeplacement()) {
				dx = -dx;
		}
			if(((MeteoriteZigZag) panel).getDep()%100==50) {
					((MeteoriteZigZag)panel).setDeplacement(!((MeteoriteZigZag)panel).isDeplacement());
			}
			((MeteoriteZigZag) panel).setDep(((MeteoriteZigZag) panel).getDep()+1);
			
		}
		if (ControleBordures.dansLeCadre(fenetre, panel, panel.getX() + dx, panel.getY() + dy)) {
			panel.setLocation(panel.getX() + dx, panel.getY() + dy);
		}
	}	
	
	
}
