package fr.afpa.services;

import fr.afpa.entites.Meteorite;
import fr.afpa.entites.Score;
import fr.afpa.ihms.FenetrePrincipale;

public class ScoreService {

	private static final int NB_POINTS_MAX = 999;
	
	/**
	 * Permet d'ajouter au score, les points d'une meteorite
	 * @param fenetre : la fenetre du jeu ou se trouve la meteorite et le score
	 * @param score : le score du joueur
	 * @param meteorite : la meteorite dont on recupere les points
	 * @return true si on a ajoute des points et false sinon
	 */
	public boolean ajoutPointsScores(FenetrePrincipale fenetre, Score score, Meteorite meteorite) {
		if (meteorite.isMeteoritePoint() && score.getScorePoints()+meteorite.getNbrPoint()<=NB_POINTS_MAX) {
			score.setScorePoints(score.getScorePoints() + meteorite.getNbrPoint());
			return true;
		}
		else if (meteorite.isMeteoritePoint() && score.getScorePoints()+meteorite.getNbrPoint()>NB_POINTS_MAX) {
			score.setScorePoints(NB_POINTS_MAX);
			return true;
		}
		else {
			return false;
		}
	}
	
}
