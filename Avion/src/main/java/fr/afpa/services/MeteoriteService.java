package fr.afpa.services;


import fr.afpa.controle.ControleVie;
import fr.afpa.entites.Avion;
import fr.afpa.entites.Meteorite;
import fr.afpa.ihms.FenetrePrincipale;

public class MeteoriteService {

	public boolean collision(Avion avion, Meteorite meteorite, FenetrePrincipale fenetre) {

		boolean coll = false;

		int avionX1 = avion.getX();
		int avionX2 = (avionX1 + avion.getWidth());
		int avionY1 = avion.getY();
		int avionY2 = (avionY1 + avion.getHeight());
		int meteoriteX1 = meteorite.getX();
		int meteoriteX2 = meteoriteX1 + meteorite.getWidth();
		int meteoriteY1 = meteorite.getY();
		int meteoriteY2 = meteoriteY1 + meteorite.getHeight();

		/*if (avionX1 <= meteoriteX1 && meteoriteX1 < avionX2 || (avionY1 <= meteoriteY1 && meteoriteY1 < avionY2)
				|| (avionX1 <= meteoriteX2 && meteoriteX2 < avionX2)
				|| (avionY1 <= meteoriteY1 && meteoriteY1 < avionY2)
				|| (avionX1 <= meteoriteX1 && meteoriteX1 < avionX2)
				|| (avionY1 <= meteoriteY2 && meteoriteY2 < avionY2)
				|| (avionX1 <= meteoriteX2 && meteoriteX2 < avionX2)
				|| (avionY1 <= meteoriteY2 && meteoriteY2 < avionY2)) {*/
		if(avion.getBounds().intersects(meteorite.getBounds())) {

			//System.out.println("YOU ARE DEAD");
			avion.setPv(avion.getPv() - meteorite.getDegatMeteorite());
			fenetre.getJLabelVie().setText("Vie : "+ avion.getPv());
			avion.setImageEnCours(avion.getExplosion1());
			meteorite.setMeteoritePoint(false);
			meteorite.setLocation(0, 1000);
			fenetre.repaint();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return false;
			}
			avion.setImageEnCours(avion.getAvionCentre());
			fenetre.repaint();
			return true;
		}
		return false;
	}
}
