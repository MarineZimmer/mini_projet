package fr.afpa.ihms;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import fr.afpa.controle.ControleVie;
import fr.afpa.services.MeteoriteService;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JPanelImageFond extends JPanel implements Runnable {

	private BufferedImage imageFond;
	private int hauteurSousImage;
	private int vitesseJeu;
	private FenetrePrincipale fenetre;
	private int pasIncrementation;

	public JPanelImageFond(String fileName) {
		setLayout(null);
		try {
			imageFond = ImageIO.read(new File(fileName));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(imageFond.getSubimage(0, this.getHeight() - hauteurSousImage, this.getWidth(), this.getHeight()), 0,
				0, null);
	}

	@Override
	public void run() {
		vitesseJeu = 500;
		pasIncrementation=1;
		while (ControleVie.estVivant(fenetre.getAvion())) {

			hauteurSousImage += pasIncrementation;
			synchronized (fenetre) {
				this.repaint();
			}
			
			if (fenetre.getScore().getScorePoints()/hauteurSousImage < 49) {
				vitesseJeu = 50 - fenetre.getScore().getScorePoints()/hauteurSousImage;
				hauteurSousImage += 1;
			}else {
				vitesseJeu=50;
				pasIncrementation++;
			}
			if (hauteurSousImage > 450)
				hauteurSousImage = 0;
			if (vitesseJeu > 400) {
				vitesseJeu -= 10;
			} else if (vitesseJeu > 300) {
				vitesseJeu -= 5;
			} else if (vitesseJeu > 200) {
				vitesseJeu -= 3;
			} else if (vitesseJeu > 30) {
				vitesseJeu -= 1;
			}
			try {
				Thread.sleep(0,vitesseJeu);
			} catch (InterruptedException e) {
				System.out.println(vitesseJeu);
				e.printStackTrace();
			}
		}

		System.out.println("fin");
	}
}
