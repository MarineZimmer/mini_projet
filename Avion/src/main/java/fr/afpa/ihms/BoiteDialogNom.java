package fr.afpa.ihms;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

import fr.afpa.listeners.BoutonJouerDialogNomListener;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class BoiteDialogNom extends JDialog {

	private static final long serialVersionUID = 1L;

	public static final JLabel LABEL_NOM = new JLabel("Veuillez entrer votre nom (3 a 6 caracteres et [a-zA-Z_0-9])");
	public static final JButton JOUER = new JButton("Jouer");
	
	private JLabel messageErreur;
	private JTextField nom;
	
	
	public BoiteDialogNom(FenetrePrincipale fenetre) {
		super();
		// new GridLayout(3, 1)
		this.add(LABEL_NOM);
		
		nom = new JTextField(30);
		this.add(nom);
		
		JOUER.addActionListener(new BoutonJouerDialogNomListener(fenetre, this, nom));
		this.add(JOUER);
		
		messageErreur = new JLabel("");
		this.add(messageErreur);
	}
	
	
	
	
}
