package fr.afpa.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTextField;

import fr.afpa.controle.ControleNom;
import fr.afpa.ihms.BoiteDialogNom;
import fr.afpa.ihms.FenetrePrincipale;

public class BoutonJouerDialogNomListener implements ActionListener {

	private JTextField zoneTexte;
	private FenetrePrincipale fenetre;
	private BoiteDialogNom bdn;
	
	
	public BoutonJouerDialogNomListener(FenetrePrincipale fenetre, BoiteDialogNom bdn, JTextField zoneTexte) {
		this.fenetre = fenetre;
		this.zoneTexte = zoneTexte;
		this.bdn = bdn;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (ControleNom.nomValide(this.zoneTexte.getText())) {
			this.fenetre.getJLabelNom().setText(this.zoneTexte.getText());
			this.bdn.dispose();
		}
		else {
			this.bdn.setMessageErreur(new JLabel("Veuillez entrer un nom valide ! (3 a 6 caracteres et [a-zA-Z_0-9])"));
		}
	}

}
