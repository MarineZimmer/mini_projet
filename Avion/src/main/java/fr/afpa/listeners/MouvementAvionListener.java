package fr.afpa.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import fr.afpa.controle.ControleBordures;
import fr.afpa.entites.Avion;
import fr.afpa.entites.Boulet;
import fr.afpa.ihms.FenetrePrincipale;
import fr.afpa.services.DeplacementService;

public class MouvementAvionListener implements KeyListener {

	private FenetrePrincipale fenetre;
	
	
	public MouvementAvionListener(FenetrePrincipale fenetre) {
		this.fenetre = fenetre;
	}
	
	
	@Override
	public void keyPressed(KeyEvent e) {
		int pas = fenetre.getJPanelFond().getPasIncrementation()*10;
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			DeplacementService.deplacer(this.fenetre, this.fenetre.getAvion(), -pas, 0);			
			BufferedImage image = this.fenetre.getAvion().getAvionW();
			//this.fenetre.getAvion().setImageEnCours(image);
		}
		
		if (key == KeyEvent.VK_RIGHT) {
			DeplacementService.deplacer(this.fenetre, this.fenetre.getAvion(), pas, 0);
			BufferedImage image = this.fenetre.getAvion().getAvionE();
			this.fenetre.getAvion().setImageEnCours(image);
		}
		
		if (key == KeyEvent.VK_UP) {
			DeplacementService.deplacer(this.fenetre, this.fenetre.getAvion(), 0, -pas);
			BufferedImage image = this.fenetre.getAvion().getAvionN();
			this.fenetre.getAvion().setImageEnCours(image);
		}
		
		if (key == KeyEvent.VK_DOWN) {
			DeplacementService.deplacer(this.fenetre, this.fenetre.getAvion(), 0, pas);
			BufferedImage image = this.fenetre.getAvion().getAvionS();
			this.fenetre.getAvion().setImageEnCours(image);
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode()==32 && fenetre.getListeBoulet().size()<4) {
		
		  Boulet boulet = new Boulet();
		  boulet.setSize(10, 10);
		  boulet.setLocation(this.fenetre.getAvion().getX()+10, this.fenetre.getAvion().getY()+10);
		  synchronized (fenetre) {
			  fenetre.getListeBoulet().add(boulet);
			  fenetre.getJPanelFond().add(boulet);
		}
		  
		}
		
		
		BufferedImage image = this.fenetre.getAvion().getAvionCentre();
		this.fenetre.getAvion().setImageEnCours(image);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	
	
}
