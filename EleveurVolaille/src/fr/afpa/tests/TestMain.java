package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entites.Eleveur;
import fr.afpa.ihms.Main;
import junit.framework.TestCase;

public class TestMain extends TestCase {
@Test
public void testInitialisationFerme() {
	assertEquals("nombre animaux ferme incorrect", 45, Main.initialisationFerme(new Eleveur(), 10, 15, 20));
}
}
