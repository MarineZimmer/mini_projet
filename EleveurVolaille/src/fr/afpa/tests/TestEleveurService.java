package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.services.EleveurService;
import junit.framework.TestCase;

public class TestEleveurService extends TestCase {

	private EleveurService es;
	private Eleveur eleveur;
	private Poulet poulet;
	private Paon paon;
	private Canard canard;
	
	
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		es =new EleveurService();
		eleveur = new Eleveur();
		poulet = new Poulet(2);
		Poulet.setPoidsAbattage(10);
		Poulet.setPrixKg(5);
		paon = new Paon();
		canard = new Canard(4);
		Canard.setPoidsAbattage(10);
		Canard.setPrixKg(10);
		eleveur.getListeVolailles().put(poulet.getIdentifiant(), poulet);
		eleveur.getListeVolailles().put(paon.getIdentifiant(), paon);
		eleveur.getListeVolailles().put(canard.getIdentifiant(), canard);
		
	}
	
	@Test
	public void testModifierPoidsVolaille(){
		assertTrue("modif poids poulet incorect" , es.modifierPoidsVolaille(eleveur, poulet.getIdentifiant(), 10));
		assertFalse("modif poids inconnu incorect" , es.modifierPoidsVolaille(eleveur, "000", 10));
		assertFalse("modif poids paon incorect" , es.modifierPoidsVolaille(eleveur, paon.getIdentifiant(), 10));
	
	}
	
	@Test
	public void testRendrePaon(){
		assertNotNull("rendre paon incorect" , es.rendrePaon(eleveur, paon.getIdentifiant()));
		assertNull("rendre inconnu incorect" , es.rendrePaon(eleveur, "000"));
		assertNull("rendre poulet incorect" , es.rendrePaon(eleveur, poulet.getIdentifiant()));
	}
	
	@Test
	public void testVendreVolaille(){
		assertEquals("vendre poulet incorect" ,0.0, es.vendreVolaille(eleveur, poulet.getIdentifiant()),0.1);
		assertEquals("vendre canard pas assez gros incorect",0.0 , es.vendreVolaille(eleveur, canard.getIdentifiant()),0.1);
		
		Poulet.setPoidsAbattage(1);
		Canard.setPoidsAbattage(3);
		assertEquals("vendre poulet incorect" ,10.0, es.vendreVolaille(eleveur, poulet.getIdentifiant()),0.1);
		assertEquals("vendre canard incorect",40.0 , es.vendreVolaille(eleveur, canard.getIdentifiant()),0.1);	
		
		assertEquals("vendre paon incorect",0.0 , es.vendreVolaille(eleveur, paon.getIdentifiant()),0.1);
		assertEquals("vendre inconnu incorect" ,0.0, es.vendreVolaille(eleveur, "000"),0.1);
		
	}
	
	


	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		es =null;
		eleveur = null;
		poulet=null;
		paon=null;
		canard=null;
	}



	
}
