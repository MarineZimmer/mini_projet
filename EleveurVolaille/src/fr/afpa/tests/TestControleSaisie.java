package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.controles.ControleSaisie;
import junit.framework.TestCase;

public class TestControleSaisie extends TestCase {

	@Test
	public void testSaisieInt() {
		assertTrue("saisie int incorrect", ControleSaisie.saisieInt("10", 0));
		assertFalse("saisie non int incorrect", ControleSaisie.saisieInt("a10", 0));
		assertFalse("saisie int inferireur incorrect", ControleSaisie.saisieInt("15", 20));
	}
	
	@Test
	public void testSaisieFloat() {
		assertTrue("saisie float incorrect", ControleSaisie.saisieFloatPositif("10"));
		assertFalse("saisie float int incorrect", ControleSaisie.saisieFloatPositif("a10"));
		assertFalse("saisie float inferireur incorrect", ControleSaisie.saisieFloatPositif("-10"));
	}
	
	@Test
	public void testSaisieOuiOuNon() {
		assertTrue("saisie oui incorrect", ControleSaisie.isOuiNon("oui"));
		assertTrue("saisie non int incorrect",  ControleSaisie.isOuiNon("non"));
		assertFalse("saisie autre chose incorrect",  ControleSaisie.isOuiNon("aaa"));
	}
}
