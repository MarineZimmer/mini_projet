package fr.afpa.tests;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class TestsAppli extends TestCase {

	public static TestSuite suite() {
		TestSuite suite = new TestSuite();
		suite.addTestSuite(TestEleveurService.class);
		suite.addTestSuite(TestControleSaisie.class);
		suite.addTestSuite(TestLivraisonService.class);
		suite.addTestSuite(TestVollaileService.class);
		suite.addTestSuite(TestEntites.class);
		suite.addTestSuite(TestMain.class);
		suite.addTestSuite(TestIhmMenu.class);
		return suite;
	}
	
	public void main(String[] args) {
		TestRunner.run(suite());
	}
}
