package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entites.Eleveur;
import fr.afpa.services.LivraisonService;
import junit.framework.TestCase;

public class TestLivraisonService extends TestCase{

	private LivraisonService ls;
	private Eleveur eleveur;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		ls=new LivraisonService();
		eleveur =new Eleveur();
		
	}
	@Test
	public void testLivraisonPossible() {
		assertTrue("livraison ok incorect", ls.livraisonPossible(2, 2, 2));
		assertFalse("livraison trop canard incorect", ls.livraisonPossible(6, 0, 0));
		assertFalse("livraison trop poulet incorect", ls.livraisonPossible(0, 6, 0));
		assertFalse("livraison trop paon incorect", ls.livraisonPossible(0, 0, 5));
		assertFalse("livraison trop animal incorect", ls.livraisonPossible(3, 4, 2));
	}
	
	@Test 
	public void testAjouterVolaille() {
		assertNotNull("ajout poulet  incorect", ls.ajouterVolaille(eleveur, "poulet", 10));
		assertNotNull("ajout canard  incorect", ls.ajouterVolaille(eleveur, "canard", 15));
		assertNotNull("ajout paon  incorect", ls.ajouterVolaille(eleveur, "paon", 0));
		assertNull("ajout non identifié  incorect", ls.ajouterVolaille(eleveur, "fayak", 2));
	}
	

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		ls=null;
	}

	
}
