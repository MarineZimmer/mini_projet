package fr.afpa.tests;

import org.junit.Test;


import fr.afpa.entites.Canard;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.utils.Constantes;
import junit.framework.TestCase;

public class TestEntites extends TestCase{

	
	
	@Test
	public void testEntiteCanard() {
	
		assertNotNull("constructeur par defaut canard incorrect", new Canard());
		Canard.setPoidsAbattage(10);
		Canard.setPrixKg(20);
		assertEquals("getter poids abbatage canard incorect", 10.0, Canard.getPoidsAbattage(),0.01);
		assertEquals("getter prix abbatage canard incorect", 20.0, Canard.getPrixKg(),0.01);
		
		Canard canard= new Canard(10);
		StringBuilder builder = new StringBuilder();
		builder.append("\nType : Canard");
		builder.append(Constantes.IDENTIFIANT + canard.getIdentifiant());
		builder.append("\nPoids : " + canard.getPoids());
		builder.append("\nPoids d'abattage canard : " + Canard.getPoidsAbattage());
		builder.append("\nPrix au Kg canard : " + Canard.getPrixKg());
		assertEquals("affichage info canard incorrect", builder.toString(), canard.affichageInfo());
		
	}
	
	@Test
	public void testEntitePoulet() {
		Poulet.setPoidsAbattage(10);
		Poulet.setPrixKg(20);
		assertNotNull("constructeur par defaut poulet incorrect", new Poulet());
		assertEquals("getter poids abbatage poulet incorect", 10.0, Poulet.getPoidsAbattage(),0.01);
		assertEquals("getter prix abbatage poulet incorect", 20.0, Poulet.getPrixKg(),0.01);
		
		Poulet poulet= new Poulet(10);
		StringBuilder builder = new StringBuilder();
		builder.append("\nType : Poulet");
		builder.append(Constantes.IDENTIFIANT + poulet.getIdentifiant());
		builder.append("\nPoids : " + poulet.getPoids());
		builder.append("\nPoids d'abattage poulet : " + Poulet.getPoidsAbattage());
		builder.append("\nPrix au Kg poulet : " + Poulet.getPrixKg());
		assertEquals("affichage info poulet incorrect", builder.toString(), poulet.affichageInfo());
	}
	
	@Test
	public void testEntitePaon() {
		Paon paon= new Paon();
		StringBuilder builder = new StringBuilder();
		builder.append("\nType : Paon");
		builder.append(Constantes.IDENTIFIANT + paon.getIdentifiant());
		assertEquals("affichage info paon incorrect", builder.toString(), paon.affichageInfo());
	}
	
	@Test
	public void testEntiteEleveur() {
		Eleveur eleveur = new Eleveur("Fayak");
		assertEquals("getter nom eleveur incorect", "Fayak", eleveur.getNom());
		eleveur.setNom("toto");
	}


	

}
