package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Poulet;
import fr.afpa.services.VolailleAbbatableService;
import fr.afpa.utils.Constantes;
import junit.framework.TestCase;

public class TestVollaileService extends TestCase {
	private VolailleAbbatableService vas;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		vas = new VolailleAbbatableService();
	}

	@Test
	public void testModifierPoidsOuPrixAbbatage() {
		assertTrue("modif poids poulet incorrect",
				vas.modifierPoidsOuPrixAbbatage(Constantes.POULET, Constantes.POIDS, 10));
		assertTrue("modif poids canard incorrect",
				vas.modifierPoidsOuPrixAbbatage(Constantes.CANARD, Constantes.POIDS, 10));
		assertTrue("modif prix poulet incorrect",
				vas.modifierPoidsOuPrixAbbatage(Constantes.POULET, Constantes.PRIX, 10));
		assertTrue("modif prix canard incorrect",
				vas.modifierPoidsOuPrixAbbatage(Constantes.CANARD, Constantes.PRIX, 10));
		
		
		assertFalse("modif prix canard incorrect", vas.modifierPoidsOuPrixAbbatage(Constantes.CANARD, "pri", 10));
		assertFalse("modif prix poids incorrect", vas.modifierPoidsOuPrixAbbatage(Constantes.POULET, "", 10));
		assertFalse("modif prix poids incorrect", vas.modifierPoidsOuPrixAbbatage("", "", 10));
	}

	@Test
	public void testIsAbattable() {
		Poulet.setPoidsAbattage(15);
		Canard.setPoidsAbattage(20);
	}

	@Test
	public void testprixAbattage() {
		Poulet.setPoidsAbattage(2);
		Poulet.setPrixKg(10);
		Canard.setPoidsAbattage(4);
		Canard.setPrixKg(5);
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		vas = null;
	}

}
