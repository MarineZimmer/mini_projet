package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.ihms.IhmMenu;
import junit.framework.TestCase;

public class TestIhmMenu extends TestCase {
	private IhmMenu menu;
	private Eleveur eleveur;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		menu = new IhmMenu();
		eleveur = new Eleveur();
		eleveur.getListeVolailles().put("000001", new Poulet(5));
		eleveur.getListeVolailles().put("000002", new Poulet(2));
		eleveur.getListeVolailles().put("000003", new Poulet(5));
		eleveur.getListeVolailles().put("000004", new Canard(5));
		eleveur.getListeVolailles().put("000005", new Canard(2));
		eleveur.getListeVolailles().put("000006", new Paon());
		Poulet.setPoidsAbattage(3);
		Poulet.setPrixKg(5);
		Canard.setPoidsAbattage(3);
		Canard.setPrixKg(5);

	}

	@Test
	public void testAffichageMenu() {
		StringBuilder builder = new StringBuilder();
		builder.append(" ------------------------- MENU ---------------------------------\n"
				+ " 1 : Livraison volailles" + "\n 2 : Modifier poids abattage" + "\n 3 : Modifier prix du jour"
				+ "\n 4 : Modifier poids d'une vollaile" + "\n 5 : Voir le nombre de vollaile par type"
				+ "\n 6 : Voir le total de prix des volailles abattables " + "\n 7 : Vendre une volaille "
				+ "\n 8 : Rendre un paon au parc" + "\n 9 : Affichage Etat" + "\n 0 : Quitter\n"
				+ " ----------------------------------------------------------------" + "\n\n Choix : ");
		assertEquals("affichage incorrect", builder.toString(), menu.affichageMenu());
	}

	@Test
	public void testCalculTotalVolailleAbattable() {
		assertEquals("prix total incorrect", 75.0, menu.calculTotalVolailleAbattable(eleveur), 0.01);
	}

	@Test
	public void testAffichageNbVolailleType() {
		StringBuilder builder = new StringBuilder();
		builder.append("Nombre de canard : " + "2" + "\nNombre de poulet : " + "3" + "\nNombre de paon : " + "1");
		assertEquals("affichage incorrect", builder.toString(), menu.affichageNbVolailleType(eleveur));
		
	}
	@Test
	public void testTraitementChoix() {
		assertFalse("choix 0 incorrect", menu.traitementChoix("0", null));
		assertTrue("choix 1 incorrect", menu.traitementChoix("1", null));
		assertTrue("choix 4 incorrect", menu.traitementChoix("4", null));
		assertTrue("choix 5 incorrect", menu.traitementChoix("5", null));
		assertTrue("choix 6 incorrect", menu.traitementChoix("6", null));
		assertTrue("choix 7 incorrect", menu.traitementChoix("7", null));
		assertTrue("choix 8 incorrect", menu.traitementChoix("8", null));
		assertTrue("choix 9 incorrect", menu.traitementChoix("9", null));
		assertTrue("choix 9 eleveur incorrect", menu.traitementChoix("9", eleveur));
		assertTrue("choix invalide incorrect", menu.traitementChoix("a", null));
		
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		menu = null;
	}

}
