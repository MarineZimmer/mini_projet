package fr.afpa.ihms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import fr.afpa.entites.Animal;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.ientites.IAbattable;



public final class IhmAfficherEtat {
	
	private static final int LARGEUR_MAX=40;
	/**
	 * affiche l'etat de la ferme
	 * 
	 * @param eleveur : l'eleveur possédant l'elevage
	 */
	public void afficherEtat(Eleveur eleveur) {
		if (eleveur != null) {
			List<Animal> listeCanard = new ArrayList<>();
			List<Animal> listPoulet = new ArrayList<>();
			List<Animal> listPaon = new ArrayList<>();

			StringBuilder builder = new StringBuilder();
			for (Entry<String, Animal> volaille : eleveur.getListeVolailles().entrySet()) {
				if (volaille.getValue() instanceof Poulet) {
					listPoulet.add((Poulet) volaille.getValue());
				} else if (volaille.getValue() instanceof Canard) {
					listeCanard.add((Canard) volaille.getValue());
				} else if (volaille.getValue() instanceof Paon) {
					listPaon.add((Paon) volaille.getValue());
				}
			}
			Collections.shuffle(listeCanard);
			Collections.shuffle(listPoulet);
			String canard = "C  ";
			String poulet = "P  ";
			String paon = "Pa ";
			String canardAbattable = "CC ";
			String pouletAbattable = "PP ";

			//Calcul de la largeur de l'enclos nécessaire pour que les animaux soient répartis en carré
			int nbCanardLigne = (int)Math.round(Math.sqrt(listeCanard.size()));
			int nbPouletLigne = (int)Math.round(Math.sqrt(listPoulet.size()));
			int nbPaonLigne = (int)Math.round(Math.sqrt(listPaon.size()));
			
			//Si la largeur de l'enclos des paons est plus grande que la somme des largeurs des enclos des canards et des poulets
			//augmentation de la largeur des poulets et des canards
			if(nbPaonLigne>nbCanardLigne+nbPouletLigne) {
				int augmentation= nbPaonLigne-nbCanardLigne-nbPouletLigne;
				nbCanardLigne=nbCanardLigne+augmentation/2+augmentation%2;
				nbPouletLigne=nbPouletLigne+augmentation/2;
			}
			
			//Si la largeur des enclos des canards + celle des poulets et supérieur à la largeur max
			//reduction de la largeur des englos des canard et poulets
			if(nbCanardLigne+nbPouletLigne>LARGEUR_MAX) {
				int reduction= nbCanardLigne+nbPouletLigne-LARGEUR_MAX;
				nbCanardLigne-=reduction/2+reduction%2;
				nbPouletLigne-=reduction/2;
				if(nbCanardLigne<=0) { 
					nbPouletLigne+=nbCanardLigne-1;
					nbCanardLigne=1;
				}else if(nbPouletLigne<=0) {
					nbCanardLigne+=nbPouletLigne-1;
					nbPouletLigne=1;
				}
				
			}
			
			//Creation de l'affichage de la ferme
			String ligne=ajoutLigne(nbPouletLigne+nbCanardLigne);
			builder.append(ligne);
			
			while (!(listeCanard.isEmpty() && listPoulet.isEmpty())) {
				builder.append(ajoutAnimalLigne(listeCanard, nbCanardLigne, canard, canardAbattable));
				builder.append(ajoutAnimalLigne(listPoulet, nbPouletLigne, poulet, pouletAbattable));
				builder.append("|\n");
			}
			
			builder.append(ligne);
			
			while (!listPaon.isEmpty()) {
				builder.append(ajoutAnimalLigne(listPaon, nbPouletLigne+nbCanardLigne, paon, paon));
				builder.append(" |\n");
			}
			
			builder.append(ligne);

			System.out.println(builder);
		}
	}
	
	/**
	 * Retourne une ligne de l'enclos d'un animal
	 * @param listeAnimal : liste des animaux à ajouter deans l'enclos
	 * @param nbAnimalLigne : nombre d'animaux par ligne
	 * @param animal : la representation d'un animal non abbatable
	 * @param animalAbbatable : la representation d'un animal abbatable
	 * @return une ligne représentant une ligne de l'enclos avec les  animaux dedans
	 */
	public  String ajoutAnimalLigne(List<Animal> listeAnimal, int nbAnimalLigne, String animal, String animalAbbatable) {
		StringBuilder builder = new StringBuilder();
		builder.append("|");
		for (int i = 0; i < nbAnimalLigne; i++) {
			if (!listeAnimal.isEmpty()) {
				if (listeAnimal.get(0) instanceof IAbattable && ((IAbattable)listeAnimal.get(0)).isAbattable()) {
					builder.append(animalAbbatable);
				} else {
					builder.append(animal);
				}
				listeAnimal.remove(0);
			} else {
				builder.append("   ");
			}
		}
		
		return builder.toString();
	}
	
	/**
	 * ajout d'une ligne delimitatrice 
	 * @param nbCaractere : le nombre de caractères que comporte la ligne
	 * @return : la ligne delimitatrice
	 */
	public String  ajoutLigne(int nbCaractere) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < nbCaractere + 1; i++) {
			builder.append("___");
		}
		builder.append("\n");
	return builder.toString();
	}
	

}
