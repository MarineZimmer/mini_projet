package fr.afpa.ihms;

import java.util.Scanner;
import java.util.Map.Entry;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.ientites.IAbattable;
import fr.afpa.controles.ControleSaisie;
import fr.afpa.entites.Animal;
import fr.afpa.services.LivraisonService;
import fr.afpa.services.EleveurService;
import fr.afpa.services.VolailleAbbatableService;
import fr.afpa.utils.Constantes;

public final class IhmMenu {

	/**
	 * run de l'application
	 * 
	 * @param eleveur : l'eleveur possédant l'elevage
	 */
	public void run(Eleveur eleveur) {
		String choix;

		do {
			System.out.println(affichageMenu());
			choix = choixEleveur();
		} while (traitementChoix(choix, eleveur));
	}

	/**
	 * Affichage du Menu de l'Eleveur
	 *  @return le menu à afficher
	 */
	public String affichageMenu() {
		StringBuilder builder = new StringBuilder();
		builder.append(" ------------------------- MENU ---------------------------------\n"
				+ " 1 : Livraison volailles" + "\n 2 : Modifier poids abattage" + "\n 3 : Modifier prix du jour"
				+ "\n 4 : Modifier poids d'une vollaile" + "\n 5 : Voir le nombre de vollaile par type"
				+ "\n 6 : Voir le total de prix des volailles abattables " + "\n 7 : Vendre une volaille "
				+ "\n 8 : Rendre un paon au parc" + "\n 9 : Affichage Etat" + "\n 0 : Quitter\n"
				+ " ----------------------------------------------------------------" + "\n\n Choix : ");
		return builder.toString();
	}

	/**
	 * Traitement du choix de l'eleveur
	 * 
	 * @param choix   : choix de l'eleveur
	 * @param eleveur : l'eleveur possédant l'elevage
	 * @return false si il a décidé de quitter l'application, true sinon
	 */
	public boolean traitementChoix(String choix, Eleveur eleveur) {
		switch (choix) {
		case "0":
			return false;
		case "1":
			livraisonVolailles(eleveur);
			break;
		case "2":
			modifierPoidsOuPrixAbattage(Constantes.POULET, Constantes.POIDS);
			modifierPoidsOuPrixAbattage(Constantes.CANARD, Constantes.POIDS);
			break;
		case "3":
			modifierPoidsOuPrixAbattage(Constantes.POULET, Constantes.PRIX);
			modifierPoidsOuPrixAbattage(Constantes.CANARD, Constantes.PRIX);
			break;
		case "4":
			modifierPoidsVolaille(eleveur);
			break;
		case "5":
			System.out.println(affichageNbVolailleType(eleveur));
			break;
		case "6":
			System.out.println("Le prix total si vous abattez toutes les volailles abattables est de "
					+ calculTotalVolailleAbattable(eleveur));
			break;
		case "7":
			vendreVolaille(eleveur);
			break;
		case "8":
			rendrePaon(eleveur);
			break;
		case "9":
			new IhmAfficherEtat().afficherEtat(eleveur);
			break;
		default:
			System.out.println("Choix invalide");

		}
		return true;
	}

	/**
	 * Méthode de livraison
	 * 
	 * @param eleveur : l'eleveur possedant l'elevage
	 */
	public void livraisonVolailles(Eleveur eleveur) {
		int nbCanard;
		int nbPoulet;
		int nbPaon;
		float poids;
		if (eleveur != null) {
			LivraisonService ls = new LivraisonService();
			System.out.println("Entrer le nombre de canard à livrer : ");
			nbCanard = saisieInt();
			System.out.println("Entrer le nombre de poulet à livrer : ");
			nbPoulet = saisieInt();
			System.out.println("Entrer le nombre de paon à livrer : ");
			nbPaon = saisieInt();
			if (ls.livraisonPossible(nbCanard, nbPoulet, nbPaon)) { // controle de la quantité de la livraison
				for (int i = 0; i < nbCanard; i++) {
					System.out.println("Entrer le poids du " + (i + 1) + " canard : ");
					poids = saisieFloat();
					System.out.println(ls.ajouterVolaille(eleveur, Constantes.CANARD, poids).affichageInfo());
				}
				for (int i = 0; i < nbPoulet; i++) {
					System.out.println("Entrer le poids du " + (i + 1) + " poulet : ");
					poids = saisieFloat();
					System.out.println(ls.ajouterVolaille(eleveur, Constantes.POULET, poids).affichageInfo());
				}
				for (int i = 0; i < nbPaon; i++) {
					System.out.println(ls.ajouterVolaille(eleveur, Constantes.PAON, 0).affichageInfo());
				}
			} else {
				System.out.println("désolé la ferme est incapable d'accepter cette livraison");
			}
		}
	}

	/**
	 * Méthode pour modifier le poids d'abbatage ou le prix au Kg des volailles
	 * 
	 * @param volaille     : le type de volaille à modifier (poulet ou canard)
	 * @param modification la modification choisie (poids ou prix)
	 */
	public void modifierPoidsOuPrixAbattage(String volaille, String modification) {
		String choix;
		float valeur;

		System.out.println("Modification " + modification + " d'abatage des " + volaille + " (oui ou non) : ");
		choix = saisieOuiouNon();
		if ("oui".equalsIgnoreCase(choix)) {
			valeur = saisieFloat();
			VolailleAbbatableService volailleAbbatableService = new VolailleAbbatableService();

			volailleAbbatableService.modifierPoidsOuPrixAbbatage(volaille, modification, valeur);
		}
	}

	/**
	 * Méthode pour modifier le poids d' une vollaile
	 * 
	 * @param eleveur : l'eleveur possédant l'elevage
	 */
	public void modifierPoidsVolaille(Eleveur eleveur) {
		if (eleveur != null) {

			String identifiant;
			float poids;
			EleveurService eleveurService = new EleveurService();
			System.out.println("Entrer l' identifiant de la volaille : ");
			identifiant = choixEleveur();
			System.out.println("Entrer le nouveau poids : ");
			poids = saisieFloat();
			if (eleveurService.modifierPoidsVolaille(eleveur, identifiant, poids)) {
				System.out.println("Le poids a été modifier");
			} else {
				System.out.println("l'identifiant ne correspond pas à une volaille de l'élevage");
			}
		}
	}

	/**
	 * Affichage du nombre de volailles par type
	 * 
	 * @param eleveur : l'eleveur possedant l'elevage
	 * @return l'affichage du nombre de volaille par type
	 */
	public String affichageNbVolailleType(Eleveur eleveur) {
		StringBuilder builder = new StringBuilder();
		if (eleveur != null) {
			int nbCanard = 0;
			int nbPoulet = 0;
			int nbPaon = 0;

			for (Entry<String, Animal> volaille : eleveur.getListeVolailles().entrySet()) {
				if (volaille.getValue() instanceof Poulet) {
					nbPoulet++;
				} else if (volaille.getValue() instanceof Canard) {
					nbCanard++;
				} else if (volaille.getValue() instanceof Paon) {
					nbPaon++;
				}
			}
			builder.append("Nombre de canard : " + nbCanard + "\nNombre de poulet : " + nbPoulet + "\nNombre de paon : "
					+ nbPaon);
		}
		return builder.toString();
	}

	/**
	 * Retourne le total si on vend toutes les volailles abbatables
	 * 
	 * @param eleveur : l'eleveur possédant l'elevage
	 * @return le total des prix des volaille abbattable
	 */
	public float calculTotalVolailleAbattable(Eleveur eleveur) {
		float prixTotal = 0;
		if (eleveur != null) {
			for (Entry<String, Animal> animal : eleveur.getListeVolailles().entrySet()) {
				if (animal.getValue() instanceof IAbattable && ((IAbattable) animal.getValue()).isAbattable()) {
					prixTotal += ((IAbattable) animal.getValue()).prixAbattage();
				}
			}
		}
		return prixTotal;
	}

	/**
	 * Méthode pour vendre une volaille
	 * 
	 * @param eleveur : l'eleveur possédant l'elevage
	 */
	public void vendreVolaille(Eleveur eleveur) {
		if (eleveur != null) {
			String identifiant;
			float prix;

			EleveurService es = new EleveurService();

			System.out.println("Entrer l'identifiant de la volaille a vendre : ");
			identifiant = choixEleveur();
			prix=es.vendreVolaille(eleveur, identifiant);
			if (prix != 0) {
				System.out.println("Opération réussite, la volaille a été vendu, lre prix de vente est de " + prix);
			} else {
				System.out.println(
						"l'identifiant entré ne correspond a aucune volaille de l'élevage ou la volaille n'a pas atteint le poids necessaire");
			}
		}
	}

	/**
	 * Méthode pour rendre un paon
	 * 
	 * @param eleveur : l'eleveur possédant l'elevage
	 */
	public void rendrePaon(Eleveur eleveur) {
		if (eleveur != null) {
			String identifiant;
			Animal paon;
			EleveurService es = new EleveurService();

			System.out.println("Entrer l'identifiant du paon : ");
			identifiant = choixEleveur();
			paon = es.rendrePaon(eleveur, identifiant);
			if (paon!=null) {
				System.out.println("Opération réussite, le paon : "
						+ paon.affichageInfo() + " a retouvé sont parc");
			} else {
				System.out.println("l'identifiant entré ne correspond a aucun paon de l'élevage");
			}
		}
	}

	/**
	 * Méthode qui retourne ce que l'eleveur à rentré au clavier
	 * 
	 * @return la chaine de caractères que l'eleveur a rentré au clavier
	 */
	public String choixEleveur() {
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}

	/**
	 * Méthode qui retoune le float positif rentré par l'eleveur (boucle tant que
	 * l'eleveur n'a pas rentrer un float positif)
	 * 
	 * @return le float positif rentré par l'éleveur
	 */
	public float saisieFloat() {
		String saisieFloat;
		System.out.println("Entrer la valeur : ");
		do {
			saisieFloat = choixEleveur();
			if (ControleSaisie.saisieFloatPositif(saisieFloat)) {
				break;
			} else {
				System.out.println("Erreur entrer un float positif");
			}
		} while (true);
		return Float.parseFloat(saisieFloat);
	}

	/**
	 * Methode qui retourne le int positif ou nul rentré par l'eleveur
	 * 
	 * @return l'entier saisi
	 */
	public int saisieInt() {
		String saisieInt;
		do {
			saisieInt = choixEleveur();
			if (ControleSaisie.saisieInt(saisieInt, 0)) {
				break;
			} else {
				System.out.println("Erreur entrer un entier positif ou nul");
			}
		} while (true);
		return Integer.parseInt(saisieInt);
	}

	/**
	 * Methode qui retourne le choix oui ou non rentré par l'éleveur rentré par
	 * l'eleveur
	 * 
	 * @return la réponse saisie
	 */
	public String saisieOuiouNon() {
		String saisie;
		do {
			saisie = choixEleveur();
			if (ControleSaisie.isOuiNon(saisie)) {
				break;
			} else {
				System.out.println("Entrer oui ou non");
			}
		} while (true);
		return saisie;
	}
}
