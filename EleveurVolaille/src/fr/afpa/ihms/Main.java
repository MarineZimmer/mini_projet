package fr.afpa.ihms;



import fr.afpa.entites.Canard;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;

public class Main {

	public static void main(String[] args) {
		Eleveur eleveur = new Eleveur();
		
		initialisationFerme(eleveur, 50, 25, 5);
		Poulet.setPoidsAbattage(2);
		Canard.setPoidsAbattage(2);
		Poulet.setPrixKg(5);
		Canard.setPrixKg(5);
		
		IhmMenu ihmMenu = new IhmMenu();
		ihmMenu.run(eleveur);
		
	}
	
	/**
	 * intitialisation de la ferme
	 * @param eleveur : l'éleveur
	 * @param nbCanard : le nombre de canard à ajouter à la ferme
	 * @param nbPoulet : le nombre de poulet à ajouter à la ferme
	 * @param nbPaon : le nombre de paon à ajouter à la ferme
	 * @return le nombre d'animaux present dans la ferme
	 */
	public static int initialisationFerme(Eleveur eleveur, int nbCanard, int nbPoulet, int nbPaon) {
		for (int i = 0; i < nbCanard; i++) {
			Canard canard = new Canard((i+1)%6);
			eleveur.getListeVolailles().put(canard.getIdentifiant(), canard);
		}
		for (int i = 0; i < nbPoulet; i++) {
			Poulet poulet = new Poulet((i+1)%6);
			eleveur.getListeVolailles().put(poulet.getIdentifiant(), poulet);
		}
		for (int i = 0; i < nbPaon; i++) {
			Paon paon = new Paon();
			eleveur.getListeVolailles().put(paon.getIdentifiant(), paon);
		}
		return eleveur.getListeVolailles().size();
	}

}
