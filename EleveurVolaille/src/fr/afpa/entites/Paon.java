package fr.afpa.entites;

import fr.afpa.utils.Constantes;

public final class Paon extends Animal {

	/**
	 * Constructeur du Paon
	 */
	public Paon() {
		super();
	}

	@Override
	public String affichageInfo() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nType : Paon");
		builder.append(Constantes.IDENTIFIANT + this.getIdentifiant());
	return builder.toString();
	
	}

}
