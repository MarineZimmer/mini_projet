package fr.afpa.entites;

public abstract class Animal {
	
	private String identifiant;
	private static  int nbAnimal;
	
	/**
	 * Constructeur par defaut
	 */
	public Animal() {
		super();
		this.identifiant=String.format("%06d", ++nbAnimal);
	}

	/**
	 * Retourne l'identifiant de l'animal
	 * @return : identifiant de l'animal
	 */
	public final String getIdentifiant() {
		return identifiant;
	}
	
	/**
	 * Affichage des info de l'animal
	 * @return une chaine de caractères représentant les infos de l'animal
	 */
	public abstract String affichageInfo();
}
