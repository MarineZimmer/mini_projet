package fr.afpa.entites;

import java.util.HashMap;
import java.util.Map;

public final class Eleveur {
	private String nom;
	private Map<String, Animal> listeVolailles;

	/**
	 * Constructeur d'un eleveur
	 */
	public Eleveur() {
		super();
		listeVolailles = new HashMap<>();
	}
	
	/**
	 * Constructeur d'un éleveur
	 * @param nom : le nom de l'éleveur
	 */
	public Eleveur(String nom) {
		super();
		this.nom=nom;
		listeVolailles = new HashMap<>();
	}

	
	/**
	 * Retourne le nom de l'éleveur
	 * @return le nom de l'eleveur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Modifie le nom de l'éleveur
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Retourne la liste des animaux de l'éleveur
	 * @return la liste des animaux de l'éleveur
	 */
	public Map<String, Animal> getListeVolailles() {
		return listeVolailles;
	}


}
