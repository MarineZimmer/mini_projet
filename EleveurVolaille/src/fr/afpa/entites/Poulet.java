package fr.afpa.entites;

import fr.afpa.ientites.IAbattable;

public final class Poulet extends Volaille implements IAbattable{
	private static float poidsAbattage;
	private static float prixKg;

	/**
	 * Constructeur du poulet
	 */
	public Poulet() {
		super();
	}

	/**
	 *  Constructeur du poulet
	 * @param poids : le poids du poulet
	 */
	public Poulet(float poids) {
		super(poids);
	}

	/**
	 * Retourne le poids d'abbatage des poulets
	 * @return : le poids d'abbatage des poulets
	 */
	public static float getPoidsAbattage() {
		return poidsAbattage;
	}

	/**
	 * Modifie le poids d'abbatage des poulets
	 * @param poidsAbattage : le nouveau poids d'abbatage
	 */
	public static void setPoidsAbattage(float poidsAbattage) {
		Poulet.poidsAbattage = poidsAbattage;
	}

	/**
	 * Retourne le prix au kg du poulet
	 * @return : le prix au kg des poulets
	 */
	public static float getPrixKg() {
		return prixKg;
	}

	/**
	 * Modifie le prix au kg des poulets
	 * @param prixKg : le nouveau prix au Kg des poulets
	 */
	public static void setPrixKg(float prixKg) {
		Poulet.prixKg = prixKg;
	}

	@Override
	public boolean isAbattable() {
		
			return this.getPoids() >= poidsAbattage;
		
	}

	@Override
	public float prixAbattage() {
			return this.getPoids() *prixKg;
	}

	@Override
	public String affichageInfo() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nType : Poulet");
		builder.append(super.affichageInfo());
		builder.append("\nPoids d'abattage poulet : " + poidsAbattage);
		builder.append("\nPrix au Kg poulet : " + prixKg);
	return builder.toString();
	}

}
