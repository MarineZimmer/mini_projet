package fr.afpa.entites;

import fr.afpa.utils.Constantes;

public abstract class Volaille extends Animal {

	private float poids;

	/**
	 * Constructeur d'une volaille
	 */
	public Volaille() {
		super();
	}

	/**
	 * Constructeur d'une volaille
	 * @param poids : le poids de la volaille
	 */
	public Volaille(float poids) {
		super();
		this.poids = poids;
	}

	/**
	 * Retourne le poids de la volaille
	 * @return : le poids de la volaille
	 */
	public final float getPoids() {
		return poids;
	}

	/**
	 * Modifie le poids de la volaille
	 * @param poids : le nouveau poids de la volaille
	 */
	public final void setPoids(float poids) {
		this.poids = poids;
	}

	@Override
	public String affichageInfo() {
		StringBuilder builder = new StringBuilder();
		builder.append(Constantes.IDENTIFIANT + this.getIdentifiant());
		builder.append("\nPoids : " + this.getPoids());
	return builder.toString();
	}

	
}
