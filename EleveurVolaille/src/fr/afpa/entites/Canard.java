package fr.afpa.entites;

import fr.afpa.ientites.IAbattable;

public final class Canard extends Volaille implements IAbattable{

	private static float poidsAbattage;
	private static float prixKg;

	/**
	 * Constructeur d'un canard
	 */
	public Canard() {
		super();
	}

	/**
	 * Constructeur d'un canard
	 * @param poids : le poids du canard
	 */
	public Canard(float poids) {
		super(poids);
	}

	/**
	 * Retourne le poids d'abbatage des canards
	 * @return : le poids d'abbatage des canards
	 */
	public static float getPoidsAbattage() {
		return poidsAbattage;
	}

	/**
	 * Modifie le poids d'abbatage des canards
	 * @param poidsAbattage : le nouveau poids d'abbatage
	 */
	public static void setPoidsAbattage(float poidsAbattage) {
		Canard.poidsAbattage = poidsAbattage;
	}

	/**
	 * Retourne le prix au kg du canard
	 * @return : le prix au kg des canards
	 */
	public static float getPrixKg() {
		return prixKg;
	}

	/**
	 * Modifie le prix au kg des canards
	 * @param prixKg : le nouveau prix au Kg des canards
	 */
	public static void setPrixKg(float prixKg) {
		Canard.prixKg = prixKg;
	}
	@Override
	public String affichageInfo() {
		StringBuilder builder = new StringBuilder();
			builder.append("\nType : Canard");
			builder.append(super.affichageInfo());
			builder.append("\nPoids d'abattage canard : " + poidsAbattage);
			builder.append("\nPrix au Kg canard : " + prixKg);
		return builder.toString();
	}  
	
	@Override
	public boolean isAbattable() {
			return this.getPoids() >= poidsAbattage;
	}

	@Override
	public float prixAbattage() {
			return this.getPoids() *prixKg;
	}

	

}
