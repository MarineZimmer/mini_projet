package fr.afpa.ientites;



public interface IAbattable {
	/**
	 * Controle si l'objet peut être abbattu
	 * @return : true si les critères d'abbatage sont respecter, false sinon
	 */
	public boolean isAbattable();
	
	/**
	 * Calcul le prix de revient si on vend l'objet
	 * @return le prix de revient de la vente
	 */
	public float prixAbattage(); 
}
