package fr.afpa.services;

import fr.afpa.entites.Animal;

import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Volaille;
import fr.afpa.ientites.IAbattable;

public final class EleveurService {

	/**
	 * Service de modification du poids d'une volaille
	 * @param eleveur : l'eleveur
	 * @param identifiant : l'identifiant de la volaille
	 * @param poids : le nouveau poids
	 * @return true si la modification à été faite, false sinon
	 */
	public boolean modifierPoidsVolaille(Eleveur eleveur, String identifiant, float poids) {

		if (eleveur.getListeVolailles().containsKey(identifiant)
				&& eleveur.getListeVolailles().get(identifiant) instanceof Volaille) {
			((Volaille) eleveur.getListeVolailles().get(identifiant)).setPoids(poids);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * service qui rend un paon au parc
	 * @param eleveur : l'eleveur
	 * @param identifiant : l'identifiant du paon à rendre
	 * @return le Paon rendu ou null si le paon n'existe pas
	 */
	public Animal rendrePaon(Eleveur eleveur, String identifiant) {
		if (eleveur.getListeVolailles().get(identifiant) instanceof Paon) {
			return eleveur.getListeVolailles().remove(identifiant);
		}
		return null;
	}

	/**
	 * Service de vente d'une volaille
	 * @param eleveur : l'eleveur
	 * @param identifiant : l'identifiant du paon à rendre
	 * @return : le prix de la vente
	 */
	public float vendreVolaille(Eleveur eleveur, String identifiant) {
		Animal volaille = eleveur.getListeVolailles().get(identifiant);
		float prix = 0;
		if (volaille instanceof IAbattable && ((IAbattable) volaille).isAbattable()) {
			prix = ((IAbattable) volaille).prixAbattage();
			System.out.println("vente de la volaille " + volaille.affichageInfo());
			eleveur.getListeVolailles().remove(identifiant);
		}
		return prix;
	}
}
