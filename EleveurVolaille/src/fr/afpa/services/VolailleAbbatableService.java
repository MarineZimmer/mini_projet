package fr.afpa.services;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Poulet;

import fr.afpa.utils.Constantes;

public final class VolailleAbbatableService {

	public boolean modifierPoidsOuPrixAbbatage(String volaille, String modification, float valeur) {
		if (Constantes.POULET.equals(volaille)) {
			if (Constantes.POIDS.equals(modification)) {
				Poulet.setPoidsAbattage(valeur);
				System.out.println("Modification du " + modification +" du " + volaille + " reussit,nouveau"+  modification+" : "
						+ Poulet.getPoidsAbattage());
				return true;
			} else if (Constantes.PRIX.equals(modification)) {
				Poulet.setPrixKg(valeur);
				System.out.println("Modification du " + modification +" du " + volaille + " reussit ,nouveau"+  modification+" : "
						+ Poulet.getPrixKg());
				return true;
			}
		} else if (Constantes.CANARD.equals(volaille)) {
			if (Constantes.POIDS.equals(modification)) {
				Canard.setPoidsAbattage(valeur);
				System.out.println("modification du " + modification +" d'abbatage du " + volaille + " reussit, nouveau"+  modification+" : "
						+ Canard.getPoidsAbattage());
				return true;
			} else if (Constantes.PRIX.equals(modification)) {
				Canard.setPrixKg(valeur);
				System.out.println("modification du " + modification +" d'abbatage du " + volaille + " reussit , nouveau"+  modification+" : "
						+ Canard.getPrixKg());
				return true;
			}
		}
		return false;
	}

	

}
