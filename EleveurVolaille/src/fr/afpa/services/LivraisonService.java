package fr.afpa.services;

import fr.afpa.entites.Animal;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.utils.Constantes;

public final class LivraisonService {

	private static final int NB_VOLAILLE = 7;
	private static final int NB_CANARD = 4;
	private static final int NB_POULET = 5;
	private static final int NB_PAON = 3;
	
	
	
	/**
	 * Controle si la livraison est possible
	 * @param nbCanard : nombre de canard livré
	 * @param nbPoulet : nombre de poulet livré
	 * @param nbPaon : nombre de paon livré
	 * @return true si la livraison est possible
	 */
	public boolean livraisonPossible(int nbCanard, int nbPoulet, int nbPaon) {
		return nbCanard <= NB_CANARD && nbPoulet<=NB_POULET && nbPaon<=NB_PAON && (nbPoulet+nbCanard+nbPaon)<=NB_VOLAILLE;
	}
	
	/**
	 * Service d'ajout d'un canard dans l'élevage
	 * @param eleveur : l'éleveur possédant l'élevage
	 * @param type : le type d'ajout
	 * @param poids : le poids de l'animal
	 * @return : l'animal ajouté ou null si le type de l'animal est incorect
	 */
	public Animal ajouterVolaille(Eleveur eleveur, String type, float poids) {
		if (Constantes.CANARD.equals(type)) {
			Canard canard = new Canard(poids);
			eleveur.getListeVolailles().put(canard.getIdentifiant(), canard);
			return canard;
		}
		if (Constantes.POULET.equals(type)) {
			Poulet poulet = new Poulet(poids);
			eleveur.getListeVolailles().put(poulet.getIdentifiant(), poulet);
			return poulet;
		}
		if (Constantes.PAON.equals(type)) {
			Paon paon = new Paon();
			eleveur.getListeVolailles().put(paon.getIdentifiant(), paon);
			return paon;
		}
		return null;
	}
	
	

}
