package fr.afpa.controles;

public final class ControleSaisie {

	private ControleSaisie() {
	}
	
	
	/**
	 * Controle si la chaine de caracteres est un int compris entre superieur au min
	 * @param nombre : chaine de caracteres a tester
	 * @param min    : int representant le minimum
	 * @return true la chaine est un int compris entre le min et le max inclus,
	 *         false sinon
	 */
	public static boolean saisieInt(String nombre, int min) {
		try {
			int n = Integer.parseInt(nombre);
			if (n < min ) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	
	/**
	 * Controle si la chaine en parametre est un float
	 * @param nombre : chaine a tester
	 * @return true si la chaine en parametre est un float positif
	 */
	public static boolean saisieFloatPositif(String nombre) {
		try {
			if(Float.parseFloat(nombre)<=0) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Controle si la chaine de caractere est egale a oui ou non
	 * 
	 * @param reponse : la chaine de caractere a tester
	 * @return true si reponse est egale a oui ou non
	 */
	public static boolean isOuiNon(String reponse) {
		return "oui".equalsIgnoreCase(reponse) || "non".equalsIgnoreCase(reponse);
	}

}
