package fr.afpa.tests;

import java.time.LocalDate;

import org.junit.Test;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.ihm.IhmMenu;
import junit.framework.TestCase;

public class TestIhmBanque extends TestCase{
private IhmMenu ihmMenu;

@Override
protected void setUp() throws Exception {
	super.setUp();
	ihmMenu = new IhmMenu();
}

@Test
public void testTraitementChoixUtilisateur() {
	Administrateur adm = new Administrateur();
	Banque banque= new Banque();
	ihmMenu.affichageMenu(adm);
	Client client = new Client("a", "e", LocalDate.now(), "re");
	
	assertFalse("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, banque, "0"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, banque, "fayak"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, null, "8"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, null, "9"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, null, "10"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, null, "11"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, null, "12"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, null, "13"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, null, "14"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(adm, null, "15"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client,banque , "2"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, banque, "1"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "3"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "4"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "5"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "6"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "7"));
	client=null;
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client,banque , "2"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, banque, "1"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "3"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "4"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "5"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "6"));
	assertTrue("traitementchoixinvalide", ihmMenu.traitementChoixMenu(client, null, "7"));
}

@Override
protected void tearDown() throws Exception {
	super.tearDown();
	ihmMenu=null;
}


}
