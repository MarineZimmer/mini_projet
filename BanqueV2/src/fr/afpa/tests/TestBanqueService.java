package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.ihm.Main;
import fr.afpa.service.BanqueService;
import junit.framework.TestCase;

public class TestBanqueService extends TestCase{
private BanqueService bs;
private Banque banque;
@Override
protected void setUp() throws Exception {
	super.setUp();
	bs = new BanqueService();
	Client.setNbClient(0);
	Conseiller.setNbConseillier(0);
	banque=new Banque();
	Main.initialisationBanque(banque);
}

@Test
public void testPersoneAuthentifier() {
	assertNull("authenthifiacatio incorrescte" , bs.personneAuthentifier("111", null));
	assertNull("authenthifiacatio incorrescte" , bs.personneAuthentifier("111", banque));
	assertNull("authenthifiacatio incorrescte" , bs.personneAuthentifier("CC020001", banque));
	assertNotNull("authenthifiacatio incorrescte" , bs.personneAuthentifier("CC000001", banque));
	
	assertNotNull("authenthifiacatio incorrescte" , bs.personneAuthentifier("ADM01", banque));
	assertNotNull("authenthifiacatio incorrescte" , bs.personneAuthentifier("CO0002", banque));
	assertNull("authenthifiacatio incorrescte" , bs.personneAuthentifier("CO7002", banque));
}


@Test
public void testCreationAgence() {
	//assertTrue("creation agence", bs.creationAgence(banque, banque.getAdministrateur()));
	assertFalse("creation agence", bs.creationAgence(null, banque.getAdministrateur()));
	assertFalse("creation agence", bs.creationAgence(banque, null));
	assertFalse("creation agence", bs.creationAgence(null, null));
}

@Test 
public void testRechercheConseillerClient() {
	Conseiller cons = banque.getListeAgences().get("001").getListeConseillers().get("CO0002");
	assertSame("recherche cons ", cons, bs.chercherConseiller(cons.getPortefeuilleClient().get("CC000001"), banque));
	assertNull("recherche cons ",  bs.chercherConseiller(cons.getPortefeuilleClient().get("CC020001"), banque));
	assertNull("recherche cons ", bs.chercherConseiller(cons.getPortefeuilleClient().get("CC000001"), null));
	assertNull("recherche cons ", bs.rechercheConseiller("CO0002", null));
	
}

@Test
public void testRechercheClient() {
	Client client = banque.getListeAgences().get("001").getListeConseillers().get("CO0002").getPortefeuilleClient().get("CC000001");
	assertSame("recherche client ", client, bs.rechercheClient(banque, "1", "CC000001"));
	assertNull("recherche client ", bs.rechercheClient(banque, "1", "CC040001"));
	
	assertSame("recherche client ", client, bs.rechercheClient(banque, "2", "client un"));
	assertNull("recherche client ", bs.rechercheClient(banque, "2", "fayak"));
	
	assertSame("recherche client ", client, bs.rechercheClient(banque, "3", "00000000001"));
	assertNull("recherche client ", bs.rechercheClient(banque, "3", "01234567891"));
	
	assertNull("recherche client ", bs.rechercheClient(null, "3", "01234567891"));	
}

@Test
public void testRechercheAgence() {
	Agence agence = banque.getListeAgences().get("001");
	assertSame("recherche agence ", agence, bs.rechercheAgence(banque, "001"));
	assertNull("recherche agence ", bs.rechercheAgence(banque, "101"));
	assertNull("recherche agence ", bs.rechercheAgence(null, "001"));
	
	
}

@Override
protected void tearDown() throws Exception {
	super.tearDown();
	bs=null;
	banque=null;
}


}
