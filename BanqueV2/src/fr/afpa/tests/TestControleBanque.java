package fr.afpa.tests;


import org.junit.Test;

import fr.afpa.controle.ControleBanque;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.ihm.Main;
import junit.framework.TestCase;

public class TestControleBanque extends TestCase {

	Banque banque ;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		banque = new Banque();
	}

	@Test
	public void testAgenceExiste() {
		assertFalse("agnece existe incorrect", ControleBanque.agenceExiste(banque));
		Main.initialisationBanque(banque);
		assertTrue("agnece existe incorrect", ControleBanque.agenceExiste(banque));
	}
	
	@Test
	public void testCodeAgenceUnique() {
		Main.initialisationBanque(banque);
		assertFalse("code agence uniqu incorrect", ControleBanque.codeAgenceUnique(banque, "001"));
		assertTrue("code agence uniqu incorrect", ControleBanque.codeAgenceUnique(banque, "005"));
	}
	
	@Test
	public void testHasClient() {
		assertFalse("as clients incorrect", ControleBanque.hasClient(banque));
		Main.initialisationBanque(banque);
		banque.getListeAgences().put("254", new Agence("re", "az", "254"));
		assertTrue("as clients incorrect", ControleBanque.hasClient(banque));
	}
	
	@Test
	public void testHasCompte() {
		assertFalse("as compte incorrect", ControleBanque.hasCompte(banque));
		Main.initialisationBanque(banque);
		assertTrue("as compte incorrect", ControleBanque.hasCompte(banque));
	}
	
	@Test
	public void testNumCompteUnique() {
		Main.initialisationBanque(banque);
		assertFalse("num compte non unique", ControleBanque.numeroCompteUnique(banque, "00000000001"));
		assertTrue("num compte non unique", ControleBanque.numeroCompteUnique(banque, "01234567895"));
	}
	
	
	
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		banque=null;
	}
	
	

}
