package fr.afpa.tests;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class TestsAppli extends TestCase {

	public static TestSuite suite() {
		TestSuite suite = new TestSuite();
		suite.addTestSuite(TestControleSaisie.class);
		suite.addTestSuite(TestControleCompte.class);
		suite.addTestSuite(TestControlePersonne.class);
		suite.addTestSuite(TestControleConseiller.class);
		suite.addTestSuite(TestControleBanque.class);
		suite.addTestSuite(TestControleAgence.class);
		suite.addTestSuite(TestControleClient.class);
		suite.addTestSuite(TestControleAuthentification.class);
		suite.addTestSuite(TestServiceAleatoire.class);
		suite.addTestSuite(TestCompteService.class);
		suite.addTestSuite(TestClientService.class);
		
		suite.addTestSuite(TestBanqueEntite.class);
		suite.addTestSuite(TestCryptageService.class);
		suite.addTestSuite(TestIhmBanque.class);
		suite.addTestSuite(TestChargementBanque.class);
		suite.addTestSuite(TestFichierService.class);
		suite.addTestSuite(TestBanqueService.class);
		
		
		return suite;
	}
	
	public void main(String[] args) {
		TestRunner.run(suite());
	}
}