package fr.afpa.tests;

import org.junit.Test;


import fr.afpa.controle.ControleClient;
import fr.afpa.entite.Client;
import fr.afpa.ihm.IhmMenu;
import junit.framework.TestCase;

public class TestControleClient extends TestCase {

	

	@Test
	public void testClientActif() {
		Client client = new Client();
		client.setActif(true);
		assertTrue("client actif incorrect", ControleClient.isActif(client));
		client.setActif(false);
		new IhmMenu().affichageMenu(client);
		assertFalse("client actif incorrect", ControleClient.isActif(client));
		
	}
	
	

}
