package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.CompteEpargne;
import fr.afpa.entite.LivretA;
import fr.afpa.service.CompteService;
import junit.framework.TestCase;

public class TestCompteService extends TestCase{
CompteService cs;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		cs = new CompteService();
	}

	
	@Test
	public void testCalculFrais() {
		Compte compte = new CompteCourant();
		compte.setFrais(25);
		assertEquals("frais incorect ", 25,cs.calculFrais(compte) , 1);
		Compte compte2 = new LivretA();
		compte2.setFrais(25);
		 compte2.setSolde(100);
		assertEquals("frais incorect ", 35 ,cs.calculFrais(compte2) ,1);
		Compte compte3 = new CompteEpargne();
		compte3.setFrais(25);
		 compte3.setSolde(100);
		assertEquals("frais incorect ", 50 ,cs.calculFrais(compte3) , 1);
	}
	
	@Test
	public void testModifSolde() {
		Compte compte = new CompteCourant();
		compte.setActif(false);
		assertFalse("modif solde", cs.modificationSoldeCompte(12, compte));
		compte=null;
		assertFalse("modif solde", cs.modificationSoldeCompte(12, compte));
	}
	
	@Test
	public void testAjoutTransaction() {
		Compte compte = new CompteCourant();
		compte.setActif(true);
		compte.setDecouvert(true);
		compte.setSolde(100);
		assertTrue("modif solde", cs.modificationSoldeCompte(-20, compte));
		
	}
	
	
	

}
