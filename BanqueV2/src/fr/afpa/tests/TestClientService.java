package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entite.Client;
import fr.afpa.service.ClientService;
import junit.framework.TestCase;

public class TestClientService extends TestCase{
	private ClientService cs;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		cs=new ClientService();
	}
@Test
public void testCreerClient() {
	Client client = cs.creerClient("fayak", "fayak", "21/11/1990", "fayak@gmzil.com", "1234");
	assertNotNull("creation client incorest", client);
	assertEquals("nom incorete", "fayak", client.getNom());
	client.setNom("fayaz");
	assertEquals("nom incorete", "fayaz", client.getNom());
	
	assertEquals("prenom incorete", "fayak", client.getPrenom());
	client.setPrenom("fayaz");
	assertEquals("prenom incorete", "fayaz", client.getPrenom());
	
	assertEquals("mail incorete", "fayak@gmzil.com", client.getMail());
	client.setMail("fayaz@gmzil.com");
	assertEquals("mail incorete", "fayaz@gmzil.com", client.getMail());
}

	

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		cs=null;
	}

}
