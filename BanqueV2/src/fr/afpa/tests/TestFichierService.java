package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entite.Banque;
import fr.afpa.service.FichierService;
import junit.framework.TestCase;

public class TestFichierService extends TestCase{
	private FichierService fs;
	private Banque banque;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		fs = new FichierService();
		banque = new Banque();
	}
	
	@Test
	public void testSauvegardeObjet() {
		assertTrue("sauvzgarde echoue" ,fs.ecrireObjetFichier(banque, "ressources\\sauvegarde\\testSauvegardeBanque.txt"));
		assertFalse("sauvzgarde echoue" ,fs.ecrireObjetFichier(null, "resources\\sauvegarde\\testSauvegardeBanque.txt"));
		assertFalse("sauvzgarde echoue" ,fs.ecrireObjetFichier(banque, "resources\\sauvegrde\\testSauvegardeBanque.txt"));
		assertNotNull("chargement echoue", fs.lireObjetFichier("ressources\\sauvegarde\\testSauvegardeBanque.txt"));
		assertNull("chargement echoue", fs.lireObjetFichier("resources\\sauegarde\\testSauvegardeBanque.txt"));
	}
	
	@Test
	public void testSauvegardeFichier() {

		assertFalse("sauvzgarde echoue" ,fs.ecritureFichier("ressources\\sauvegarde\\testSauvegardefichier.txt", null, false));
		assertTrue("sauvzgarde echoue" ,fs.ecritureFichier("ressources\\sauvegarde\\testSauvegardefichier.txt", "test", false));
		assertTrue("sauvzgarde echoue" ,fs.ecritureFichier("ressources\\sauvegarde\\testSauvegardefichier.txt", "test", true));
		assertFalse("sauvzgarde echoue" ,fs.ecritureFichier("ressoues\\sauvegarde\\testSauvegardefichier.txt", "test", false));
		assertEquals("lecture incorerect" , 2, fs.lecture("ressources\\sauvegarde\\testSauvegardefichier.txt").length);

	}
	
	
	
	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}
	
	
}