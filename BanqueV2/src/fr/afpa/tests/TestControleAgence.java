package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.controle.ControleAgence;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Conseiller;
import junit.framework.TestCase;

public class TestControleAgence extends TestCase {

	Agence agence;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		agence = new Agence("nom", "adresse", "123");
	}
	
	@Test
	public void testConseillerExiste() {
		assertFalse("conseiller existe incorrect", ControleAgence.conseillerExiste(agence));
		agence.getListeConseillers().put("CO0001", new Conseiller());
		assertTrue("conseiller existe incorrect", ControleAgence.conseillerExiste(agence));
	}

	public void testConseillerPresent() {
		assertFalse("conseiller existe incorrect", ControleAgence.conseillerPresent(agence, "CO0001"));
		agence.getListeConseillers().put("CO0001", new Conseiller());
		assertTrue("conseiller existe incorrect", ControleAgence.conseillerPresent(agence, "CO0001"));
	}
	

}
