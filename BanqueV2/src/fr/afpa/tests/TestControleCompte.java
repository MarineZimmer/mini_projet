package fr.afpa.tests;

import java.time.LocalDate;

import org.junit.Test;

import fr.afpa.controle.ControleCompte;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteCourant;
import fr.afpa.ihm.Main;
import junit.framework.TestCase;

public class TestControleCompte extends TestCase{

	
	
	
	@Test
	public void testNbCompte() {
		Client client= new Client("fayak", "fayak", LocalDate.parse("2000-01-01"), "fayak@gmzil.com");
		client.getListeComptes().put("000", new CompteCourant(true, "000"));
		client.getListeComptes().put("001", new CompteCourant(true, "001"));
		client.getListeComptes().put("002", new CompteCourant(true, "002"));
		CompteCourant compte = new CompteCourant(false, "003");
		compte.setActif(false);
		client.getListeComptes().put("003", compte);
		
		assertEquals("nb compte invalid", 3, ControleCompte.nbComptesActif(client));
	}
	
	@Test
	public void testRetraitPossible() {
		CompteCourant compte = new CompteCourant(false, "000");
		assertFalse("retrait incorrect", ControleCompte.retraitPossible(compte, 10));
		compte.setSolde(20);
		assertTrue("retrait incorrect", ControleCompte.retraitPossible(compte, 10));
		CompteCourant compte2 = new CompteCourant(true, "001");
		assertTrue("retrait incorrect", ControleCompte.retraitPossible(compte2, 10));
		compte.setActif(false);
		assertFalse("retrait incorrect", ControleCompte.retraitPossible(compte, 10));
	}
	
	@Test
	public void testDateOperation() {
		String dateDebut = "21/11/2010";
		String dateFin = "29/11/2018";
		String dateOperation = "2017-01-01";
		assertTrue("date incorrect", ControleCompte.dateOperation(dateOperation, dateDebut, dateFin));
		 dateOperation = "2009-01-01";
		assertFalse("date incorrect", ControleCompte.dateOperation(dateOperation, dateDebut, dateFin));
		dateOperation = "2019-01-01";
		assertFalse("date incorrect", ControleCompte.dateOperation(dateOperation, dateDebut, dateFin));
		dateOperation = "292/11/2009";
		assertFalse("date incorrect", ControleCompte.dateOperation(dateOperation, dateDebut, dateFin));

	}
	
	public void testIsCompteValide() {
		Banque banque = new Banque();
		Main.initialisationBanque(banque);
		assertFalse("compte invalide",ControleCompte.isCompteValide("01234567891", banque));
		assertTrue("compte invalide",ControleCompte.isCompteValide("00000000001", banque));
		banque=null;
		assertFalse("compte invalide",ControleCompte.isCompteValide("01234567891", banque));
	}
	

	
	

}
