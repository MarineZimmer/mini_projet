package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.controle.ControleAuthentification;

import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.ihm.Main;
import junit.framework.TestCase;

public class TestControleAuthentification extends TestCase {

	
	@Test
	public void testAuthentification() {
		Conseiller.setNbConseillier(0);
		Client.setNbClient(0);
	
		Banque banque = new Banque();
		Main.initialisationBanque(banque);
		assertEquals("authentification incorrecte", "CC000001", ControleAuthentification.authentification("0000000001", "client1"));
		assertNull("authentification incorrecte",  ControleAuthentification.authentification("0000000001", "clent1"));
		assertEquals("authentification incorrecte", "ADM01", ControleAuthentification.authentification("ADM01", "admin"));
		assertEquals("authentification incorrecte", "CO0002", ControleAuthentification.authentification("CO0002", "cons1"));
	}
}
