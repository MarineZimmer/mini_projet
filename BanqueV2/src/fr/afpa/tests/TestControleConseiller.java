package fr.afpa.tests;

import java.time.LocalDate;

import org.junit.Test;

import fr.afpa.controle.ControleConseiller;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import junit.framework.TestCase;

public class TestControleConseiller  extends TestCase{

	

	@Test
	public void testHasClients() {
		Conseiller cons = new Conseiller("fayak", "fayak", LocalDate.parse("2000-01-01"), "fayak@gmzil.com");
		assertFalse("conseiller client invalide", ControleConseiller.hasClients(cons));
		Client client = new Client("fayak", "fayak", LocalDate.parse("2000-01-01"), "fayak@gmzil.com");
		cons.getPortefeuilleClient().put("FF000001", client);
		assertTrue("conseiller client invalide", ControleConseiller.hasClients(cons));
		
	}
	
	

}
