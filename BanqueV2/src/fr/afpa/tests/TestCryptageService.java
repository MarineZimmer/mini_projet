package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.service.CryptageService;
import junit.framework.TestCase;

public class TestCryptageService extends TestCase {

	CryptageService cs;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		cs=new CryptageService();
	}

	@Test
	public void testDecoder() {
		assertEquals("abcd", cs.decoder("dcba"));
	}
	
	@Test
	public void testCoder() {
		assertEquals("dcba", cs.encoder("abcd"));
	}
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		cs=null;
	}
	

}
