package fr.afpa.tests;

import org.junit.Test;

import fr.afpa.entite.Banque;
import fr.afpa.ihm.Main;
import fr.afpa.service.FichierService;
import junit.framework.TestCase;

public class TestChargementBanque extends TestCase {

	private FichierService fs;
	private Banque banque;
	
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		fs = new FichierService();
		banque = new Banque();
	}
	
	@Test
	public void testSauvegardeBanque() {
		assertTrue("sauvzgarde echoue" ,fs.ecrireObjetFichier(banque, "ressources\\sauvegarde\\testSauvegardeBanque.txt"));
		assertNotNull("chargement echoue",Main.chargementBanque("ressources\\sauvegarde\\testSauvegardeBanque.txt"));
		assertNull("chargement echoue",Main.chargementBanque("resources\\sauvegarde\\testSauvegrdeBanque.txt"));
		
	}
	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}
	
	
}
