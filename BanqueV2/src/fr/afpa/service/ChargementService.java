package fr.afpa.service;

import java.util.Map;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;

public class ChargementService {

	/**
	 * Service qui modifie les variable de Class des classes client(nbClient) et
	 * conseiller(nbConseiller) lorsque l'on regharge notre banque
	 * 
	 * @param banque : la banque
	 */
	public void initialisationVariableStatique(Banque banque) {
		int nbClient = 0;
		int nbConseiller = 1; // l'administrateur est le conseiller 1
		for (Map.Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			for (Map.Entry<String, Conseiller> conseiller : agence.getValue().getListeConseillers().entrySet()) {
				nbConseiller++;
				nbClient += conseiller.getValue().getPortefeuilleClient().size();
			}
		}
		Client.setNbClient(nbClient);
		Conseiller.setNbConseillier(nbConseiller);
	}

}
