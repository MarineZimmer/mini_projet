package fr.afpa.entite;

import java.io.Serializable;

public class CompteCourant extends Compte implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompteCourant() {
		super();
	}

	public CompteCourant(boolean decouvert, String numeroCompte) {
		super(decouvert, numeroCompte);
	}

	@Override
	public String toString() {
		return "CompteCourant [toString()=" + super.toString() + "]";
	}

}
