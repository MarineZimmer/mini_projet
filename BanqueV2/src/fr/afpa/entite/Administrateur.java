package fr.afpa.entite;

import java.io.Serializable;
import java.time.LocalDate;

public class Administrateur extends Conseiller implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Administrateur() {
		super();
	}

	public Administrateur(String nom, String prenom, LocalDate dateNaissance, String mail) {
		super(nom, prenom, dateNaissance, mail);
		this.setLogin("ADM" + String.format("%02d", Conseiller.getNbConseillier()));
	}
	

	

	

	@Override
	public String toString() {
		return "Administrateur [toString()=" + super.toString() + "]";
	}

}
