package fr.afpa.entite;

import java.io.Serializable;

public class LivretA extends Compte implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LivretA() {
		super();
	}

	public LivretA(boolean decouvert, String numeroCompte) {
		super(decouvert, numeroCompte);
	}

	@Override
	public String toString() {
		return "LivretA [toString()=" + super.toString() + "]";
	}

}
