package fr.afpa.controle;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map.Entry;

import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.service.BanqueService;

public class ControleCompte {

	private ControleCompte() {
		super();
	}

	/**
	 * service qui renvoie le nombre de comptes actifs du client
	 * 
	 * @param client : le Client
	 * @return le nombre de compte actif du client
	 */
	public static int nbComptesActif(Client client) {
		int nbCompteActif = 0;
		for (Entry<String, Compte> compte : client.getListeComptes().entrySet()) {
			if (compte.getValue().isActif()) {
				nbCompteActif++;
			}
		}
		return nbCompteActif;
	}

	/**
	 * Cotrole si la date d'une operation est comprise entre la date de debut et la
	 * date de fin
	 * 
	 * @param dateOperation : date de l'operation
	 * @param dateDebut     : date de debut de la periode
	 * @param dateFin       : date de fin de la periode
	 * @return true si la date d'une operation est comprise entre la date de debut
	 *         et la date de fin, false sinon false
	 */
	public static boolean dateOperation(String dateOperation, String dateDebut, String dateFin) {
		try {
			LocalDate date1 = LocalDate.parse(dateOperation);
			LocalDate dateDebut1 = LocalDate.parse(dateDebut, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			LocalDate datefin1 = LocalDate.parse(dateFin, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			if (dateDebut1.isAfter(date1) || datefin1.isBefore(date1)) {
				return false;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Controle si le retrait d'argent est possible
	 * 
	 * @param compte  : le compte a controller
	 * @param montant : le montant a retirer
	 * @return true si le retrait est possible, false sinon
	 */
	public static boolean retraitPossible(Compte compte, float montant) {
		return !(!compte.isActif() || (!compte.isDecouvert() && compte.getSolde() - montant < 0));

	}

	/**
	 * Controle si un numero de compte est valide (si il existe dans la banque)
	 * 
	 * @param numeroCompte : numero de compte a tester
	 * @param banque       : la Banque
	 * @return true si le compte existe dans la banque
	 */
	public static boolean isCompteValide(String numeroCompte, Banque banque) {
		BanqueService bs = new BanqueService();
		return bs.rechercheCompte(numeroCompte, banque) != null;

	}
}
