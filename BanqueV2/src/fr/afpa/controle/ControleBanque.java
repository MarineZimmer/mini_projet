package fr.afpa.controle;

import java.util.Map.Entry;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.service.BanqueService;

public class ControleBanque {

	private ControleBanque() {
		super();
	}

	/**
	 * Controle si une agence existe au sein de la Banque
	 * 
	 * @param banque : la Banque
	 * @return true si au moins une agence existe, false sinon
	 */
	public static boolean agenceExiste(Banque banque) {
		return !banque.getListeAgences().isEmpty();
	}

	/**
	 * Controle si le code agence est deja present dans la banque
	 * 
	 * @param banque     : la Banque dans laquelle l'agence est cree
	 * @param codeAgence : le code agence � tester
	 * @return false si le code agence est deja present dans la Banque
	 */
	public static boolean codeAgenceUnique(Banque banque, String codeAgence) {
		return !banque.getListeAgences().containsKey(codeAgence);
	}

	public static boolean hasClient(Banque banque) {
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			for (Entry<String, Conseiller> conseiller : agence.getValue().getListeConseillers().entrySet()) {
				if (ControleConseiller.hasClients(conseiller.getValue())) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean hasCompte(Banque banque) {
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			for (Entry<String, Conseiller> conseiller : agence.getValue().getListeConseillers().entrySet()) {
				for (Entry<String, Client> client : conseiller.getValue().getPortefeuilleClient().entrySet()) {
					if (ControleClient.isActif(client.getValue())
							&& ControleCompte.nbComptesActif(client.getValue()) > 0) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Controle si le numero de compte est deja present dans la banque
	 * 
	 * @param banque       : la Banque dans laquelle le compte est cree
	 * @param numeroCompte : le numero de compte � tester
	 * @return false si le numero de compte est deja present dans la Banque
	 */
	public static boolean numeroCompteUnique(Banque banque, String numeroCompte) {
		BanqueService bs = new BanqueService();
		return bs.rechercheCompte(numeroCompte, banque) == null;
	}
}
