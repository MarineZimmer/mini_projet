package fr.afpa.controle;

public class ControlePersonne {

	private ControlePersonne() {
		super();
	}

	/**
	 * controle si le login est un login Administrateur
	 * 
	 * @param login : la chaine de caracteres a controler
	 * @return : true si le login est un login Administrateur
	 */
	public static boolean isAdminitrateur(String login) {
		return login.matches("^ADM[0-9]{2}$");
	}

	/**
	 * controle si le login est un login Conseiller
	 * 
	 * @param login : la chaine de caracteres a controler
	 * @return : true si le login est un login Conseiller
	 */
	public static boolean isConseiller(String login) {
		return login.matches("^CO[0-9]{4}$");
	}

	/**
	 * controle si le login est un login Client
	 * 
	 * @param login : la chaine de caracteres a controler
	 * @return : true si le login est un login Client
	 */
	public static boolean isClient(String login) {
		return login.matches("^[A-Z]{2}[0-9]{6}$");
	}

}
