package fr.afpa.ihm;

import java.time.format.DateTimeFormatter;
import java.util.Map;

import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;

public class IhmClient {

	/**
	 * affichage info client, ainsi que ces compte
	 * 
	 * @param client : le Client
	 */
	public void affichageInfoClient(Client client) {
		IhmCompte ihmCompte = new IhmCompte();
		System.out.println("-------------------- Infos Client ---------------------------");
		System.out.println("Nom : " + client.getNom());
		System.out.println("Prenom : " + client.getPrenom());
		System.out.println("Mail : " + client.getMail());
		System.out.println("Date de naissance : " + client.getDateNaissance());
		System.out.println("Actif : " + client.isActif());
		System.out.println("Login : " + client.getLogin());
		System.out.println("Identifiant : " + client.getIdentifiant());
		for (Map.Entry<String, Compte> compte : client.getListeComptes().entrySet()) {
			ihmCompte.affichageInfoCompte(compte.getValue());
		}
		System.out.println("-----------------------------------------------------------------");
	}

	/**
	 * Affichage de la fiche client
	 * 
	 * @param client : le client
	 */
	public void affichageFicheClient(Client client) {
		StringBuilder infoClient = new StringBuilder();
		infoClient.append(String.format("%20s", "Fiche client"));
		infoClient.append("\n\nNum�ro client : " + client.getIdentifiant());
		infoClient.append("\nNom : " + client.getNom());
		infoClient.append("\nPrenom : " + client.getPrenom());
		infoClient.append(
				"\nDate de naissance :" + client.getDateNaissance().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		infoClient.append("\n\n_________________________________________________________");
		infoClient.append("\nListe de compte");
		infoClient.append("\n_________________________________________________________");
		infoClient.append(String.format("%23s", "\nNumero de compte"));
		infoClient.append(String.format("%20s", "solde"));
		infoClient.append("\n_________________________________________________________");

		for (Map.Entry<String, Compte> compte : client.getListeComptes().entrySet()) {
			if (compte.getValue().isActif()) {
				infoClient.append(String.format("%23s", "\n" + compte.getKey()));
				infoClient.append(String.format("%25s", compte.getValue().getSolde() + " euros"));
				if (compte.getValue().getSolde() > 0) {
					infoClient.append(String.format("%20s", ":-)"));
				} else {
					infoClient.append(String.format("%20s", ":-("));
				}
			}
		}

		System.out.println(infoClient);

	}
}
