package fr.afpa.ihm;

import java.util.Map.Entry;

import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;

public class IhmConseiller {

	/**
	 * Affichage info conseiller, et sa liste de clients
	 * 
	 * @param conseiller ; le Conseiller que l'on veut afficher les infos
	 */
	public void affichageInfoConseiller(Conseiller conseiller) {
		System.out.println("-------------------- Infos Conseiller ---------------------------");
		System.out.println("Nom : " + conseiller.getNom());
		System.out.println("Prenom : " + conseiller.getPrenom());
		System.out.println("Login/identifiant : " + conseiller.getLogin());
		for (Entry<String, Client> client : conseiller.getPortefeuilleClient().entrySet()) {
			System.out.println("\tClient : " + client.getValue().getNom() + "identifiant : " + client.getKey()
					+ "nb Compte : " + client.getValue().getListeComptes().size());
		}
		System.out.println("-----------------------------------------------------------------");
	}
}
