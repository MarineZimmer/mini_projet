package fr.afpa.igTestImage.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class JPanelImage extends JPanel implements Runnable {
	public BufferedImage imageFond;
	public int y;

	public JPanelImage(String fileName)  {
		setLayout(null);
		y=0;
		try {
			imageFond = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	//	g.drawImage(imageFond, 0, 0, null);
		g.drawImage(imageFond.getSubimage(0, y, this.getHeight(), this.getHeight()), 0, 0, null);
		
	}
	
	@Override
	public void run() {
		int t= 500;
	while(true) {
		
		y+=10;
		this.repaint();
		if(y>imageFond.getHeight()-getHeight()-10)
			y=0;
		if(t>10)
			t-=5;
		try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
	
}
