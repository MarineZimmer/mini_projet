package fr.afpa.gesper.main;

import java.nio.channels.SeekableByteChannel;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.gesper.entitypersistante.Adresse;
import fr.afpa.gesper.entitypersistante.Compte;
import fr.afpa.gesper.entitypersistante.Personne;
import fr.afpa.gesper.ihms.MenuIhm;
import fr.afpa.gesper.utils.HibernateUtils;

/**
 * Hello world!
 *
 */
public class App {
	private static Session session = null;
	private static Scanner in;

	/**
	 * @return the session
	 */
	public static Session getSession() {
		return session;
	}

	/**
	 * @return the in
	 */
	public static Scanner getIn() {
		return in;
	}

	public static void main(String[] args) {
		session = HibernateUtils.getSession();
		in = new Scanner(System.in);
		do {
			MenuIhm.affichageMenu();
		} while (MenuIhm.traitementMenu(in.nextLine()));
		session.close();
	}
}
