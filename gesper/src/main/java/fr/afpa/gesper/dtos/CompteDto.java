package fr.afpa.gesper.dtos;

import fr.afpa.gesper.daos.CompteDao;
import fr.afpa.gesper.entitypersistante.Compte;
import fr.afpa.gesper.entitypersistante.Personne;

public class CompteDto {

	/**
	 * lien entre le service compte et le dto compte pour la sauvegarde d'un compte
	 * @param compte : le compte à sauvegarder
	 * @return true si le compte a été sauvegardé, false sinon
	 */
	public Compte saveCompte(fr.afpa.gesper.entitymetier.Compte compte) {
		Compte comptePersistant = new Compte(compte.getLogin(), compte.getMdp());
		return new CompteDao().save(comptePersistant);
	}

	/**
	 * lien entre le service compte et le dto compte pour la mise à jour d'un compte
	 * @param compte : le compte à mettre à jour
	 * @return true si le compte a été mis à jour, false sinon
	 */
	public boolean updateCompte(Personne personne) {
		return new CompteDao().updateCompte(personne);
	}

	/**
	 * lien entre le service compte et le dto compte pour la suppression d'un compte
	 * @param compte : le compte à supprimer
	 * @return true si le compte a été supprimer, false sinon
	 */
	public boolean  deleteCompte(Compte compteSupprimer) {
		 return new CompteDao().deleteCompte(compteSupprimer);
	}

}
