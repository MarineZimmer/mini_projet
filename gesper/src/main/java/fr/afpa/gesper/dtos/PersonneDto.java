package fr.afpa.gesper.dtos;

import java.util.List;

import fr.afpa.gesper.daos.PersonneDao;
import fr.afpa.gesper.entitypersistante.Compte;
import fr.afpa.gesper.entitypersistante.Personne;

public class PersonneDto {

	/**
	 * lien entre le service personne et le dto personne pour la sauvegarde d'une personne
	 * @param personne : la personne à sauvegarder
	 * @return true si la personne a été sauvegardée, false sinon
	 */
	public boolean savePersonne(fr.afpa.gesper.entitymetier.Personne personne,Compte compte) {
		Personne personnePersistant = new Personne(personne.getNom(), personne.getPrenom());
		personnePersistant.setCompte(compte);
		return new PersonneDao().savePersonne(personnePersistant);
	}

	/**
	 * lien entre le service personne et le dto personne pour la recupération de la liste de toutes les personnes
	 * @return la liste de toutes les personnes
	 */
	public List<Personne> getAllPersonnes() {
		return new PersonneDao().getAllPersonnes();
		
	}

	/**
	 * lien entre le service personne et le dto personne pour la suppression d'une personne
	 * @param personne : la personne à supprimer
	 * @return true si la personne a été supprimer, false sinon
	 */
	public boolean deletePersonne(Personne personne) {
		return new PersonneDao().deletePersonne(personne);
	}

	/**
	 * lien entre le service personne et le dto personne pour la recherche d'une personne
	 * @param id : l'id de la personne recherchée
	 * @return la personne rechechée ou null si elle n'existe pas en bdd
	 */
	public Personne getPersonne(int id) {
		return new PersonneDao().getPersonne(id);
	}

	/**
	 * lien entre le service personne et le dto personne pour la mise à jour d'une personne
	 * @param personne : la personne à mettre à jour
	 * @return true si la personne a été mis à jour, false sinon
	 */
	public boolean updatePersonne(Personne personne) {
		return new PersonneDao().updatePersonne(personne);
		
	}

}
