package fr.afpa.gesper.dtos;

import fr.afpa.gesper.daos.AdresseDao;

import fr.afpa.gesper.entitypersistante.Adresse;
import fr.afpa.gesper.entitypersistante.Personne;

public class AdresseDto {

	/**
	 * lien entre le service adresse et le dto adresse pour la sauvegarde d'une adresse
	 * @param adresse : l'adresse à sauvegarder
	 * @return true si l'adresse a été sauvegardée, false sinon
	 */
	public boolean saveAdresse(fr.afpa.gesper.entitymetier.Adresse adresse, Personne personne) {
		Adresse adressePersistante = new Adresse(adresse.getNumero(), adresse.getRue(), adresse.getVille(), personne);
		return  new AdresseDao().saveAdresse(adressePersistante);
		
	}

	/**
	 * lien entre le service adresse et le dto adresse pour la recupération d'une adresse
	 * @param id : l'id de l'adresse à récupérer
	 * @return l'adresse rechechée ou null si elle n'existe pas en bdd
	 */
	public Adresse getAdresse(int id) {
		return new AdresseDao().getAdresse(id);
	}

	/**
	 * lien entre le service adresse et le dto adresse pour la mise à jour d'une adresse
	 * @param adresse : l'adresse à mettre à jour
	 * @return true si l'adresse a été mise à jour, false sinon
	 */
	public boolean updateAdresse(Adresse adresse) {
		return new AdresseDao().updateAdresse(adresse);
	}

	/**
	 * lien entre le service adresse et le dto adresse pour la suppression d'une adresse
	 * @param personne : l'adresse à supprimer
	 * @return true si l'adresse a été supprimé, false sinon
	 */
	public boolean deleteAdresse(Adresse adresse) {
		return new AdresseDao().deleteAdresse(adresse);
	}

}
