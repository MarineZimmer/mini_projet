package fr.afpa.gesper.entitymetier;




import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class Adresse {
	
	
	private int numero;
	private String rue;
	private String Ville;
	


	public Adresse(int numero, String rue, String ville) {
		super();
		this.numero = numero;
		this.rue = rue;
		Ville = ville;
	}


	public Adresse() {
		super();
	}
	
	
	

}
