package fr.afpa.gesper.ihms;

import fr.afpa.gesper.controles.ControleSaisie;
import fr.afpa.gesper.entitypersistante.Adresse;
import fr.afpa.gesper.entitypersistante.Compte;
import fr.afpa.gesper.entitypersistante.Personne;
import fr.afpa.gesper.main.App;
import fr.afpa.gesper.services.AdresseService;
import fr.afpa.gesper.services.PersonneService;

public class AdresseIhm {
	
	/**
	 * ihm de création d'une adresse
	 * @param personne la personne domiciliée a l'adresse créée
	 */
	public static void creationAdresse(Personne personne) {
		String numero;
		String rue;
		String ville;
		
		System.out.print("Numero  : ");
		numero=App.getIn().nextLine();
		while(!ControleSaisie.saisieInt(numero)) {
			System.out.println("Veuillez entrer un nombre");
			numero=App.getIn().nextLine();
		}
		System.out.print("Rue  : ");
		rue=App.getIn().nextLine();
		while(!ControleSaisie.saisieNonVide(rue)) {
			System.out.println("Veuillez entrer une chaine non vide");
			rue=App.getIn().nextLine();
		}
		
		System.out.print("Ville  : ");
		ville=App.getIn().nextLine();
		while(!ControleSaisie.saisieNonVide(ville)) {
			System.out.println("Veuillez entrer une chaine non vide");
			ville=App.getIn().nextLine();
		}
		new AdresseService().creationAdresse(personne, Integer.parseInt(numero), rue,ville);
	}
	
	/**
	 * ihm d'affichage d'une adresse
	 */
	public static void afficherAdresse(Adresse adresse) {
		System.out.println(" id adresse : " + adresse.getIdAd() + " Adresse : " + adresse.getNumero() +" "+ adresse.getRue() + " "+ adresse.getVille());
	}

	/**
	 * ihm de suppression d'une adresse
	 * @param personne la personne qui souhaite supprimer une adresse
	 */
	public static void  suppressionAdresse(Personne personne) {
		Adresse adresse = choixAdresse(personne) ;
		if(adresse!=null) {
			if(new AdresseService().supprimerAdresse(personne, adresse)) {
				System.out.println("La suppression a bien été prise en compte");	
			}else {
				System.out.println("Erreur lors de la suppression");
			}
		}else {
		System.out.println("la personne n'est pas domicilié à cette addresse");
		}
	}
	
	/**
	 * ihm de modification d'une adresse
	 * @param personne la personne qui souhaite modifier une adresse
	 * @return
	 */
	public static boolean modifierAdresse(Personne personne) {
		Adresse adresse = choixAdresse(personne) ;
		if(adresse!=null) {
			String numero;
			String rue;
			String ville;
			
			System.out.print("Entrer le nouveau numero (si vous ne souhaitez pas modifier le numero appuyer sur la touche entrée): ");
			numero=App.getIn().nextLine();
			System.out.print("Entrer la nouvelle rue (si vous ne souhaitez pas modifier la rue appuyer sur la touche entrée): ");
			rue=App.getIn().nextLine();
			System.out.print("Entrer la nouvelle ville (si vous ne souhaitez pas modifier la ville appuyer sur la touche entrée): ");
			ville=App.getIn().nextLine();
			if(new AdresseService().modifierAdresse(numero, rue,ville, adresse)) {
				System.out.println("La modification a bien été prise en compte");
			}else {
				System.out.println("Erreur lors de la modification");
			}
		}
		return false;
	}
	
	/**
	 * ihm de choix d'une adresse d'une personne
	 * @param personne la personne qui souhaite choisir une  de ses adresses
	 * @return l'adresse choisie, null si elle n'existe pas
	 */
	public static Adresse choixAdresse(Personne personne) {
		String id;
		System.out.println("Identifiant de l'adresse : ");
		id = App.getIn().nextLine();
		if (ControleSaisie.saisieInt(id)) {
			return new AdresseService().getAdressePersonne(Integer.parseInt(id), personne);
		}
		return null;
	}

}
