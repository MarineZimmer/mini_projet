package fr.afpa.gesper.ihms;

import fr.afpa.gesper.controles.ControleSaisie;
import fr.afpa.gesper.entitypersistante.Compte;
import fr.afpa.gesper.entitypersistante.Personne;
import fr.afpa.gesper.main.App;
import fr.afpa.gesper.services.CompteService;

public class CompteIhm {

	/**
	 * ihm de création d'un compte
	 * @return le compte créé
	 */
	public Compte creationCompte() {
		String login;
		String mdp;
		System.out.println("LOGIN : ");
		login=App.getIn().nextLine();
		while(!ControleSaisie.saisieNonVide(login)) {
			System.out.print("Entrer un login non vide : ");
			login=App.getIn().nextLine();
		}
		System.out.println("MDP : ");
		mdp=App.getIn().nextLine();
		while(!ControleSaisie.saisieNonVide(mdp)) {
			System.out.print("Entrer un mot de passe non vide : ");
			mdp=App.getIn().nextLine();
		}
		return new CompteService().creationCompte(login,mdp);
		
	}

	/**
	 * ihm de modification d'un compte
	 * @param personne la personne qui souhaite modifier son compte
	 */
	public static void modifInfoCompte(Personne personne) {
		String login;
		String mdp;
		System.out.print("Entrer le nouveau login (si vous ne souhaitez pas modifier le numero appuyer sur la touche entrée): ");
		
		login=App.getIn().nextLine();
		
		System.out.print("Entrer le nouveau mot de passe (si vous ne souhaitez pas modifier le numero appuyer sur la touche entrée): ");
		
		mdp=App.getIn().nextLine();
		
		 new CompteService().modificationCompte(personne,login,mdp);
		
	}

}
