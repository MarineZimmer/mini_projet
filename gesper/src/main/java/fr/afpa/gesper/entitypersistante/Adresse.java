package fr.afpa.gesper.entitypersistante;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@Entity
public class Adresse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private int idAd;
	@Column
	private int numero;
	@Column
	private String rue;
	@Column
	private String Ville;
	
	
	@ToString.Exclude
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="id_pers")
	private Personne pers;


	public Adresse(int numero, String rue, String ville, Personne pers) {
		super();
		this.numero = numero;
		this.rue = rue;
		Ville = ville;
		this.pers = pers;
	}


	public Adresse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
