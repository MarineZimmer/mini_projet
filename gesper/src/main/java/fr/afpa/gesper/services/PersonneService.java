package fr.afpa.gesper.services;

import java.util.List;

import fr.afpa.gesper.dtos.PersonneDto;
import fr.afpa.gesper.entitypersistante.Compte;
import fr.afpa.gesper.entitypersistante.Personne;

public class PersonneService {
	
	/**
	 * service de création d'une personne
	 * @param nom le nom de la personne
	 * @param prenom le prenom de la personne
	 * @param compte le compte de la personne
	 * @return true si la personne a été créée
	 */
	public boolean creationPersonne(String nom, String prenom, Compte compte) {
		fr.afpa.gesper.entitymetier.Personne personne = new fr.afpa.gesper.entitymetier.Personne(nom,prenom);
		return new PersonneDto().savePersonne(personne,compte); 
	}
	
	/**
	 * service de récupération de toutes les personnes
	 * @return une liste de toutes les personnes
	 */
	public List<Personne> getAllPersonnes(){
		return new PersonneDto().getAllPersonnes(); 
	}
	
	/**
	 * service de récupération d'une personne
	 * @param id l'identifiant de la personne recherchée
	 * @return la personne recherchée, null si aucune personne a été trouvé
	 */
	public Personne getPersonne(int id){
		return new PersonneDto().getPersonne(id); 
	}
	
	/**
	 * service de suppression d'une personne
	 * @param personne la personne à supprimer
	 * @return true si la personne a été supprimé,false sinon
	 */
	public boolean deletePersonne(Personne personne) {
		if(personne!=null) {
		return new PersonneDto().deletePersonne(personne);
		}
		return false;
	}

	/**
	 * sevice de modification d'une personne 
	 * @param nom : le nouveau nom (chaine vide si nom non modifié)
	 * @param prenom : le nouveau prenom (chaine vide si nom non modifié)
	 * @param personne : la personne à modifier
	 * @return true si la modification de la personne a été effectué, false sinon
	 */
	public boolean modifierPersonne(String nom, String prenom, Personne personne) {
		if(!"".equals(nom)) {
			personne.setNom(nom);
		}
		if(!"".equals(prenom)) {
			personne.setPrenom(prenom);
		}
		return new PersonneDto().updatePersonne(personne); 
		
	}

}
