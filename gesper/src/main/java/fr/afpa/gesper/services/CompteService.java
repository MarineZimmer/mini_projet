package fr.afpa.gesper.services;

import fr.afpa.gesper.daos.CompteDao;
import fr.afpa.gesper.dtos.CompteDto;
import fr.afpa.gesper.entitypersistante.Compte;
import fr.afpa.gesper.entitypersistante.Personne;

public class CompteService {

	/**
	 * service de création d'un compte
	 * @param login : le login du compte
	 * @param mdp : le mot de passe du compte
	 * @return le compte créé
	 */
	public Compte creationCompte(String login, String mdp) {
		fr.afpa.gesper.entitymetier.Compte compte = new fr.afpa.gesper.entitymetier.Compte(login, mdp);
		return new  CompteDto().saveCompte(compte);
		
	}

	/**
	 * service de modification d'un compte
	 * @param personne la personne propriétaire du compte
	 * @param login le nouveau login (chaine vide si le login n'est pas modifié)
	 * @param mdp le mot de passe (chaine vide si le mot de passe n'est pas modifié)
	 * @return true si la modification a été effectuée
	 */
	public boolean modificationCompte(Personne personne, String login, String mdp) {
		Compte compteSupprimer=null;
		boolean retour;
		if(!"".equals(login) && !personne.getCompte().getLogin().equals(login)) {
			 compteSupprimer =  personne.getCompte();
			Compte compte = new Compte(login, personne.getCompte().getMdp());
			personne.setCompte(compte);
		}
		if(!"".equals(mdp)) {
			personne.getCompte().setMdp(mdp);
		}
		 retour = new CompteDto().updateCompte(personne);
		 if(compteSupprimer!=null) {
		 new CompteDto().deleteCompte(compteSupprimer);
		 }
		 return retour;
	}

}
