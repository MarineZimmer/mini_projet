package fr.afpa.gesper.daos;

import org.hibernate.Transaction;

import fr.afpa.gesper.entitypersistante.Adresse;
import fr.afpa.gesper.main.App;

public class AdresseDao {

	/**
	 * sauvegarde une nouvelle adresse dans la bdd
	 * 
	 * @param adresse : l'adresse à sauvegarder
	 * @return true si la sauvegarde a été éffectuée, false sinon
	 */
	public boolean saveAdresse(Adresse adresse) {
		Transaction tx = null;
		try {
			tx = App.getSession().beginTransaction();

			App.getSession().saveOrUpdate(adresse);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;
	}

	/**
	 * supprime une adresse de la bdd
	 * 
	 * @param adresse l' adresse à supprimer
	 * @return true si la adresse a été supprimée, false sinon
	 */
	public boolean deleteAdresse(Adresse adresse) {
		Transaction tx = null;
		try {
			tx = App.getSession().beginTransaction();
			App.getSession().delete(adresse);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;

	}

	/**
	 * renvoie la adresse correspondant à l'identifiant en paramètre
	 * 
	 * @param id : l'id de la adresse recherchée
	 * @return la adresse correspondant à l'id entré en paramètre, ou null si elle
	 *         n'est pas présente en bdd
	 */
	public Adresse getAdresse(int id) {
		return App.getSession().get(Adresse.class, id);
	}

	/**
	 * mets à jour les informations d'une adresse en bdd
	 * 
	 * @param adresse : l' adresse à mettre à jour
	 * @return true si la mise à jour a été effectué
	 */
	public boolean updateAdresse(Adresse adresse) {

		Transaction tx = null;
		try {
			tx = App.getSession().beginTransaction();
			App.getSession().update(adresse);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;

	}

}
