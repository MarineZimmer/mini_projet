package fr.afpa.gesper.daos;

import org.hibernate.Transaction;

import fr.afpa.gesper.entitypersistante.Compte;
import fr.afpa.gesper.entitypersistante.Personne;
import fr.afpa.gesper.main.App;

public class CompteDao {

	/**
	 * sauvegarde une nouveau compte dans la bdd
	 * 
	 * @param adresse : le compte à sauvegarder
	 * @return true si la sauvegarde a été éffectuée, false sinon
	 */
	public Compte save(Compte compte) {
		Transaction tx = null;
		try {
			tx = App.getSession().beginTransaction();
			App.getSession().save(compte);
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			compte.setLogin("");
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return compte;

	}

	/**
	 * supprime un compte de la bdd
	 * 
	 * @param adresse le compte à supprimer
	 * @return true si le compte a été supprimé, false sinon
	 */
	public boolean deleteCompte(Compte compte) {

		Transaction tx = null;
		try {
			tx = App.getSession().beginTransaction();
			App.getSession().delete(compte);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;

	}

	/**
	 * mets à jour les informations d'un compte en bdd
	 * 
	 * @param personne : la personne propriétaire du compte à mettre à jour
	 * @return true si la mise à jour du compte a été effectué
	 */
	public boolean updateCompte(Personne personne) {

		Transaction tx=null;
		try {
			tx = App.getSession().beginTransaction();
			App.getSession().saveOrUpdate(personne.getCompte());
			App.getSession().saveOrUpdate(personne);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;

	}

}
