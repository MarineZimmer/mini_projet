package fr.afpa.gesper.daos;

import java.util.List;

import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.gesper.entitypersistante.Personne;
import fr.afpa.gesper.main.App;

public class PersonneDao {

	/**
	 * sauvegarde une personne dans la bdd
	 * @param personne : la personne à sauvegarder
	 * @return true si la sauvegarde a été éffectuée, false sinon
	 */
	public boolean savePersonne(Personne personne) {
		Transaction tx = null;
		try {
			tx = App.getSession().beginTransaction();
			App.getSession().save(personne);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;
	}

	/**
	 * supprime une personne de la bdd, ainsi que son compte et ses adresses
	 * @param personne la personne à supprimer
	 * @return true si la personne a été supprimée, false sinon
	 */
	public boolean deletePersonne(Personne personne) {
		Transaction tx = null;
		try {
			tx = App.getSession().beginTransaction();
			personne.getListeAdresse().forEach(App.getSession()::delete);
			App.getSession().delete(personne.getCompte());
			App.getSession().delete(personne);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;
	}

	/**
	 * renvoie la  personne correspondant à l'identifiant en paramètre
	 * @param id : l'id de la personne recherchée
	 * @return la personne correspondant à l'id entré en paramètre, ou null si elle n'est pas présente en bdd
	 */
	public Personne getPersonne(int id) {
		return App.getSession().get(Personne.class, id);
	}

	/**
	 * renvoie la liste de toutes les personnes 
	 * @return une liste de toutes les personnes
	 */
	public List<Personne> getAllPersonnes() {
		Query q = App.getSession().createQuery("from Personne");
		return (List<Personne>) q.list();
	}

	/**
	 * mets à jour les informations d'une personne en bdd
	 * @param personne : la personne à mettre à jour
	 * @return true si la mise à jour a été effectué
	 */
	public boolean updatePersonne(Personne personne) {
		Transaction tx = null;
		try {

			tx = App.getSession().beginTransaction();
			App.getSession().update(personne);
			tx.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null) {
				try {
					tx.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return false;
	}
}
