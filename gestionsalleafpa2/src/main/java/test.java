
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import fr.afpa.dao.entites.SalleDao;
import fr.afpa.dao.entites.TypeSalleDao;
import fr.afpa.metier.entites.Salle;
import fr.afpa.metier.services.ServiceFiltreSalle;
import fr.afpa.metier.services.ServiceSalle;
import fr.afpa.utils.HibernateUtils;

/**
 * Classe main pour tester la partie back
 *
 *
 */
public abstract class test {

	public static void main(String[] args) {

		// tests service authentification

		// System.out.println(new ServiceAuthentification().authentification("d", "d"));
		// System.out.println(new ServiceAuthentification().authentification("", ""));

		/*
		 * System.out.println(new ServiceAuthentification().authentification("adm",
		 * "admin")); System.out.println(new
		 * ServiceAuthentification().authentification("mb", "1234"));
		 * System.out.println(new ServiceAuthentification().authentification("gg",
		 * "1234")); System.out.println(new
		 * ServiceAuthentification().authentification("ff", "1234"));
		 */

		// test service recherche by login et nom
		/*
		 * System.out.println(new
		 * ServiceRecherche().rechercheByLoginOrNom("login","az"));
		 * System.out.println(new
		 * ServiceRecherche().rechercheByLoginOrNom("login","ff"));
		 * System.out.println(new
		 * ServiceRecherche().rechercheByLoginOrNom("login","zez"));
		 * 
		 * new ServiceRecherche().rechercheByLoginOrNom("nom","Z").forEach(System.out::
		 * println);
		 */
		// new ServiceSalle().getAllSalles().forEach(e->System.out.println(e.getNom()));

		/*
		 * Salle salle = new Salle(0, 54, "salle54", 12, 14, 5, true);
		 * salle.setBatiment(new Batiment(1, "")); salle.setTypeSalle(new TypeSalle(1,
		 * "")); List<Materiel> listeMateriel = new ArrayList<Materiel>();
		 * 
		 * 
		 * listeMateriel.add(new Materiel(0,salle,new TypeMateriel(1, ""),15));
		 * listeMateriel.add(new Materiel(0,salle,new TypeMateriel(2, ""),10));
		 * listeMateriel.add(new Materiel(0,salle,new TypeMateriel(3, ""),1));
		 * 
		 * salle.setListeMateriel(listeMateriel); System.out.println(new
		 * ServiceSalle().createSalle(salle));
		 */

		/*
		 * Salle salleR = new Salle(); salleR.setIdSalle(1); Reservation reservation =
		 * new Reservation(0, LocalDate.parse("2020-01-01"),
		 * LocalDate.parse("2020-02-02"), "testReservation",salleR);
		 * System.out.println(new ServiceReservation().createReservation(reservation));
		 */

		// List<Batiment> liste = new ServiceBatiment().getAll();
		// liste.forEach(e->System.out.println(e.getNom()));

	/*	Session s = HibernateUtils.getSession();
		CriteriaBuilder cb = s.getCriteriaBuilder();
		CriteriaQuery<SalleDao> q = cb.createQuery(SalleDao.class);
		Root<SalleDao> c = q.from(SalleDao.class);
		

		Predicate nom = cb.and(cb.like(c.get("nom"),"C%"));
		Predicate type = cb.and(cb.equal(c.get("typeSalleDao"), new TypeSalleDao(1,"",null)));
		//Predicate p = cb.and(nom);
	//	p=cb.and(p,nom);
		//q.select(c).where(cb.and(nom,type));
	//	q.select(c).where(cb.and(nom));
		q.where(cb.and(nom));
		q.where(cb.and(q.getRestriction(),type));
		//System.out.println(q.);
		
		q.select(c);
		TypedQuery<SalleDao> query = s.createQuery(q);
		
		
		
		
	//type de salle	
	Predicate typeSalle = cb.equal(c.get("typeSalleDao"), new TypeSalleDao(1,"",null));
	//type de salle	
		Predicate actif = cb.equal(c.get("actif"), true);
	
	//materiel quantite
	int id=1;
	int qtit=35;
	List<Salle> liste = new ServiceSalle().getAllSalles();
	List<Salle> liste2 = liste.stream().filter(e -> e.getListeMateriel().stream().anyMatch(m->m.getTypeMateriel().getIdTypeMateriel()==id && m.getQuantite()>=qtit)).collect(Collectors.toList());
	
		
		
		//List<SalleDao> liste = query.getResultList();
		liste2.forEach(e->System.out.println(e.getNom()));*/
		
		
		List<Salle> liste = new ServiceFiltreSalle().filtreSalle(LocalDate.parse("2020-03-03"), LocalDate.parse("2020-03-03"), true);
		liste.forEach(e->System.out.println(e.getNom()));
	
	}
}
