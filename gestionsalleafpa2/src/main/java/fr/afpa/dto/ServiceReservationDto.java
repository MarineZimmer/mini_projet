package fr.afpa.dto;

import fr.afpa.dao.ServiceReservationDao;
import fr.afpa.dao.entites.ReservationDao;
import fr.afpa.dao.entites.SalleDao;
import fr.afpa.metier.entites.Reservation;

public class ServiceReservationDto {

	public static Reservation reservationDaoToReservation(ReservationDao reservationDao) {
		Reservation reservation = new Reservation(reservationDao.getIdReservation(), reservationDao.getDateDebut(),
				reservationDao.getDateFin(), reservationDao.getNomReservation());
		//reservation.setSalle(ServiceSalleDto.salleDaoToSalle(reservationDao.getSalleDao()));
		return reservation;
	}

	public static ReservationDao reservationToReservationDao(Reservation reservation) {
		ReservationDao reservationDao = new ReservationDao(reservation.getIdReservation(), reservation.getDateDebut(),
				reservation.getDateFin(), reservation.getNomReservation());
		//reservationDao.setSalleDao(ServiceSalleDto.salleToSalleDao(reservation.getSalle()));
		reservationDao.setSalleDao(new SalleDao(reservation.getSalle().getIdSalle()));
		return reservationDao;
	}

	public boolean createReservation(Reservation reservation) {
		ReservationDao reservationDao = reservationToReservationDao(reservation);
		
		return new ServiceReservationDao().createReservation(reservationDao);
	}

}
