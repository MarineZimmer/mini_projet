package fr.afpa.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.afpa.dao.entites.MaterielDao;
import fr.afpa.dao.entites.TypeMaterielDao;
import fr.afpa.dao.persistance.ServiceTypeMaterielDao;
import fr.afpa.metier.entites.TypeMateriel;

public class ServiceTypeMaterielDto {
	
	public List<TypeMateriel> getAll() {
		List<TypeMaterielDao> listeTypeMaterielDao = new ServiceTypeMaterielDao().getAll();
		List<TypeMateriel> listeTypeMateriel = listeTypeMaterielDao.stream().map(ServiceTypeMaterielDto::typeMaterielDaoToTypeMateriel).collect(Collectors.toList());
		return listeTypeMateriel;
	}
	
	public static TypeMateriel typeMaterielDaoToTypeMateriel(TypeMaterielDao typeMaterielDao) {
		return new TypeMateriel(typeMaterielDao.getIdTypeMateriel(), typeMaterielDao.getLibelle());
	}
	
	
	public static TypeMaterielDao typeMaterielToTypeMaterielDao(TypeMateriel typeMateriel) {
		TypeMaterielDao typeMaterielDao =  new TypeMaterielDao();
		typeMaterielDao.setIdTypeMateriel(typeMateriel.getIdTypeMateriel());
		typeMaterielDao.setLibelle(typeMateriel.getLibelle());
		typeMaterielDao.setListeMAterielDao(new ArrayList<MaterielDao>());	
		return typeMaterielDao;
	}

}
