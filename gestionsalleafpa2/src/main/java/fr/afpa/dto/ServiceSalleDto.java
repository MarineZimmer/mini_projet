package fr.afpa.dto;

import java.util.List;
import java.util.stream.Collectors;

import fr.afpa.dao.entites.MaterielDao;
import fr.afpa.dao.entites.ReservationDao;
import fr.afpa.dao.entites.SalleDao;
import fr.afpa.dao.persistance.ServiceSalleDao;
import fr.afpa.metier.entites.Materiel;
import fr.afpa.metier.entites.Reservation;
import fr.afpa.metier.entites.Salle;

public class ServiceSalleDto {

	public static Salle salleDaoToSalle(SalleDao salleDao) {
		Salle salle = new Salle(salleDao.getIdSalle(), salleDao.getNumero(), salleDao.getNom(), salleDao.getSurface(), salleDao.getCapacite(), salleDao.getEtage(), salleDao.isActif());
		salle.setTypeSalle(ServiceTypeSalleDto.typeSalleDaoToTypeSalle(salleDao.getTypeSalleDao()));
		
		List<Materiel> listeMateriel = salleDao.getListeMaterielDao().stream().map(ServiceMaterielDto::materielDaoToMateriel).collect(Collectors.toList());
		List<Reservation> listeReservation = salleDao.getListeReservationDao().stream().map(ServiceReservationDto::reservationDaoToReservation).sorted((r1,r2) ->r1.getDateDebut().compareTo(r2.getDateDebut())).collect(Collectors.toList());
		salle.setBatiment(ServiceBatimentDto.batimentDaoToBatiment(salleDao.getBatimentDao()));
		salle.setListeMateriel(listeMateriel);
		salle.setListeReservation(listeReservation);
		return salle;
	}
	
	public static SalleDao salleToSalleDao(Salle salle) {
		SalleDao salleDao = new SalleDao(salle.getIdSalle(), salle.getNumero(), salle.getNom(), salle.getSurface(), salle.getCapacite(), salle.getEtage(), salle.isActif());
		
		salleDao.setTypeSalleDao(ServiceTypeSalleDto.typeSalleToTypeSalleDao(salle.getTypeSalle()));
		
		
		salleDao.setBatimentDao(ServiceBatimentDto.batimentToBatimentDao(salle.getBatiment()));
		
		List<MaterielDao> listeMaterielDao = salle.getListeMateriel().stream().map(ServiceMaterielDto::materielToMaterielDao).collect(Collectors.toList());
		//List<ReservationDao> listeReservationDao = salle.getListeReservation().stream().map(ServiceReservationDto::reservationToReservationDao).collect(Collectors.toList());
		
		salleDao.setListeMaterielDao(listeMaterielDao);
		//salleDao.setListeReservationDao(listeReservationDao);
		
		return salleDao;
	}

	public List<Salle> getAllSalles() {
		List<SalleDao> listeSalleDao = new ServiceSalleDao().getAllSalles();
		return listeSalleDao.stream().map(ServiceSalleDto::salleDaoToSalle).collect(Collectors.toList());
	}

	public boolean createSalle(Salle salle) {
		SalleDao salleDao = salleToSalleDao(salle);
		salleDao.getListeMaterielDao().forEach(e-> e.setSalleDao(salleDao));
		return new ServiceSalleDao().createSalle(salleDao);
	}

	public Salle getSalle(int id) {
		return salleDaoToSalle(new ServiceSalleDao().getSalle(id));
	}
}
