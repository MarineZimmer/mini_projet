package fr.afpa.servlets;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.metier.entites.Reservation;
import fr.afpa.metier.entites.Salle;
import fr.afpa.metier.services.ServiceReservation;

/**
 * Servlet implementation class CreationReservation
 */
@WebServlet(description = "enregistre la reservation", urlPatterns = { "/reserver/salle" })
public class CreationReservation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreationReservation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Reservation reservation = new Reservation(0, LocalDate.parse(request.getParameter("deb")), LocalDate.parse(request.getParameter("fin")), request.getParameter("nom"));
		Salle salle = new Salle();
		salle.setIdSalle(Integer.parseInt(request.getParameter("id")));
		reservation.setSalle(salle);
		
		boolean res = new ServiceReservation().createReservation(reservation);
		response.getWriter().append(res +" " +  reservation.getNomReservation());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
