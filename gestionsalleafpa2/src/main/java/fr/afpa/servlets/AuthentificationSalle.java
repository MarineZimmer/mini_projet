package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.constantes.Parametrage;
import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServiceAuthentification;

/**
 * Servlet implementation class AuthentificationSalle
 */
@WebServlet(description = "authentification pour la visualisation et la gestion des salles", urlPatterns = { "/authentification" })
public class AuthentificationSalle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthentificationSalle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher requestDispatcher;
		String uri = "index.jsp";
		requestDispatcher = request.getRequestDispatcher("/" + uri);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri;

		ServiceAuthentification servAuth = new ServiceAuthentification();
		Personne persAuth;

		uri = "index.jsp";

		// recuperation de la personne authentifier, null si l'authenfication a échoué
		persAuth = servAuth.authentification(request.getParameter("login"), request.getParameter("mdp"));

		if (persAuth != null ) { // la pers auth est un admin ou utilisateur
			uri = Parametrage.URI_VISUALISER_SALLE;
			request.getSession().setAttribute("persAuthSalle", persAuth);
			response.sendRedirect( uri);
		} else { // authentification incorrecte
			request.setAttribute("KO", "Erreur login ou mot de passe incorrects ");
			request.getSession().setAttribute("persAuthSalle", null);
			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		}
	}

}
