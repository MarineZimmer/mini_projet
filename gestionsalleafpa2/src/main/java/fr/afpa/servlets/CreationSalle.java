package fr.afpa.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.query.criteria.internal.expression.function.SubstringFunction;

import fr.afpa.metier.entites.Batiment;
import fr.afpa.metier.entites.Materiel;
import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.entites.Reservation;
import fr.afpa.metier.entites.Salle;
import fr.afpa.metier.entites.TypeMateriel;
import fr.afpa.metier.entites.TypeSalle;
import fr.afpa.metier.services.ServiceBatiment;
import fr.afpa.metier.services.ServiceSalle;
import fr.afpa.metier.services.ServiceTypeMateriel;
import fr.afpa.metier.services.ServiceTypeSalle;

/**
 * Servlet implementation class CreationSalle
 */
@WebServlet(description = "formulaire de creation salle", urlPatterns = { "/creation" })
public class CreationSalle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreationSalle() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = "";

		// verifie si une personne est bien authentifié pour accéder au menu
		if (request.getSession().getAttribute("persAuthSalle") instanceof Personne) {
			uri = "creationSalle.jsp";
			List<Batiment> listeBatiment = new ServiceBatiment().getAll();
			request.setAttribute("listeBatiment", listeBatiment);

			List<TypeSalle> listeTypeSalle = new ServiceTypeSalle().getAll();
			request.setAttribute("listeTypeSalle", listeTypeSalle);

			List<TypeMateriel> listeTypeMateriel = new ServiceTypeMateriel().getAll();
			request.setAttribute("listeTypeMateriel", listeTypeMateriel);

			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		} else { // non authentifier retour à la page authentification
			response.sendRedirect(request.getContextPath() + "/" + uri);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Salle salle = new Salle(0, Integer.parseInt(request.getParameter("numero")), request.getParameter("nom"),
				Float.parseFloat(request.getParameter("superficie")),
				Integer.parseInt(request.getParameter("capacite")), Integer.parseInt(request.getParameter("etage")),
				true);
		salle.setBatiment(new Batiment(Integer.parseInt(request.getParameter("batiment")), ""));
		salle.setTypeSalle(new TypeSalle(Integer.parseInt(request.getParameter("typeSalle")), ""));
		salle.setListeReservation(new ArrayList<Reservation>());

		Map<String, String[]> params = request.getParameterMap();
		List<Materiel> listeMateriel = new ArrayList<Materiel>();
		params.forEach((k, v) -> {
			if ("tm".equals(k.substring(0, 2))) {
				Materiel materiel = new Materiel(0, salle, new TypeMateriel(Integer.parseInt(k.substring(2)), ""),
						Integer.parseInt(v[0]));
				listeMateriel.add(materiel);
			}
		});

		salle.setListeMateriel(listeMateriel);
		
		boolean result = new ServiceSalle().createSalle(salle);
		System.out.println(result);
	}

}
