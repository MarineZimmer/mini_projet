package fr.afpa.servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.entites.Salle;
import fr.afpa.metier.entites.TypeMateriel;
import fr.afpa.metier.entites.TypeSalle;
import fr.afpa.metier.services.ServiceFiltreSalle;
import fr.afpa.metier.services.ServiceSalle;
import fr.afpa.metier.services.ServiceTypeMateriel;
import fr.afpa.metier.services.ServiceTypeSalle;

/**
 * Servlet implementation class ReserverSalle
 */
@WebServlet(description = "reservation d'une salle", urlPatterns = { "/reserver" })
public class ReserverSalle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReserverSalle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = "";

		// verifie si une personne est bien authentifié pour accéder au menu
		if (request.getSession().getAttribute("persAuthSalle") instanceof Personne) {
			uri = "reserverSalle.jsp";

			if (request.getAttribute("listeSalle") == null) {
				List<Salle> listeSalle = new ArrayList<Salle>();
				request.setAttribute("listeSalle", listeSalle);
			}

			List<TypeSalle> listeTypeSalle = new ServiceTypeSalle().getAll();
			request.setAttribute("listeTypeSalle", listeTypeSalle);

			List<TypeMateriel> listeTypeMateriel = new ServiceTypeMateriel().getAll();
			request.setAttribute("listeTypeMateriel", listeTypeMateriel);

			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		} else { // non authentifier retour à la page authentification
			response.sendRedirect(request.getContextPath() + "/" + uri);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = "visualisationSalle.jsp";
		
		request.setAttribute("dateDebut", request.getParameter("dateDebut"));
		request.setAttribute("dateFin", request.getParameter("dateFin"));
		request.setAttribute("nom", request.getParameter("nom")); 
		
		
		List<Salle> listeSalle = new ServiceSalle().getAllSalles();
		ServiceFiltreSalle servFiltreSalle = new ServiceFiltreSalle();
		//filtre date salle dispo
			listeSalle = servFiltreSalle.filtreDateSalle(listeSalle,
					LocalDate.parse(request.getParameter("dateDebut")),
					LocalDate.parse(request.getParameter("dateFin")), true);
		
		//filtre capacite
		if(!"".equals(request.getParameter("capacite"))){
			listeSalle = servFiltreSalle.filtreCapacite(listeSalle, Integer.parseInt(request.getParameter("capacite")));
					}
		
		//filtre type salle
		String[] tabTypeSalle = request.getParameterValues("typeSalle");
		if(tabTypeSalle!=null) {
		List<Integer> listeTypeSalle = Arrays.stream(tabTypeSalle).map(Integer::parseInt).collect(Collectors.toList());
		listeSalle = servFiltreSalle.filtreTypeSalle(listeSalle, listeTypeSalle);
		}
		
		//filtre materiel
		Map<String, String[]> params = request.getParameterMap();
		for (Entry<String, String[]> param : params.entrySet()) {
			if("tm".equals(param.getKey().substring(0,2)) && !"".equals(param.getValue()[0])){
				listeSalle = servFiltreSalle.filtreMateriel(listeSalle, Integer.parseInt(param.getKey().substring(2)), Integer.parseInt(param.getValue()[0]));
				}
		}
		
		request.setAttribute("listeSalle", listeSalle);
		doGet(request, response);
	}

}
