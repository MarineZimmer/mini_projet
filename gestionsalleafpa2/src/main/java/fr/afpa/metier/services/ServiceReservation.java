package fr.afpa.metier.services;

import fr.afpa.dto.ServiceReservationDto;
import fr.afpa.metier.entites.Reservation;

public class ServiceReservation {
	
	public boolean createReservation(Reservation reservation) {
		return new ServiceReservationDto().createReservation(reservation);
	}

}
