package fr.afpa.metier.services;

import java.util.List;

import fr.afpa.dto.ServiceBatimentDto;
import fr.afpa.metier.entites.Batiment;

public class ServiceBatiment {

	public List<Batiment> getAll() {
		return new ServiceBatimentDto().getAll();
	}
}
