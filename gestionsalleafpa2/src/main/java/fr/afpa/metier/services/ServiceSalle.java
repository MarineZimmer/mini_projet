package fr.afpa.metier.services;

import java.util.List;

import fr.afpa.dto.ServiceSalleDto;
import fr.afpa.metier.entites.Salle;

public class ServiceSalle {
	
	public boolean createSalle(Salle salle) {
		return new ServiceSalleDto().createSalle(salle);
		
	}
	
	
	public List<Salle> getAllSalles() {
		return new ServiceSalleDto().getAllSalles();
		
	}
	
	public Salle getSalle(int id) {
		return new ServiceSalleDto().getSalle(id);
		
	}

}
