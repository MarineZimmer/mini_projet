package fr.afpa.metier.services;

import java.util.List;

import fr.afpa.dto.ServiceTypeSalleDto;
import fr.afpa.metier.entites.TypeSalle;

public class ServiceTypeSalle {
	
	public List<TypeSalle> getAll() {
		return new ServiceTypeSalleDto().getAll();
	}

}
