package fr.afpa.metier.services;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.hibernate.jpa.event.spi.jpa.ExtendedBeanManager.LifecycleListener;

import fr.afpa.metier.entites.Salle;

public class ServiceFiltreSalle {

	public List<Salle> filtreSalle(LocalDate dateDebut,LocalDate dateFin, boolean libre){
		List<Salle> listeSalle = new  ServiceSalle().getAllSalles();
	
	
		return filtreDateSalle(listeSalle, dateDebut, dateFin, libre);
	}

	public List<Salle> filtreDateSalle(List<Salle> listeSalle, LocalDate dateDebut,LocalDate dateFin, boolean libre){
		List<Salle> liste2 = listeSalle.stream()
									.filter(s -> 
											(s.getListeReservation().stream()
													.allMatch(r->dateDebut.isAfter(r.getDateFin()) || dateFin.isBefore(r.getDateDebut())))==libre
									)
					.collect(Collectors.toList());
		
	
		return liste2;
	}
	
	public List<Salle> filtreTypeSalle(List<Salle> listeSalle, List<Integer> listeId){
		List<Salle> liste2 = listeSalle.stream()
									.filter(s -> 
											listeId.contains(s.getTypeSalle().getId()))
					.collect(Collectors.toList());
		
	
		return liste2;
	}
	
	public List<Salle> filtreMateriel(List<Salle> listeSalle, int id, int quantite){
		
		List<Salle> liste2 = listeSalle.stream().filter(e -> e.getListeMateriel().stream().anyMatch(m->m.getTypeMateriel().getIdTypeMateriel()==id && m.getQuantite()>=quantite)).collect(Collectors.toList());
		return liste2;
	}
	
	
public List<Salle> filtreCapacite(List<Salle> listeSalle, int min){
		
		List<Salle> liste2 = listeSalle.stream().filter(s->s.getCapacite()>=min).collect(Collectors.toList());
		return liste2;
	}
	
	
	
}
