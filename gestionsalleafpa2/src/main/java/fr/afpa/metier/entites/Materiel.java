package fr.afpa.metier.entites;

import fr.afpa.dao.entites.SalleDao;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Materiel {

	
	int id;
	Salle salle;
	TypeMateriel typeMateriel;
	int quantite;
	
	public Materiel(int id, int quantite) {
		super();
		this.id = id;
		this.quantite = quantite;
	}
	
	
}
