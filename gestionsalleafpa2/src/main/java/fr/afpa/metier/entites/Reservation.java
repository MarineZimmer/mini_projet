package fr.afpa.metier.entites;

import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation {
	
int idReservation;
	
	
	LocalDate dateDebut;
	
	LocalDate dateFin;
	
	String nomReservation;
	
	
	Salle salle;//????


	public Reservation(int idReservation, LocalDate dateDebut, LocalDate dateFin, String nomReservation) {
		super();
		this.idReservation = idReservation;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nomReservation = nomReservation;
	}
	
	

}
