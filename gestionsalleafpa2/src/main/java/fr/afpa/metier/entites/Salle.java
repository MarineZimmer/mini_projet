package fr.afpa.metier.entites;

import java.util.List;import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;


@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Salle {

	int idSalle;
	int numero;
	String nom;
	float surface;
	int capacite;
	int etage;
	boolean actif;
	TypeSalle typeSalle;
	Batiment batiment;
	List<Reservation> listeReservation;
	List<Materiel> listeMateriel;
	
	
	public Salle(int idSalle, int numero, String nom, float surface, int capacite, int etage, boolean actif) {
		super();
		this.idSalle = idSalle;
		this.numero = numero;
		this.nom = nom;
		this.surface = surface;
		this.capacite = capacite;
		this.etage = etage;
		this.actif = actif;
	}
	
	
	

}
