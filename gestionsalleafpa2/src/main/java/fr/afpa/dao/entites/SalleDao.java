package fr.afpa.dao.entites;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Proxy;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "salle")
@NamedQuery(name = "getAllSalle", query = "from SalleDao")
@NamedQuery(name = "getSalleId", query = "from SalleDao where idSalle=:id")
public class SalleDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "salle_generator")
	@SequenceGenerator(name = "salle_generator", sequenceName = "seq_salle", allocationSize = 1)
	@Column(name = "id_salle", updatable = false, nullable = false)

	int idSalle;

	@Column(name = "numero", updatable = true, nullable = false)
	int numero;
	@Column(name = "nom", updatable = true, nullable = false, length = 50)
	String nom;
	@Column(name = "surface", updatable = true, nullable = false)
	float surface;
	@Column(name = "capacite", updatable = true, nullable = false)
	int capacite;
	@Column(name = "etage", updatable = true, nullable = false)
	int etage;
	@Column(name = "actif", updatable = true, nullable = false)
	boolean actif;

	@ManyToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_type")
	TypeSalleDao typeSalleDao;

	@ManyToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_batiment")
	BatimentDao batimentDao;

	@OneToMany(mappedBy = "salleDao")
	List<ReservationDao> listeReservationDao;

	@OneToMany(mappedBy = "salleDao")
	List<MaterielDao> listeMaterielDao;

	public SalleDao(int idSalle, int numero, String nom, float surface, int capacite, int etage, boolean actif) {
		super();
		this.idSalle = idSalle;
		this.numero = numero;
		this.nom = nom;
		this.surface = surface;
		this.capacite = capacite;
		this.etage = etage;
		this.actif = actif;
	}

	public SalleDao(int idSalle) {
		this.idSalle = idSalle;
	}

	/*
	 * @JoinTable(name="posseder", joinColumns = @JoinColumn(name="id_salle"),
	 * inverseJoinColumns = @JoinColumn(name="id_type_materiel") )
	 * Set<TypeMaterielDao> setMaterielDao;
	 */

}
