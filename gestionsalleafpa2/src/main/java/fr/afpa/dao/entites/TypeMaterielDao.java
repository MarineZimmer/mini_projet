package fr.afpa.dao.entites;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Proxy;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table (name="type_materiel")
@NamedQuery(name = "getAllTypeMateriel", query = "from TypeMaterielDao")
public class TypeMaterielDao {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "type_materiel_generator")
	@SequenceGenerator(name = "type_materiel_generator", sequenceName = "seq_type_materiel",allocationSize = 1)
	@Column (name="id_type_materiel", updatable = false, nullable = false )
	int idTypeMateriel;
	
	@Column (name="libelle", updatable = true, nullable = false,length = 50)
	String libelle;
	
	@OneToMany(mappedBy = "typeMaterielDao")
	List<MaterielDao> listeMAterielDao;
	
	//@ManyToMany( mappedBy = "setMaterielDao")
	//Set<SalleDao> setSalleDao;

}
