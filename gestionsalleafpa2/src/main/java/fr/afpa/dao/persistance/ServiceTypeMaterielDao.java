package fr.afpa.dao.persistance;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.dao.entites.TypeMaterielDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceTypeMaterielDao {

	
	public List<TypeMaterielDao> getAll() {
		Session s = null;
		List<TypeMaterielDao> listeTypeMaterielDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("getAllTypeMateriel");
			if(q!=null) {
				listeTypeMaterielDao = q.list();
			}

		} catch (Exception e) {
			Logger.getLogger(ServiceTypeMaterielDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceTypeMaterielDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listeTypeMaterielDao;
	}

}
