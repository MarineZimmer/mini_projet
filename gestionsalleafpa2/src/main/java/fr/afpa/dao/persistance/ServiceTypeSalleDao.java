package fr.afpa.dao.persistance;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.dao.entites.TypeSalleDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceTypeSalleDao {
	
	public List<TypeSalleDao> getAll() {
		Session s = null;
		List<TypeSalleDao> listeTypeSalleDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("getAllTypeSalle");
			if(q!=null) {
				listeTypeSalleDao = q.list();
			}

		} catch (Exception e) {
			Logger.getLogger(ServiceTypeSalleDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceTypeSalleDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listeTypeSalleDao;
	}


}
