package fr.afpa.dao.persistance;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.dao.entites.BatimentDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceBatimentDao {

	public List<BatimentDao> getAll() {
		Session s = null;
		List<BatimentDao> listeBatimentDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("getAllBatiment");
			if(q!=null) {
				listeBatimentDao = q.list();
			}

		} catch (Exception e) {
			Logger.getLogger(ServiceBatimentDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceBatimentDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listeBatimentDao;
	}

}
