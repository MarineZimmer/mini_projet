package fr.afpa.donjonnicosammarine;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.donjonnicosammarine.entites.Joueur;
import fr.afpa.donjonnicosammarine.entites.Monstre;
import fr.afpa.donjonnicosammarine.entites.Personnage;
import fr.afpa.donjonnicosammarine.services.PersonnageService;

class PersonnageServiceTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAttaquer() {
		Personnage personnage = new Joueur("billy",12,12,12);
		Personnage personnage2 = new Monstre();
		PersonnageService personnageService = new PersonnageService();
		assertTrue(personnageService.attaquer(personnage, personnage2));
		assertNotNull(personnageService.attaquer(null, personnage2));
		assertNotNull(personnageService.attaquer(personnage, null));
		
	
		
	}

}
