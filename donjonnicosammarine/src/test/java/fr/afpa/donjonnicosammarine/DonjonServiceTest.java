package fr.afpa.donjonnicosammarine;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.donjonnicosammarine.entites.Donjon;
import fr.afpa.donjonnicosammarine.entites.PieceArriver;
import fr.afpa.donjonnicosammarine.entites.Salle;
import fr.afpa.donjonnicosammarine.services.DonjonService;

class DonjonServiceTest {


	@BeforeEach
	void init() {
	

	}

	@AfterEach
	void tearDown()  {
	
	}

	@Test
	void testCreationDonjonAleatoire() {
	DonjonService donjonService = new DonjonService();
	assertNotNull(donjonService.creationDonjonAleatoire(2,3,0,0));
	assertNotNull(donjonService.creationDonjonAleatoire(20,30,2,2));
	assertNull(donjonService.creationDonjonAleatoire(-1, 5, 2, 8));
	assertNull(donjonService.creationDonjonAleatoire(2, 1, -1, 5));
	assertNull(donjonService.creationDonjonAleatoire(2, -5, 1, 5));
	assertNull(donjonService.creationDonjonAleatoire(5, 1, 1, -5));
	assertNull(donjonService.creationDonjonAleatoire(5, 0, 1, 5));
	assertNull(donjonService.creationDonjonAleatoire(8, 0, 0, 0));
	
	

	}

	@Test
	void testCalculChemin() {
		
	
		DonjonService donjonService = new DonjonService();
		Donjon donjon = donjonService.creationDonjon();
		Salle salleA = donjonService.calculChemin(donjon);
		assertTrue(salleA instanceof PieceArriver);	
			assertEquals(1,salleA.getCoordonneeX());
			assertEquals(5,salleA.getCoordonneeY());
			
	}


	@Test
	void testGenerateLabyrinthe() {
			
		}

	@Test
	void testMurACasse() {
		Donjon donjon = new Donjon(3,3,2,2);
		DonjonService donjonService = new DonjonService();
		List<String>testMurACasser = new ArrayList<String>();
		testMurACasser.add("B");
		testMurACasser.add("D");
		assertEquals(testMurACasser,donjonService.murACasse(donjon, donjon.getLabyrinthe()[0][0]) );
		testMurACasser.clear();
		testMurACasser.add("H");
		testMurACasser.add("G");
		assertEquals(testMurACasser,donjonService.murACasse(donjon, donjon.getLabyrinthe()[2][2]) );
		testMurACasser.clear();
		assertEquals(testMurACasser, donjonService.murACasse(null, null));
		assertEquals(testMurACasser, donjonService.murACasse(donjon, null));


		
		
	}

	@Test
	void testCreationDonjon() {
		DonjonService donjonService = new DonjonService();
		assertNotNull(donjonService.creationDonjon());
	}

	@Test
	void testAjoutMonstreEtObjet() {
		Donjon donjon = new Donjon(3,3,2,2);
		DonjonService donjonService = new DonjonService();
		assertFalse(donjonService.ajoutMonstreEtObjet(donjon, null));
		assertFalse(donjonService.ajoutMonstreEtObjet(null, donjon.getLabyrinthe()[1][1]));
		assertTrue(donjonService.ajoutMonstreEtObjet(donjon, donjon.getLabyrinthe()[1][1]));

	
		
	}

		}
