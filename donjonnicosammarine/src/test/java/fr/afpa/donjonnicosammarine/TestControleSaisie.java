package fr.afpa.donjonnicosammarine;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import fr.afpa.donjonnicosammarine.controles.ControleSaisie;

public class TestControleSaisie {

	@Test
	public void testSaisieInt() {
		assertTrue(ControleSaisie.saisieInt("2", 2, 5), "controle saisie int,(nb=min) incorrecte");
		assertTrue(ControleSaisie.saisieInt("5", 2, 5), "controle saisie int,(nb=max) incorrecte");
		assertTrue(ControleSaisie.saisieInt("3", 2, 5), "controle saisie int incorrecte");
		
		assertFalse(ControleSaisie.saisieInt("1", 2, 5), "controle saisie int,(nb trop petit) incorrecte");
		assertFalse(ControleSaisie.saisieInt("6", 2, 5), "controle saisie int,(nb trop grand) incorrecte");
		assertFalse(ControleSaisie.saisieInt("r", 2, 5), "controle saisie int, (saisie lettre) incorrecte");
		
	}
	
	@Test
	public void testSaisieNom() {
		assertTrue(ControleSaisie.saisieNom("null pointer"), "controle saisie nom incorrecte");
		assertFalse(ControleSaisie.saisieNom(""), "controle saisie nom (chaine vide) incorrecte");
		assertFalse(ControleSaisie.saisieNom("       "), "controle saisie nom (que des espaces) incorrecte");
		assertFalse(ControleSaisie.saisieNom(null), "controle saisie nom (null) incorrecte");
		
		
	}
}