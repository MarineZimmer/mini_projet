package fr.afpa.donjonnicosammarine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.donjonnicosammarine.entites.Piece;
import fr.afpa.donjonnicosammarine.services.AleatoireService;
import fr.afpa.donjonnicosammarine.services.JeuService;

public class AleatoireServiceTest {
	

	@Test
	public void testAleatoireInt() {
	AleatoireService aleatoireService = new AleatoireService();
	int nb = aleatoireService.aleatoireInt(2, 4);
	assertTrue(nb>=2 && nb<=4, "aléatoire int incorrect");
	
	}
}
