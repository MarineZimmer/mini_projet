package fr.afpa.donjonnicosammarine;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.donjonnicosammarine.entites.Donjon;
import fr.afpa.donjonnicosammarine.entites.Joueur;
import fr.afpa.donjonnicosammarine.entites.Piece;
import fr.afpa.donjonnicosammarine.entites.PieceArriver;
import fr.afpa.donjonnicosammarine.entites.Salle;
import fr.afpa.donjonnicosammarine.services.JoueurService;

public class JoueurServiceTest {

	private JoueurService joueurService;
	private Salle salle;
	private Donjon donjon;
	
	@BeforeEach
	  void init() {
		joueurService = new JoueurService();
	    salle=new Piece(0, 0, true, true, false, false, true);
	    ((Piece)salle).setJoueur(new Joueur("", 5, 5, 5));
	    donjon = new Donjon(3, 3, 2, 2);
	    
	  }
	
	@AfterEach
	  void tearDown() {
		joueurService=null;
	    salle=null;
	  }
	
	
	
	@Test
	public void testCreationJoueur() {
		joueurService = new JoueurService();
		assertNotNull(joueurService.creationJoueur("toto"));
	}
	
	@Test
	public void testJeuContinu() {
		joueurService = new JoueurService();
	    salle=new Piece(0, 0, true, true, false, false, true);
	    ((Piece)salle).setJoueur(new Joueur("", 5, 5, 5));
	    donjon = new Donjon(3, 3, 2, 2);
		assertTrue(joueurService.jeuContinu(salle), " jeu continu incorrect");
		((Piece)salle).getJoueur().setPointDeVie(0);
		assertFalse(joueurService.jeuContinu(salle),"arret mort incorrect");
		salle=new PieceArriver();
		((Piece)salle).setJoueur(new Joueur("", 5, 5, 5));
		assertFalse(joueurService.jeuContinu(salle),"arret gagne incorrect");
		((Piece)salle).setJoueur(null);
		assertFalse(joueurService.jeuContinu(salle),"arret gagne incorrect");
		assertTrue(joueurService.jeuContinu(null),"arret gagne incorrect");
	}
	
	@Test 
	public void testDeplacer(){
		joueurService = new JoueurService();
		donjon = new Donjon(4, 4, 5, 5);
		assertNull(joueurService.deplacer(donjon, donjon.getLabyrinthe()[0][0], "Droite"));
		donjon.getLabyrinthe()[0][0].setDepD(true);
		assertEquals(donjon.getLabyrinthe()[0][1], joueurService.deplacer(donjon, donjon.getLabyrinthe()[0][0], "Droite"));
		assertNull(joueurService.deplacer(donjon, donjon.getLabyrinthe()[0][0], "Gauche"));
		donjon.getLabyrinthe()[1][2].setDepG(true);
		assertEquals(donjon.getLabyrinthe()[1][1], joueurService.deplacer(donjon, donjon.getLabyrinthe()[1][2], "Gauche"));
		
		assertNull(joueurService.deplacer(donjon, donjon.getLabyrinthe()[1][0], "Haut"));
		donjon.getLabyrinthe()[1][0].setDepH(true);
		assertEquals(donjon.getLabyrinthe()[0][0], joueurService.deplacer(donjon, donjon.getLabyrinthe()[1][0], "Haut"));
		assertNull(joueurService.deplacer(donjon, donjon.getLabyrinthe()[0][2], "Bas"));
		donjon.getLabyrinthe()[0][2].setDepB(true);
		assertEquals(donjon.getLabyrinthe()[1][2], joueurService.deplacer(donjon, donjon.getLabyrinthe()[0][2], "Bas"));
		
	}
	
	@Test
	public void testEnregistrerScores() {
		joueurService = new JoueurService();
		assertTrue(joueurService.enregistrementScore(new Joueur("test", 10, 10, 10), "src/test/ressources/enregistrerscore.txt"));
		assertFalse(joueurService.enregistrementScore(new Joueur("", 10, 10, 10), "src/test/ressources/enregistrerscore.txt"));
		assertFalse(joueurService.enregistrementScore(null, "src/test/ressources/enregistrerscore.txt"));
		
	}
}
