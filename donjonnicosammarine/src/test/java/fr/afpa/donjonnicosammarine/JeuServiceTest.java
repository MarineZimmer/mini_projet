package fr.afpa.donjonnicosammarine;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.donjonnicosammarine.entites.BourseOr;
import fr.afpa.donjonnicosammarine.entites.Monstre;
import fr.afpa.donjonnicosammarine.entites.Piece;
import fr.afpa.donjonnicosammarine.entites.Score;
import fr.afpa.donjonnicosammarine.services.FichierService;
import fr.afpa.donjonnicosammarine.services.JeuService;

public class JeuServiceTest {

	private JeuService jeuService;
	private Piece piece;

	@Test
	public void testActionJoueur() {
		jeuService = new JeuService();
		piece = new Piece(0, 0, false, false, false, false, true);
		List<String> action = new ArrayList<String>();
		action.add("Affichage de la carte en detail");
		action.add("Regarder");
		action.add("Attaquer");

		piece.getListeMonstres().add(new Monstre());
		assertEquals(action, jeuService.actionJoueur(piece), "actions salle monstre incorrect");
		piece.getListeMonstres().clear();
		action.clear();
		action.add("Affichage de la carte en detail");
		action.add("Regarder");
		action.add("Deplacer");
		assertEquals(action, jeuService.actionJoueur(piece), "actions salle deplacer incorrect");
		action.add("Objets");
		piece.getListeObjets().add(new BourseOr());
		assertEquals(action, jeuService.actionJoueur(piece), "actions salle deplacer objet incorrect");
	}

	@Test
	public void testDeplacementpossible() {
		jeuService = new JeuService();
		piece = new Piece(0, 0, false, false, false, false, true);
		List<String> deplacement = new ArrayList<String>();
		assertEquals(deplacement, jeuService.deplacementPossibleJoueur(piece), "aucun deplacement incorrect");
		deplacement.add("Haut");
		deplacement.add("Bas");
		piece.setDepH(true);
		piece.setDepB(true);
		assertEquals(deplacement, jeuService.deplacementPossibleJoueur(piece), "deplacement haut bas incorrect");
		deplacement.add("Gauche");
		deplacement.add("Droite");
		piece.setDepG(true);
		piece.setDepD(true);
		assertEquals(deplacement, jeuService.deplacementPossibleJoueur(piece),
				" deplacement toutes directions incorrect");
	}

	@Test
	public void testTopScores() {
		jeuService = new JeuService();
		FichierService fichierService = new FichierService();
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Fayak;10", false);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "23/11/2019;Seti;90", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Alexandra;80", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Chat;100", true);

		List<Score> listeScoreAttendu = new ArrayList<>();
		listeScoreAttendu.add(new Score(100, "24/11/2019", "Chat"));
		listeScoreAttendu.add(new Score(90, "23/11/2019", "Seti"));
		listeScoreAttendu.add(new Score(80, "24/11/2019", "Alexandra"));
		listeScoreAttendu.add(new Score(10, "22/11/2019", "Fayak"));

		assertEquals(listeScoreAttendu, jeuService.topScores(10, "src/test/ressources/sauvegardeTest.txt"),
				"top scores moins de 10 scores incorrect");

		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Fayak;3", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "23/11/2019;Seti;8", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Alexandra;7", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Chat;9", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Fayak;0", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "23/11/2019;Seti;5", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Alexandra;4", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Chat;6", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "24/11/2019;Chat;g6", true);
		fichierService.ecritureFichier("src/test/ressources/sauvegardetest.txt", "dfgre", true);

		listeScoreAttendu.add(new Score(9, "24/11/2019", "Chat"));
		listeScoreAttendu.add(new Score(8, "23/11/2019", "Seti"));
		listeScoreAttendu.add(new Score(7, "24/11/2019", "Alexandra"));
		listeScoreAttendu.add(new Score(6, "24/11/2019", "Chat"));
		listeScoreAttendu.add(new Score(5, "23/11/2019", "Seti"));
		listeScoreAttendu.add(new Score(4, "24/11/2019", "Alexandra"));

		assertEquals(listeScoreAttendu, jeuService.topScores(10, "src/test/ressources/sauvegardeTest.txt"),
				"top scores plus de 10 scores incorrect");
	}

}
