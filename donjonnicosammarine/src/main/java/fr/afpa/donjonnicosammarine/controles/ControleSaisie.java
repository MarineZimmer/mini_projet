package fr.afpa.donjonnicosammarine.controles;

public class ControleSaisie {

	private ControleSaisie() {
		super();
	}

	/**
	 * Controle si la chaine de caracteres en parametre est un entier compris entre
	 * le min et le max
	 * 
	 * @param saisieInt : chaine de caractères à controler
	 * @param min       : minimum
	 * @param max       : maximum
	 * @return : true si la chaine de caracteres en parametre est un entier compris
	 *         entre le min et le max, false sinon
	 */
	public static boolean saisieInt(String saisieInt, int min, int max) {
		int nombre;
		try {
			nombre = Integer.parseInt(saisieInt);
			if (nombre < min || nombre > max) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Controle si la chaine de caractères est non vide
	 * 
	 * @param nom la chaine à controler
	 * @return true si la chaine est non vide
	 */
	public static boolean saisieNom(String nom) {
		if (nom != null && nom.length() > 0 && !nom.matches("[ ]*")) {
			return true;
		}
		return false;
	}

}
