package fr.afpa.donjonnicosammarine.services;

import fr.afpa.donjonnicosammarine.entites.Donjon;
import fr.afpa.donjonnicosammarine.entites.Piece;

public class SalleService {

	/**
	 * Service qui renvoie les informations de la salle regardée
	 * 
	 * @param donjon        : le donjon
	 * @param pieceCourante : la pièce où se situe le joueur
	 * @param choix         : le choix de la salle à regarder
	 * @return le descriptif de la salle regardée
	 */
	public String regarderSalle(Donjon donjon, Piece pieceCourante, String choix) {
		if (donjon != null && pieceCourante != null&& choix!=null) {
			switch (choix) {
			case "Salle Courante":
				return pieceCourante.regarder();
			case "Haut":
				if (pieceCourante.isDepH()) {
					return ((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX() - 1][pieceCourante
							.getCoordonneeY()]).regarder();
				}
				break;
			case "Bas":
				if (pieceCourante.isDepB()) {
					return ((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX() + 1][pieceCourante
							.getCoordonneeY()]).regarder();
				}
				break;
			case "Gauche":
				if (pieceCourante.isDepG()) {
					return ((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX()][pieceCourante
							.getCoordonneeY() - 1]).regarder();
				}
				break;
			case "Droite":
				if (pieceCourante.isDepD()) {
					return ((Piece) donjon.getLabyrinthe()[pieceCourante.getCoordonneeX()][pieceCourante
							.getCoordonneeY() + 1]).regarder();
				}
				break;

			default:
				break;
			}
		}
		return "";
	}

}
