package fr.afpa.donjonnicosammarine.services;

import java.util.List;

import fr.afpa.donjonnicosammarine.entites.BanditManchot;
import fr.afpa.donjonnicosammarine.entites.BourseOr;
import fr.afpa.donjonnicosammarine.entites.Joueur;
import fr.afpa.donjonnicosammarine.entites.Objet;
import fr.afpa.donjonnicosammarine.entites.PotionForce;
import fr.afpa.donjonnicosammarine.entites.PotionSoin;

public class ObjetsService {

	/**
	 * service de création d'un objet
	 * aléatoire(PotionForce,PotionSoin,BourseOr,BanditManchot)
	 * 
	 * @return l'objet créé
	 */
	public Objet creationObjet() {
		AleatoireService aleatoireService = new AleatoireService();
		int typeObjet = aleatoireService.aleatoireInt(0, 3);
		switch (typeObjet) {
		case 0:
			return new PotionForce(aleatoireService.aleatoireInt(5, 20));
		case 1:
			return new PotionSoin(aleatoireService.aleatoireInt(5, 20));
		case 2:
			return new BourseOr(aleatoireService.aleatoireInt(5, 20));
		case 3:
			return new BanditManchot(aleatoireService.aleatoireInt(5, 20));
		default:
			break;
		}
		return null;

	}

	/**
	 * Service d'utilisation d'un objet
	 * @param listeObjets : la liste objets de la salle
	 * @param choix : le choix de l'objet à utiliser
	 * @param joueur : le joueur qui utilise l'objet
	 * @return true si l'objet a été utiliser, false sinon
	 */
	public boolean utilisationObjet(List<Objet> listeObjets, int choix, Joueur joueur) {
		if (listeObjets!=null && choix<listeObjets.size() && listeObjets.get(choix).utiliserObjet(joueur)){
			listeObjets.remove(choix);
				 return true;
			 }
		return false;		
	}
}
