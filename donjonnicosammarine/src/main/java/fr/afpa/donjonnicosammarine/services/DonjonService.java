package fr.afpa.donjonnicosammarine.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import fr.afpa.donjonnicosammarine.entites.Donjon;
import fr.afpa.donjonnicosammarine.entites.Joueur;
import fr.afpa.donjonnicosammarine.entites.Piece;
import fr.afpa.donjonnicosammarine.entites.PieceArriver;
import fr.afpa.donjonnicosammarine.entites.PieceDepart;
import fr.afpa.donjonnicosammarine.entites.Salle;

public class DonjonService {

	/**
	 * Creation d'un donjon aléatoirement
	 * 
	 * @param hauteur           : la hauteur du donjon
	 * @param largeur           : la largeur du donjon
	 * @param nbMonstreMaxSalle : le nombre max de monstres par salle
	 * @param nbObjetMaxSalle   : le nombre max d'objets par salle
	 * @return le donjon créer
	 */
	public Donjon creationDonjonAleatoire(int hauteur, int largeur, int nbMonstreMaxSalle, int nbObjetMaxSalle) {

		if (hauteur > 0 && largeur > 0 && nbMonstreMaxSalle >= 0 && nbObjetMaxSalle >= 0) {
			Donjon donjon = new Donjon(largeur, hauteur, nbMonstreMaxSalle, nbObjetMaxSalle);

			generateLabyrinthe(donjon, donjon.getLabyrinthe()[0][0]);

			for (int i = 0; i < donjon.getLabyrinthe().length; i++) {
				for (int j = 0; j < donjon.getLabyrinthe()[0].length; j++) {
					if ((i + j) % 4 == 3) {
						ajoutMonstreEtObjet(donjon, donjon.getLabyrinthe()[i][j]);
					}
				}
			}
			calculChemin(donjon);
			return donjon;
		}
		return null;
	}

	/**
	 * Calcul du chemin le plus long et transformation de la salle au bout du chemin
	 * le plus long en salle d'arriver
	 * 
	 * @param donjon : le donjon
	 * @return la salle de la sortie
	 */
	public Salle calculChemin(Donjon donjon) {
		if (donjon != null) {
			Salle[][] lab = donjon.getLabyrinthe();
			int x = 0;
			int y = 0;
			char dep = 'D';
			int compteur = 0;
			lab[x][y].setNbPassage(lab[x][y].getNbPassage() + 1);
			Salle salleArrive = lab[x][y];

			do {
				if (lab[x][y].getNbPassage() == 0) {
					compteur++;
					lab[x][y].setNbPas(compteur);
					if (lab[x][y].getNbPas() > salleArrive.getNbPas()
							&& (x == 0 || x == lab.length - 1 || y == 0 || y == lab[0].length - 1)) {
						salleArrive = lab[x][y];
					}
				} else {
					compteur = lab[x][y].getNbPas();
				}
				lab[x][y].setNbPassage(lab[x][y].getNbPassage() + 1);

				switch (dep) {
				case 'H':
					if (lab[x][y].isDepG()) {
						dep = 'G';
					} else if (lab[x][y].isDepH()) {
						dep = 'H';
					} else if (lab[x][y].isDepD()) {
						dep = 'D';
					} else if (lab[x][y].isDepB()) {
						dep = 'B';
					}
					break;
				case 'D':
					if (lab[x][y].isDepH()) {
						dep = 'H';
					} else if (lab[x][y].isDepD()) {
						dep = 'D';
					} else if (lab[x][y].isDepB()) {
						dep = 'B';
					} else if (lab[x][y].isDepG()) {
						dep = 'G';
					}
					break;
				case 'B':
					if (lab[x][y].isDepD()) {
						dep = 'D';
					} else if (lab[x][y].isDepB()) {
						dep = 'B';
					} else if (lab[x][y].isDepG()) {
						dep = 'G';
					} else if (lab[x][y].isDepH()) {
						dep = 'H';
					}

					break;
				case 'G':
					if (lab[x][y].isDepB()) {
						dep = 'B';
					} else if (lab[x][y].isDepG()) {
						dep = 'G';
					} else if (lab[x][y].isDepH()) {
						dep = 'H';
					} else if (lab[x][y].isDepD()) {
						dep = 'D';
					}
					break;
				default:
					break;
				}
				if (dep == 'H') {
					x--;
				} else if (dep == 'D') {
					y++;
				} else if (dep == 'B') {
					x++;
					dep = 'B';
				} else if (dep == 'G') {
					y--;
					dep = 'G';
				}

			} while (x != 0 || y != 0);

			Salle salle = new PieceArriver(salleArrive.getCoordonneeX(), salleArrive.getCoordonneeY(),
					salleArrive.isDepB(), salleArrive.isDepH(), salleArrive.isDepG(), salleArrive.isDepD(),
					salleArrive.isDecouverte());
			donjon.getLabyrinthe()[salleArrive.getCoordonneeX()][salleArrive.getCoordonneeY()] = salle;
			return salle;
		}
		return null;
	}

	/**
	 * Génération des chemins du labyrinthe
	 * 
	 * @param donjon : le donjon qui contient le labyrinthe
	 * @param salle  : la Salle courante d'où on veut créer le chemin
	 */
	public void generateLabyrinthe(Donjon donjon, Salle salle) {
		if (donjon != null && salle != null) {
			List<String> murACasse = murACasse(donjon, salle);
			Collections.shuffle(murACasse);
			salle.setDecouverte(true);

			for (String string : murACasse) {

				switch (string) {
				case "H":
					if (!donjon.getLabyrinthe()[salle.getCoordonneeX() - 1][salle.getCoordonneeY()].isDecouverte()) {
						salle.setDepH(true);
						donjon.getLabyrinthe()[salle.getCoordonneeX() - 1][salle.getCoordonneeY()].setDepB(true);
						generateLabyrinthe(donjon,
								donjon.getLabyrinthe()[salle.getCoordonneeX() - 1][salle.getCoordonneeY()]);
					}
					break;
				case "B":
					if (!donjon.getLabyrinthe()[salle.getCoordonneeX() + 1][salle.getCoordonneeY()].isDecouverte()) {
						salle.setDepB(true);
						donjon.getLabyrinthe()[salle.getCoordonneeX() + 1][salle.getCoordonneeY()].setDepH(true);
						generateLabyrinthe(donjon,
								donjon.getLabyrinthe()[salle.getCoordonneeX() + 1][salle.getCoordonneeY()]);
					}

					break;
				case "D":
					if (!donjon.getLabyrinthe()[salle.getCoordonneeX()][salle.getCoordonneeY() + 1].isDecouverte()) {
						salle.setDepD(true);
						donjon.getLabyrinthe()[salle.getCoordonneeX()][salle.getCoordonneeY() + 1].setDepG(true);
						generateLabyrinthe(donjon,
								donjon.getLabyrinthe()[salle.getCoordonneeX()][salle.getCoordonneeY() + 1]);
					}

					break;
				case "G":
					if (!donjon.getLabyrinthe()[salle.getCoordonneeX()][salle.getCoordonneeY() - 1].isDecouverte()) {
						salle.setDepG(true);
						donjon.getLabyrinthe()[salle.getCoordonneeX()][salle.getCoordonneeY() - 1].setDepD(true);
						generateLabyrinthe(donjon,
								donjon.getLabyrinthe()[salle.getCoordonneeX()][salle.getCoordonneeY() - 1]);
					}
					break;

				default:
					break;
				}
			}
		}
	}

	/**
	 * retourne les murs possibles à casser (un mur peut être cassé si la salle
	 * derrière le mur n'a pas été visitée et quelle existe
	 * 
	 * @param donjon : le donjon
	 * @param salle  : la salle en cours
	 * @return une liste avec les murs à casser
	 */
	public List<String> murACasse(Donjon donjon, Salle salle) {

		List<String> murACasse = new ArrayList<>();
		if (donjon != null && salle != null) {

			if (!salle.isDepB() && salle.getCoordonneeX() < donjon.getHauteur() - 1
					&& !donjon.getLabyrinthe()[salle.getCoordonneeX() + 1][salle.getCoordonneeY()].isDecouverte()) {
				murACasse.add("B");
			}
			if (!salle.isDepH() && salle.getCoordonneeX() > 0
					&& !donjon.getLabyrinthe()[salle.getCoordonneeX() - 1][salle.getCoordonneeY()].isDecouverte()) {
				murACasse.add("H");
			}
			if (!salle.isDepG() && salle.getCoordonneeY() > 0
					&& !donjon.getLabyrinthe()[salle.getCoordonneeX()][salle.getCoordonneeY() - 1].isDecouverte()) {
				murACasse.add("G");
			}
			if (!salle.isDepD() && salle.getCoordonneeY() < donjon.getLargeur() - 1
					&& !donjon.getLabyrinthe()[salle.getCoordonneeX()][salle.getCoordonneeY() + 1].isDecouverte()) {
				murACasse.add("D");
			}
		}
		return murACasse;
	}

	/**
	 * Creation d'un donjon pour le développement (celui du sujet)
	 * 
	 * @return le donjon créé
	 */
	public Donjon creationDonjon() {
		JoueurService joueurService = new JoueurService();
		Donjon donjon = new Donjon(6, 3, 5, 3);
		Salle salle = new PieceDepart(0, 0, true, false, false, false, true);

		Joueur joueur = joueurService.creationJoueur("toto");
			((Piece) salle).setJoueur(joueur);
			
		donjon.getLabyrinthe()[0][0] = salle;

		salle = new Piece(0, 1, false, false, false, false, true);
		donjon.getLabyrinthe()[0][1] = salle;
		salle = new Piece(0, 5, false, false, false, false, true);
		donjon.getLabyrinthe()[0][5] = salle;
		salle = new Piece(1, 3, false, false, false, false, true);
		donjon.getLabyrinthe()[1][3] = salle;
		salle = new Piece(2, 01, false, false, false, false, true);
		donjon.getLabyrinthe()[2][0] = salle;
		salle = new Piece(2, 1, false, false, false, false, true);
		donjon.getLabyrinthe()[2][1] = salle;

		salle = new Piece(0, 2, true, false, false, true, true);
		donjon.getLabyrinthe()[0][2] = salle;

		salle = new Piece(0, 3, false, false, true, true, true);
		donjon.getLabyrinthe()[0][3] = salle;

		salle = new Piece(0, 4, true, false, true, false, true);
		donjon.getLabyrinthe()[0][4] = salle;

		salle = new Piece(1, 0, false, true, false, true, true);
		ajoutMonstreEtObjet(donjon, salle);
		donjon.getLabyrinthe()[1][0] = salle;

		salle = new Piece(1, 1, false, false, true, true, true);
		donjon.getLabyrinthe()[1][1] = salle;

		salle = new Piece(1, 2, true, true, true, false, true);
		ajoutMonstreEtObjet(donjon, salle);
		donjon.getLabyrinthe()[1][2] = salle;

		salle = new Piece(1, 4, false, true, false, true, true);
		donjon.getLabyrinthe()[1][4] = salle;

		salle = new Piece(1, 5, false, false, true, false, true);
		ajoutMonstreEtObjet(donjon, salle);
		donjon.getLabyrinthe()[1][5] = salle;

		salle = new Piece(2, 2, false, true, false, true, true);
		donjon.getLabyrinthe()[2][2] = salle;

		salle = new Piece(2, 3, false, false, true, true, true);
		ajoutMonstreEtObjet(donjon, salle);
		donjon.getLabyrinthe()[2][3] = salle;

		salle = new Piece(2, 4, false, false, true, true, true);
		ajoutMonstreEtObjet(donjon, salle);
		donjon.getLabyrinthe()[2][4] = salle;

		salle = new PieceArriver(2, 5, false, false, true, false, true);
		donjon.getLabyrinthe()[2][5] = salle;

		return donjon;
	}

	/**
	 * Ajout aléatoire de monstres et objets dans une salle
	 * 
	 * @param donjon : le donjon
	 * @param salle  : la salle où il faut ajouter des monstres
	 * @return true si l'ajout a été effectué
	 */
	public boolean ajoutMonstreEtObjet(Donjon donjon, Salle salle) {
		if (donjon != null && salle instanceof Piece) {
			MonstreService monstreService = new MonstreService();
			ObjetsService objetsService = new ObjetsService();
			for (int i = 0; i < donjon.getNbMonstreMaxSalle(); i++) {
				if (new Random().nextBoolean()) {
					((Piece) salle).getListeMonstres().add(monstreService.creationMonstre("Fayak"));
				}
			}
			for (int i = 0; i < donjon.getNbObjetSalle(); i++) {
				if (new Random().nextBoolean()) {
					((Piece) salle).getListeObjets().add(objetsService.creationObjet());
				}
			}
			return true;
		}
		return false;
	}

}
