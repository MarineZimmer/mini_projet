package fr.afpa.donjonnicosammarine.services;

import java.util.Random;

public class AleatoireService {
	/**
	 * Fonction permettant de generer un nombre entier aléatoirement entre une valeur minimum et maximum
	 * @param min : le chiffre minimum
	 * @param max : le chiffre maaximum
	 * @return = un chiffre entier compris entre une valeur minimale et maximale
	 */

	public int aleatoireInt(int min, int max) {

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;

	}

}
