package fr.afpa.donjonnicosammarine.services;

import fr.afpa.donjonnicosammarine.entites.Personnage;
import fr.afpa.donjonnicosammarine.iservices.ICombattre;


public class PersonnageService implements ICombattre{
/**
 * Fonction permettant de retirer des points de vie au personnage touché par une attaque
 */
	@Override
	public  boolean attaquer(Personnage personnageAttaque, Personnage personnageTouche) {
		if(personnageAttaque!=null && personnageTouche!=null) {
			personnageTouche.setPointDeVie(personnageTouche.getPointDeVie()-personnageAttaque.getAttaque());
			return true;
		}
		return false;
	}

}
