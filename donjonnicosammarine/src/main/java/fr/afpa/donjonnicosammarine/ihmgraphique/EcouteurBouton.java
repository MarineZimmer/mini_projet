package fr.afpa.donjonnicosammarine.ihmgraphique;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.afpa.donjonnicosammarine.ihms.IhmJoueur;
import fr.afpa.donjonnicosammarine.services.JeuService;

public class EcouteurBouton implements ActionListener{
	private JeuService jeuService;
	private IhmJoueur ihmJoueur;
	
	
	
	public EcouteurBouton() {
		super();
		jeuService = new JeuService();
		ihmJoueur = new IhmJoueur();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "quitter" : System.exit(0);
			
			break;
		case "top10" : ihmJoueur.affichageTop10();
		
		break;
		case "nouvelle partie" : ihmJoueur.demarrerJeu("toto");
		
		break;

		default:
			break;
		}
		
	}

}
