package fr.afpa.donjonnicosammarine.ihmgraphique;

import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.afpa.donjonnicosammarine.controles.ControleSaisie;

public class BoiteDialog extends JDialog implements ActionListener {
	private List<String> resultat;
	JPanel jPanelSaisie;
	JLabel jLabelNom;
	JTextField jTextFieldNom;
	JLabel jLabelHauteur;
	JTextField jTextFieldHauteur;
	JLabel jLabelLargeur;
	JTextField jTextFieldLargeur;
	JLabel jLabelNbMonstre;
	JTextField jTextFieldNbMonstre;
	JLabel jLabelNbObjet;
	JTextField jTextFieldNbObjet;
	JButton boutonOk;
	JButton boutonAnnuler;

	public BoiteDialog(JFrame parent, String titre, boolean modal) {
		super(parent, titre, modal);
		this.setSize(550, 200);
		this.setLocationRelativeTo(null);
		this.setResizable(false);

		resultat = new ArrayList<String>();
		initialisation();

	}

	public void initialisation() {

		jPanelSaisie = new JPanel();
		jPanelSaisie.setLayout(new GridLayout(6, 2));

		jLabelNom = new JLabel("Nom : ");
		jTextFieldNom = new JTextField();

		jPanelSaisie.add(jLabelNom);
		jPanelSaisie.add(jTextFieldNom);

		jLabelHauteur = new JLabel("Hauteur labyrinthe (entre 2 et 8) : ");
		jTextFieldHauteur = new JTextField();

		jPanelSaisie.add(jLabelHauteur);
		jPanelSaisie.add(jTextFieldHauteur);

		jLabelLargeur = new JLabel("Largeur labyrinthe (entre 2 et 10) : ");
		jTextFieldLargeur = new JTextField();

		jPanelSaisie.add(jLabelLargeur);
		jPanelSaisie.add(jTextFieldLargeur);
		
		jLabelNbMonstre = new JLabel("Nombre de monstres par salle (entre 0 et 10) : ");
		jTextFieldNbMonstre = new JTextField();

		jPanelSaisie.add(jLabelNbMonstre);
		jPanelSaisie.add(jTextFieldNbMonstre);
		
		jLabelNbObjet = new JLabel("Nombre d'objets par salle (entre 0 et 10) : ");
		jTextFieldNbObjet = new JTextField();

		jPanelSaisie.add(jLabelNbObjet);
		jPanelSaisie.add(jTextFieldNbObjet);

		boutonOk = new JButton("Valider");
		boutonOk.setActionCommand("ok");
		boutonOk.addActionListener(this);
		boutonAnnuler = new JButton("Annuler");
		boutonAnnuler.setActionCommand("annuler");
		boutonAnnuler.addActionListener(this);

		jPanelSaisie.add(boutonAnnuler);
		jPanelSaisie.add(boutonOk);

		this.add(jPanelSaisie);
	}

	public List<String> recuperationSaisie() {

		this.setVisible(true);
		return resultat;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "ok":if (ControleSaisie.saisieNom(jTextFieldNom.getText())
				&& ControleSaisie.saisieInt(jTextFieldHauteur.getText(), 2, 8)
				&& ControleSaisie.saisieInt(jTextFieldLargeur.getText(), 2, 10)
				&&  ControleSaisie.saisieInt(jTextFieldNbMonstre.getText(), 0, 10)
				&& ControleSaisie.saisieInt(jTextFieldNbObjet.getText(), 0, 10)) {
		resultat.add(jTextFieldNom.getText());
		resultat.add(jTextFieldHauteur.getText());
		resultat.add(jTextFieldLargeur.getText());
		resultat.add(jTextFieldNbMonstre.getText());
		resultat.add(jTextFieldNbObjet.getText());
		}
			break;
		case "annuler" : resultat.add("annuler");
		break;

		default:
			break;
		}
		setVisible(false);

}

}
