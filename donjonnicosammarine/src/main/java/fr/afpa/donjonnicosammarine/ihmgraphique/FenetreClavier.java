package fr.afpa.donjonnicosammarine.ihmgraphique;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;

import fr.afpa.donjonnicosammarine.controles.ControleSaisie;
import fr.afpa.donjonnicosammarine.entites.Donjon;
import fr.afpa.donjonnicosammarine.entites.Joueur;
import fr.afpa.donjonnicosammarine.entites.Monstre;
import fr.afpa.donjonnicosammarine.entites.Objet;
import fr.afpa.donjonnicosammarine.entites.Piece;
import fr.afpa.donjonnicosammarine.entites.PieceArriver;
import fr.afpa.donjonnicosammarine.entites.Score;
import fr.afpa.donjonnicosammarine.ihms.IhmJoueur;
import fr.afpa.donjonnicosammarine.services.DonjonService;
import fr.afpa.donjonnicosammarine.services.JeuService;
import fr.afpa.donjonnicosammarine.services.JoueurService;
import fr.afpa.donjonnicosammarine.services.MonstreService;
import fr.afpa.donjonnicosammarine.services.ObjetsService;
import fr.afpa.donjonnicosammarine.services.PersonnageService;
import fr.afpa.donjonnicosammarine.services.SalleService;

public class FenetreClavier extends JFrame implements ActionListener, KeyListener {

	private JButton top;
	private JButton nouvellePartie;
	private JButton quitter;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuQuitter;
	private JTextArea text;
	private JTextArea textDonjon;
	private JButton boutonCombat;
	private JButton boutonObjet;
	private JRadioButton radioBoutonDetails;
	private JLabel descriptionJoueur;

	private JPanel jPanelDemarrage;
	private JPanel jPanelTop10;
	private JPanel jPanelDonjon;
	private JPanel jPanelDonjonEst;
	private JPanel jPanelDonjonNord;

	private GridLayout borderDonjon;

	private JoueurService joueurService;
	private JeuService jeuService;
	private MonstreService monstreService;
	private ObjetsService objetsService;
	private PersonnageService personnageService;
	private SalleService salleService;
	private Donjon donjon;
	int x = 0, y = 0;
	private boolean detail = false;
	private Joueur joueur;

	public FenetreClavier(String title, int largeur, int hauteur, int x, int y) throws HeadlessException {

		super(title);
		joueurService = new JoueurService();
		jeuService = new JeuService();
		monstreService = new MonstreService();
		objetsService = new ObjetsService();
		personnageService = new PersonnageService();
		salleService = new SalleService();
		jPanelDonjon = new JPanel();
		jPanelDemarrage = new JPanel();
		jPanelTop10 = new JPanel();
		jPanelDonjonEst = new JPanel();
		jPanelDonjonNord = new JPanel();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		radioBoutonDetails = new JRadioButton("affichage détaillé");
		radioBoutonDetails.setActionCommand("detail");
		radioBoutonDetails.addActionListener(this);
		addKeyListener(this);

		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		super.setBounds(x, y, largeur, hauteur);

		this.setResizable(false);
		barreMenu();
		pageDemarrage();

	}

	private void pageDemarrage() {
		top = new JButton("top 10");
		top.setActionCommand("top10");
		top.addActionListener(this);

		nouvellePartie = new JButton("Nouvelle Partie");
		nouvellePartie.setActionCommand("nouvelle partie");
		nouvellePartie.addActionListener(this);

		quitter = new JButton("Quitter");
		quitter.setActionCommand("quitter");
		quitter.addActionListener(this);

		jPanelDemarrage.removeAll();
		jPanelDonjon.setVisible(false);
		jPanelDonjonEst.setVisible(false);
		jPanelDonjonNord.setVisible(false);
		jPanelDemarrage.add(top);
		jPanelDemarrage.add(nouvellePartie);
		jPanelDemarrage.add(quitter);
		jPanelDemarrage.setVisible(true);
		getContentPane().add(jPanelDemarrage, BorderLayout.NORTH);
		this.repaint();
		this.setVisible(true);
	}

	private void barreMenu() {
		menuBar = new JMenuBar();
		menu = new JMenu("Menu");
		menuQuitter = new JMenuItem("Quitter");
		menuQuitter.setActionCommand("quitter");
		menuQuitter.addActionListener(this);
		menu.add(menuQuitter);
		menuBar.add(menu);
		setJMenuBar(menuBar);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "quitter":
			System.exit(0);

			break;
		case "top10":
			top10();

			break;
		case "nouvelle partie":
			demarrerPartie();
			break;
		case "combat":
			System.out.println("COMBAT");
			combat();
			monstreService.deplacementEnsembleMonstre(donjon, donjon.getLabyrinthe()[x][y]);
			affichageDonjon();
			break;
		case "objet":
			ramasserObjet();
			monstreService.deplacementEnsembleMonstre(donjon, donjon.getLabyrinthe()[x][y]);
			affichageDonjon();
			break;
		case "regarder":
			regarder();
			affichageDonjon();
			break;
		case "detail":
			if (e.getSource() instanceof JRadioButton && ((JRadioButton) e.getSource()).isSelected())
				detail = true;
			else
				detail = false;
			affichageDonjon();
			break;
		default:
			break;
		}

	}

	private void regarder() {
		List<String> listePiece = jeuService.deplacementPossibleJoueur((Piece) donjon.getLabyrinthe()[x][y]);
		listePiece.add(0, "Salle Courante");
		String piece = (String) JOptionPane.showInputDialog(null, "Selectionner la salle à regarder", "Regarder",
				JOptionPane.QUESTION_MESSAGE, null, listePiece.toArray(), listePiece.toArray()[0]);

		if (piece != null) {
			JOptionPane.showMessageDialog(null,
					salleService.regarderSalle(donjon, (Piece) donjon.getLabyrinthe()[x][y], piece));
		}

	}

	private void ramasserObjet() {
		Object[] choixObjets = ((Piece) donjon.getLabyrinthe()[x][y]).getListeObjets().toArray();
		Objet objet = (Objet) JOptionPane.showInputDialog(null, "Selectionner le monstre à combattre", "Combat",
				JOptionPane.QUESTION_MESSAGE, null, choixObjets, choixObjets[0]);
		if (objet != null) {
			objetsService.utilisationObjet(((Piece) donjon.getLabyrinthe()[x][y]).getListeObjets(),
					((Piece) donjon.getLabyrinthe()[x][y]).getListeObjets().indexOf(objet), joueur);
		}

	}

	private void combat() {
		Object[] choixMonstre = ((Piece) donjon.getLabyrinthe()[x][y]).getListeMonstres().toArray();
		Monstre monstre = (Monstre) JOptionPane.showInputDialog(null, "Selectionner l'objet à utiliser",
				"Ramasser objet", JOptionPane.QUESTION_MESSAGE, null, choixMonstre, choixMonstre[0]);
		if (monstre != null) {
			personnageService.attaquer(joueur, monstre);
			if (!monstreService.monstreMort(monstre, ((Piece) donjon.getLabyrinthe()[x][y]))) {
				personnageService.attaquer(monstre, joueur);
			}
		}

	}

	private void demarrerPartie() {
		x = 0;
		y = 0;
		jPanelDemarrage.setVisible(false);
		jPanelTop10.setVisible(false);
		jPanelDonjonEst.setVisible(true);
		jPanelDonjon.setVisible(true);
		jPanelDonjonNord.setVisible(true);
		jPanelDonjon.removeAll();
		jPanelDonjonEst.removeAll();
		jPanelDonjonNord.removeAll();
		this.repaint();
		//addKeyListener(this);
		BoiteDialog boiteDialog = new BoiteDialog(null, "Informations nouvelle partie", true);
		List<String> reponseSaisies;
		do {
			reponseSaisies = boiteDialog.recuperationSaisie();
		} while (reponseSaisies.isEmpty());
		if (reponseSaisies.size() == 5) {
			System.out.println(reponseSaisies);
			joueur = new JoueurService().creationJoueur(reponseSaisies.get(0));
			donjon = new DonjonService().creationDonjonAleatoire(Integer.parseInt(reponseSaisies.get(1)),
					Integer.parseInt(reponseSaisies.get(2)), Integer.parseInt(reponseSaisies.get(3)),
					Integer.parseInt(reponseSaisies.get(4)));
			donjon.setDeplacementMonstre(true);
			((Piece) donjon.getLabyrinthe()[0][0]).setJoueur(joueur);
			affichageDonjon();
		} else {
			pageDemarrage();
		}

	}

	private void top10() {

		jPanelTop10.removeAll();
		text = new JTextArea();
		text.setEditable(false);
		text.setFont(new java.awt.Font(Font.SERIF, Font.BOLD, 15));
		List<Score> liste = new JeuService().topScores(10, System.getenv("DONJON"));
		StringBuilder builder = new StringBuilder();
		int topScore = 0;

		builder.append("*************** Top Scores *************\n");

		builder.append(String.format("%-4s", "") + "| " + String.format("%-15s", "SCORE") + "| "
				+ String.format("%-20s", "NOM") + "| " + "DATE" + "\n");
		builder.append("-------------------------------------------------------------\n");
		for (Score score : liste) {
			builder.append(String.format("%-4s", ++topScore) + "| " + String.format("%-15s", score.getScore()) + "| "
					+ String.format("%-20s", score.getNom()) + "| " + score.getDate() + "\n");
		}
		builder.append("****************************************\n");
		System.out.println(builder.toString());
		text.append(builder.toString());

		jPanelTop10.add(text);
		jPanelTop10.setVisible(true);
		getContentPane().add(jPanelTop10, BorderLayout.CENTER);
		this.repaint();
		this.setVisible(true);

	}

	/*
	 * @Override public void mouseClicked(MouseEvent e) { int n;
	 * System.out.println(e.getComponent()); if (e.getSource() instanceof JTextArea)
	 * { System.out.println(((JTextArea) e.getSource()).getName()); n =
	 * Integer.parseInt(x + "" + y) - Integer.parseInt(((JTextArea)
	 * e.getSource()).getName()); System.out.println(n); if (n == -1) { if
	 * (joueurService.deplacer(donjon, donjon.getLabyrinthe()[x][y], "Droite") !=
	 * null) { y++; monstreService.deplacementEnsembleMonstre(donjon,
	 * donjon.getLabyrinthe()[x][y]); } } if (n == 1) { if
	 * (joueurService.deplacer(donjon, donjon.getLabyrinthe()[x][y], "Gauche") !=
	 * null) { y--; monstreService.deplacementEnsembleMonstre(donjon,
	 * donjon.getLabyrinthe()[x][y]); } } if (n == -10) { if
	 * (joueurService.deplacer(donjon, donjon.getLabyrinthe()[x][y], "Bas") != null)
	 * { x++; monstreService.deplacementEnsembleMonstre(donjon,
	 * donjon.getLabyrinthe()[x][y]); } } if (n == 10) { if
	 * (joueurService.deplacer(donjon, donjon.getLabyrinthe()[x][y], "Haut") !=
	 * null) { x--; monstreService.deplacementEnsembleMonstre(donjon,
	 * donjon.getLabyrinthe()[x][y]); } }
	 * 
	 * donjon.affichage(detail); affichageDonjon(); if (donjon.getLabyrinthe()[x][y]
	 * instanceof PieceArriver) { System.out.println("GAgné"); } }
	 *
	 * }
	 */

	public void affichageDonjon() {

		jPanelDonjon.removeAll();
		jPanelDonjonNord.removeAll();
		descriptionJoueur = new JLabel(joueur.toString());
		jPanelDonjonNord.add(descriptionJoueur);
		if (!joueurService.jeuContinu(donjon.getLabyrinthe()[x][y])) {
			System.out.println("fin");
			if (joueur.getPointDeVie() > 0) {
				JOptionPane.showMessageDialog(null, "GAGNE!!! score : " + joueur.getNombrePieceDOr(), "partie terminée",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, "PERDU!!!", "partie terminée", JOptionPane.ERROR_MESSAGE);
			}
			this.setFocusable(false);
			pageDemarrage();

		} else {
			borderDonjon = new GridLayout(donjon.getHauteur(), donjon.getLargeur());
			jPanelDonjon.setLayout(borderDonjon);
			jPanelDonjon.setBackground(Color.WHITE);
			borderDonjon.setHgap(0);
			borderDonjon.setVgap(0);
			for (int i = 0; i < donjon.getLabyrinthe().length; i++) {
				for (int j = 0; j < donjon.getLabyrinthe()[0].length; j++) {
					textDonjon = new JTextArea(3, 3);
					textDonjon.setName(i + "" + j);

					textDonjon.setEditable(false);
					textDonjon.setBorder(javax.swing.BorderFactory.createEmptyBorder());
					textDonjon.setFont(new java.awt.Font(Font.SERIF, Font.BOLD, 20));

					textDonjon.append(donjon.getLabyrinthe()[i][j].affichage(detail).get(0) + "\n"
							+ donjon.getLabyrinthe()[i][j].affichage(detail).get(1) + "\n"
							+ donjon.getLabyrinthe()[i][j].affichage(detail).get(2) + "\n"
							+ donjon.getLabyrinthe()[i][j].affichage(detail).get(3));
					jPanelDonjon.add("c" + i + j, textDonjon);
				}
			}
			getContentPane().add(jPanelDonjon, BorderLayout.CENTER);
			getContentPane().add(jPanelDonjonNord, BorderLayout.NORTH);
			affichageChoixAction();
			this.repaint();
			this.pack();
			this.setFocusable(true);
			this.setVisible(true);
		}
	}

	public void affichageChoixAction() {
		List<String> listeAction = jeuService.actionJoueur((Piece) donjon.getLabyrinthe()[x][y]);
		jPanelDonjonEst.removeAll();
		jPanelDonjonEst.setLayout(new GridLayout(0, 1));

		jPanelDonjonEst.add(radioBoutonDetails);

		boutonObjet = new JButton("Regarder");
		boutonObjet.setActionCommand("regarder");
		boutonObjet.addActionListener(this);
		jPanelDonjonEst.add(boutonObjet);

		if (listeAction.contains("Attaquer")) {
			boutonCombat = new JButton("Combat");
			boutonCombat.setActionCommand("combat");
			boutonCombat.setMaximumSize(new Dimension(20, 10));
			boutonCombat.setSize(10, 10);
			boutonCombat.addActionListener(this);
			jPanelDonjonEst.add(boutonCombat);
		}
		if (listeAction.contains("Objets")) {
			boutonObjet = new JButton("Objets");
			boutonObjet.setActionCommand("objet");
			boutonObjet.addActionListener(this);
			jPanelDonjonEst.add(boutonObjet);
		}

		getContentPane().add(jPanelDonjonEst, BorderLayout.EAST);

	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println("pressed " + e.getKeyCode());

	}

	@Override
	public void keyReleased(KeyEvent e) {
		System.out.println("pressed " + e.getKeyCode());
		System.out.println(e.getComponent());
		switch (e.getKeyCode()) {
		case 37:
			if (joueurService.deplacer(donjon, donjon.getLabyrinthe()[x][y], "Gauche") != null) {
				y--;
				monstreService.deplacementEnsembleMonstre(donjon, donjon.getLabyrinthe()[x][y]);
			}
			break;
		case 38:
			if (joueurService.deplacer(donjon, donjon.getLabyrinthe()[x][y], "Haut") != null) {
				x--;
				monstreService.deplacementEnsembleMonstre(donjon, donjon.getLabyrinthe()[x][y]);
			}
			break;
		case 39:
			if (joueurService.deplacer(donjon, donjon.getLabyrinthe()[x][y], "Droite") != null) {
				y++;
				monstreService.deplacementEnsembleMonstre(donjon, donjon.getLabyrinthe()[x][y]);
			}
			break;
		case 40:
			if (joueurService.deplacer(donjon, donjon.getLabyrinthe()[x][y], "Bas") != null) {
				x++;
				monstreService.deplacementEnsembleMonstre(donjon, donjon.getLabyrinthe()[x][y]);
			}
			break;
		}

		
		affichageDonjon();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}
}
