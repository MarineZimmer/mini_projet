package fr.afpa.donjonnicosammarine.entites;

import java.util.List;

import fr.afpa.donjonnicosammarine.services.JoueurService;
import lombok.Getter;
import lombok.Setter;

@Getter
public final class Donjon {

	private int largeur;
	private int hauteur;
	private int nbMonstreMaxSalle;
	private int nbObjetSalle;
	private Salle[][] labyrinthe;
	
	@Setter
	private int nbTour;
	
	@Setter
	private boolean deplacementMonstre;

	public Donjon() {
		super();
	}

	public Donjon(int largeur, int hauteur, int nbMonstreMaxSalle, int nbObjetSalle) {
		super();
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.nbMonstreMaxSalle = nbMonstreMaxSalle;
		this.nbObjetSalle = nbObjetSalle;
		labyrinthe = new Salle[hauteur][largeur];
		initialisationSalleDonjon();
	}

	/**
	 * Création des salles du donjon
	 */
	public void initialisationSalleDonjon() {
		for (int i = 0; i < labyrinthe.length; i++) {
			for (int j = 0; j < labyrinthe[0].length; j++) {
				labyrinthe[i][j] = new Piece(i, j, false, false, false, false, false);
			}
		}
		labyrinthe[0][0] = new PieceDepart(0, 0, false, false, false, false, false);
	}

	/**
	 * affichage du donjon
	 * 
	 * @param detail : true pour afficher le labyrinthe en details, false sinon
	 */
	public String affichage(boolean detail) {
		StringBuilder reponse = new StringBuilder();
		if (detail) {
			Salle.setLargeurAffichage(5);
		} else {
			Salle.setLargeurAffichage(4);
		}

		for (int i = 0; i < labyrinthe.length; i++) {
			StringBuilder builder1 = new StringBuilder();
			StringBuilder builder2 = new StringBuilder();
			StringBuilder builder3 = new StringBuilder();
			StringBuilder builder4 = new StringBuilder();
			for (int j = 0; j < labyrinthe[0].length; j++) {
				List<String> affichage = labyrinthe[i][j].affichage(detail);
				builder1.append(affichage.get(0));
				builder2.append(affichage.get(1));
				builder3.append(affichage.get(2));
				builder4.append(affichage.get(3));
			}
			builder1.append("*");
			builder2.append("|");
			builder3.append("|");
			builder4.append("|");
			System.out.println(builder1);
			reponse.append(builder1+"\n");
			System.out.println(builder2);
			reponse.append(builder2+"\n");
			if (detail) {
				if (builder3.toString().matches(".*[A-Z].*")) {
					System.out.println(builder3);
					reponse.append(builder3+"\n");
				}
				if (builder4.toString().matches(".*[A-Z].*")) {
					System.out.println(builder4);
					reponse.append(builder4+"\n");
				}
			}
		}
		for (int i = 0; i < labyrinthe[0].length; i++) {
			System.out.print(("******************".substring(0, Piece.getLargeurAffichage())));
			reponse.append(("******************".substring(0, Piece.getLargeurAffichage())));
		}
		System.out.println();
		reponse.append("\n");
		return reponse.toString();
	}

}
