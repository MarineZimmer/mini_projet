package fr.afpa.donjonnicosammarine.entites;

import fr.afpa.donjonnicosammarine.ientites.IUtiliser;

public final class BourseOr extends Objet implements IUtiliser {

	/**
	 * @param valeur
	 */
	public BourseOr(int valeur) {
		super(valeur);
		
	}

	public BourseOr() {
		super();
	}
	@Override
	public String toString() {
		return "BourseOr [valeur =" + super.getValeur() + "]";
	}

	@Override
	public boolean utiliserObjet(Personnage personnage) {
		if(personnage!=null) {
			System.out.println("Utilisation de l'objet bourse d'or d'une valeur de " + this.getValeur());
			personnage.setNombrePieceDOr(personnage.getNombrePieceDOr()+this.getValeur());
			return true;
		}
		return false;
		
	}
}
