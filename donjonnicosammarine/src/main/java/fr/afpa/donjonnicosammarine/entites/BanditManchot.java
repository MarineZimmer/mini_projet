package fr.afpa.donjonnicosammarine.entites;

import fr.afpa.donjonnicosammarine.ientites.IUtiliser;
import fr.afpa.donjonnicosammarine.services.ObjetsService;

public class BanditManchot extends Objet implements IUtiliser {

	/**
	 * @param valeur
	 */
	public BanditManchot(int valeur) {
		super(valeur);
	}

	public BanditManchot() {
		super();
	}

	@Override
	public String toString() {
		return "BanditManchot [valeur =" + super.getValeur() + "]";
	}

	
	@Override
	public boolean utiliserObjet(Personnage personnage) {
		ObjetsService objetsService = new ObjetsService();
		Objet objetTransforme;
		if (personnage != null) {
			if (personnage.getNombrePieceDOr() >= this.getValeur()) {
				personnage.setNombrePieceDOr(personnage.getNombrePieceDOr() - this.getValeur());
				do {
					objetTransforme = objetsService.creationObjet();
				} while (objetTransforme instanceof BanditManchot);
				objetTransforme.utiliserObjet(personnage);
			} else {
				System.out.println("Vous n'avez pas assez d'or pour transformer le Bandit Manchot");
			}
			return true;
		}

		return false;
	}
}
