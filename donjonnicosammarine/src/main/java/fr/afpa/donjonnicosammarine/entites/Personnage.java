package fr.afpa.donjonnicosammarine.entites;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Personnage {
	private String nom;
	private int pointDeVie;
	private int attaque;
	private int nombrePieceDOr;
	

	public  Personnage() {
		super();
	}
	
	public Personnage(String nom, int pointDeVie,int attaque, int nombrePieceDOr) {
		this.nom=nom;
		this.pointDeVie=pointDeVie;
		this.attaque=attaque;
		this.nombrePieceDOr=nombrePieceDOr;
		
	}

	
	
	
}
