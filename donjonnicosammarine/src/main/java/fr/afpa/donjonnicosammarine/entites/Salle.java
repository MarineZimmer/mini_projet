package fr.afpa.donjonnicosammarine.entites;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
public abstract class Salle {
	private int coordonneeX;
	private int coordonneeY;
	@Setter
	private boolean depB;
	@Setter
	private boolean depH;
	@Setter
	private boolean depG;
	@Setter
	private boolean depD;

	@Setter
	private int nbPassage;
	@Setter
	private int nbPas;

	@Setter
	private boolean decouverte;
	@Setter
	@Getter
	private static int largeurAffichage=4;

	public Salle() {
		super();
	}


	public Salle(int coordonneeX, int coordonneeY, boolean depB, boolean depH, boolean depG, boolean depD,
			boolean decouverte) {
		super();
		this.coordonneeX = coordonneeX;
		this.coordonneeY = coordonneeY;
		this.depB = depB;
		this.depH = depH;
		this.depG = depG;
		this.depD = depD;
		this.decouverte = decouverte;
	}

	/**
	 * Affichage graphique d'une salle
	 * 
	 * @param detail : true si l'affichage est détaillé, false sinon
	 * @return : une liste représentant chaque ligne de l'affichage de la salle
	 */
	public abstract List<String> affichage(boolean detail);

}
