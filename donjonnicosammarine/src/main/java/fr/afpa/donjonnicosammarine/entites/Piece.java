package fr.afpa.donjonnicosammarine.entites;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.donjonnicosammarine.ientites.IRegarder;
import lombok.Getter;
import lombok.Setter;

@Getter
public class Piece extends Salle implements IRegarder {

	private List<Monstre> listeMonstres;
	private List<Objet> listeObjets;
	@Setter
	private Joueur joueur;

	public Piece() {
		super();
		joueur = null;
	}


	public Piece(int coordonneeX, int coordonneeY, boolean depB, boolean depH, boolean depG, boolean depD,
			boolean decouverte) {
		super(coordonneeX, coordonneeY, depB, depH, depG, depD, decouverte);
		listeMonstres = new ArrayList<>();
		listeObjets = new ArrayList<>();
		joueur = null;

	}

	@Override
	public List<String> affichage(boolean detail) {

		setLargeurAffichage(8);
		List<String> lignes = new ArrayList<>();

		StringBuilder builder = new StringBuilder();
		if (!isDepH()) {
			for (int i = 0; i < getLargeurAffichage(); i++) {
				builder.append("*");
			}
			lignes.add(builder.toString());
			builder = new StringBuilder();
		}else {
			lignes.add(String.format("%-" + getLargeurAffichage() + "s","*" ));
		}

		if (!isDepG()) {
			builder.append("|");
		} else {
			builder.append(" ");
		}
		if (lignes.isEmpty()) {
			lignes.add(String.format("%-" + getLargeurAffichage() + "s", builder.toString()));
		}

		if (joueur != null) {
			lignes.add(String.format("%-" + getLargeurAffichage() + "s", builder.toString() + " P"));
		}
		if (detail && !listeMonstres.isEmpty()) {
			lignes.add(
					String.format("%-" + getLargeurAffichage() + "s", builder.toString() + listeMonstres.size() + "M"));
		}
		if (detail && !listeObjets.isEmpty()) {
			lignes.add(
					String.format("%-" + getLargeurAffichage() + "s", builder.toString() + listeObjets.size() + "O"));
		}
		while (lignes.size() != 4) {
			lignes.add(String.format("%-" + getLargeurAffichage() + "s", builder.toString()));
		}

		return lignes;
	}

	@Override
	public String regarder() {
		StringBuilder builder = new StringBuilder();
		builder.append("Salle (" + getCoordonneeX() + "," + getCoordonneeY() + ")" + "\n");
		if (joueur != null) {
			builder.append(joueur + "\n");
		}
		if (listeMonstres.isEmpty()) {
			builder.append("Pas de monstre dans la salle\n");
		}
		for (Monstre monstre : listeMonstres) {
			builder.append(monstre + "\n");
		}
		if (listeObjets.isEmpty()) {
			builder.append("Pas d'objet dans la salle\n");
		}
		for (Objet objet : listeObjets) {
			builder.append(objet + "\n");
		}

		return builder.toString();
	}
}
