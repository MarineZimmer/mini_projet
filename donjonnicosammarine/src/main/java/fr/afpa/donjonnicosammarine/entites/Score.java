package fr.afpa.donjonnicosammarine.entites;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Score implements Comparable{
 private int score;
 private String date;
 private String nom;
 
 
@Override
public int compareTo(Object o) {
	if(o instanceof Score) {
		return ((Score) o).getScore()-this.score;
	}
	return 0;
}


@Override
public boolean equals(Object obj) {
	if(obj instanceof Score) {
		return ((Score) obj).getScore()==this.score;
	}
	return false;
}


 
 
}
