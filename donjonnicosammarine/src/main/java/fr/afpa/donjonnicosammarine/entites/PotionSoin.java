package fr.afpa.donjonnicosammarine.entites;

import fr.afpa.donjonnicosammarine.ientites.IUtiliser;

public final class PotionSoin extends Objet implements IUtiliser{

	/**
	 * @param valeur
	 */
	public PotionSoin(int valeur) {
		super(valeur);
		
	}

	public PotionSoin() {
		super();
	}
	@Override
	public String toString() {
		return "PotionSoin [valeur =" + super.getValeur() + "]";
	}

	@Override
	public boolean utiliserObjet(Personnage personnage) {
		if(personnage!=null) {
			System.out.println("Utilisation de l'objet potion soin d'une valeur de " + this.getValeur());
			personnage.setPointDeVie(personnage.getPointDeVie()+this.getValeur());
			return true;
		}
		return false;
	}
}
