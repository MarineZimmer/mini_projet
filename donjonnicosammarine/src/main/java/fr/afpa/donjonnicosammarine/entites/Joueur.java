package fr.afpa.donjonnicosammarine.entites;

import lombok.Getter;
import lombok.Setter;

public final class Joueur extends Personnage {
	@Getter
	private static int forceMax = 40;
	@Getter
	private static int pointVieMax = 100;
	@Getter
	private static int nombrePieceOrMaxDepart = 20;
	@Getter
	private static int forceMin = 10;
	@Getter
	private static int pointVieMin = 50;
	@Getter
	private static int nombrePieceOrMinDepart = 1;

	public Joueur() {
		super();
	}

	/**
	 * @param nom
	 * @param pointDeVie
	 * @param attaque
	 * @param nombrePieceDOr
	 */
	public Joueur(String nom, int pointDeVie, int attaque, int nombrePieceDOr) {
		super(nom, pointDeVie, attaque, nombrePieceDOr);
	}

	@Override
	public String toString() {
		return "Joueur [Nom :" + getNom() + ", point de vie=" + getPointDeVie() + ", attaque =" + getAttaque()
				+ ", piece d'or =" + getNombrePieceDOr() + "]";
	}

}
