package fr.afpa.donjonnicosammarine.entites;


import java.util.List;

public class PieceArriver extends Piece {

	


	public PieceArriver() {
		super();
	}
	

	public PieceArriver(int coordonneeX, int coordonneeY, boolean depB, boolean depH, boolean depG, boolean depD,
			boolean decouverte) {
		super(coordonneeX, coordonneeY, depB, depH, depG, depD, decouverte);
	}


	@Override
	public List<String> affichage(boolean detail) {
		List<String> lignes = super.affichage(detail);

		StringBuilder builder = new StringBuilder();
		if (!isDepG()) {
			builder.append("|");
		}
		lignes.add(1,String.format("%-"+getLargeurAffichage()+"s", builder.toString() + " ><"));
		return lignes;
	}



	@Override
	public String regarder() {
		
		return "Sortie \n" + super.regarder();
	}

	
}
