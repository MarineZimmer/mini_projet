package fr.afpa.donjonnicosammarine.entites;

import fr.afpa.donjonnicosammarine.ientites.IUtiliser;
import lombok.Getter;

@Getter
public abstract class Objet implements IUtiliser {
	private int valeur;
	/**
	 * 
	 */
	public Objet() {
		super();
	
	}
	

	/**
	 * @param valeur
	 */
	public Objet(int valeur) {
		super();
		this.valeur = valeur;
	}

}
