package fr.afpa.donjonnicosammarine.ihms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.donjonnicosammarine.entites.Donjon;
import fr.afpa.donjonnicosammarine.entites.Joueur;
import fr.afpa.donjonnicosammarine.entites.Monstre;
import fr.afpa.donjonnicosammarine.entites.Objet;
import fr.afpa.donjonnicosammarine.entites.Piece;
import fr.afpa.donjonnicosammarine.entites.Salle;
import fr.afpa.donjonnicosammarine.entites.Score;
import fr.afpa.donjonnicosammarine.services.JeuService;
import fr.afpa.donjonnicosammarine.services.JoueurService;
import fr.afpa.donjonnicosammarine.services.MonstreService;
import fr.afpa.donjonnicosammarine.services.ObjetsService;
import fr.afpa.donjonnicosammarine.services.PersonnageService;
import fr.afpa.donjonnicosammarine.services.SalleService;

public class IhmJoueur {

	/**
	 * Menu de démarrage, choix entre afficher top 10, jouer une partie, quitter
	 */
	public void menuDemarrage() {
		IhmSaisie ihmSaisie = new IhmSaisie();
		effacerConsole();
		int choix;
		while (true) {
			System.out.println(" 1 : Afficher le top 10 des scores\n 2 : Demarrer une nouvelle partie \n 3 : Quitter");
			choix = ihmSaisie.saisieInt(1, 3);
			effacerConsole();
			switch (choix) {
			case 1:
				affichageTop10();
				break;
			case 2:
				String nomJoueur = creationJoueur();
				demarrerJeu(nomJoueur);
				break;
			case 3:
				return;

			default:
				break;
			}
		}
	}

	/**
	 * Fonction permettant le lancement du jeu
	 */
	public void demarrerJeu(String nomJoueur) {
		JoueurService joueurService = new JoueurService();
		MonstreService monstreService = new MonstreService();
		IhmDonjon ihmDonjon = new IhmDonjon();
		JeuService jeuService = new JeuService();
		IhmSaisie ihmSaisie = new IhmSaisie();
		int choix;

		Donjon donjon = ihmDonjon.creationDonjon();

		Joueur joueur = joueurService.creationJoueur(nomJoueur);

		((Piece) donjon.getLabyrinthe()[0][0]).setJoueur(joueur);

		Salle salleCourante = donjon.getLabyrinthe()[0][0];
		effacerConsole();
		while (true) {
			donjon.setDeplacementMonstre(true);
			donjon.affichage(false);
			if (joueurService.jeuContinu(salleCourante)) {

				if (salleCourante instanceof Piece) {
					List<String> action = jeuService.actionJoueur((Piece) salleCourante);
					affichageListes(action);
					choix = ihmSaisie.saisieInt(0, action.size() - 1);
					salleCourante = traitementChoixAction(action.get(choix), donjon, salleCourante);
				}
				if (donjon.isDeplacementMonstre()) {
					monstreService.deplacementEnsembleMonstre(donjon, salleCourante);
				}
			} else {
				break;
			}
		}

	}
/**
 * Affichage des options disponibles pour le joueur au cours d'une partie
 * @param action : ce que peut faire le joueur
 * @param donjon : Le donjon
 * @param salleCourante : la salle ou se trouve le joueur
 * @return ce que peut faire le joueur dans la salle dans laquelle il se trouve
 */
	public Salle traitementChoixAction(String action, Donjon donjon, Salle salleCourante) {

		switch (action) {
		case "Regarder":
			regarder(donjon, salleCourante);
			break;
		case "Attaquer":
			attaquer(salleCourante);
			break;
		case "Deplacer":
			return deplacementJoueur(donjon, salleCourante);

		case "Objets":
			ramasserObjet(salleCourante);
			break;
		case "Affichage de la carte en detail":
			donjon.setDeplacementMonstre(false);
			effacerConsole();
			donjon.affichage(true);
			break;
		}
		return salleCourante;

	}

	/**
	 * Execution de l'action ramasser objet, demande le choix de l'objet parmi la
	 * liste d'objets de la salle et utilisation de l'objet par le joueur
	 * 
	 * @param salleCourante : la salle où se situe le joueur
	 */
	public void ramasserObjet(Salle salleCourante) {
		if (salleCourante != null) {
			int choix;
			IhmSaisie ihmSaisie = new IhmSaisie();
			ObjetsService objetsService = new ObjetsService();

			if (salleCourante instanceof Piece) {
				List<String> listeObjets = new ArrayList<>();

				for (Objet objet : ((Piece) salleCourante).getListeObjets()) {
					listeObjets.add(objet.toString());
				}

				affichageListes(listeObjets);
				choix = ihmSaisie.saisieInt(0, listeObjets.size() - 1);
				effacerConsole();
				objetsService.utilisationObjet(((Piece) salleCourante).getListeObjets(), choix,
						((Piece) salleCourante).getJoueur());
			}
		}
	}

	/**
	 * Fonction permettant le déplacement du joueur dans le labyrinthe avec un choix
	 * de propositions restreint selon les deplacements autorisés par le labyrinthe
	 * 
	 * @param donjon
	 * @param salleCourante
	 * @return
	 */
	public Salle deplacementJoueur(Donjon donjon, Salle salleCourante) {
		int choixDeplacement;
		JeuService jeuService = new JeuService();
		IhmSaisie ihmSaisie = new IhmSaisie();
		if (salleCourante instanceof Piece) {
			System.out.println("Ou souhaitez vous aller ?");
			List<String> deplacement = jeuService.deplacementPossibleJoueur((Piece) salleCourante);
			affichageListes(deplacement);
			choixDeplacement = ihmSaisie.saisieInt(0, deplacement.size() - 1);
			effacerConsole();
			JoueurService joueurService = new JoueurService();
			Salle salle = joueurService.deplacer(donjon, salleCourante, deplacement.get(choixDeplacement));
			if (salle != null) {
				return salle;
			}

		}

		return salleCourante;

	}

	/**
	 * Execution de l'action attaquer, demande quelle monstre le joueur veut
	 * attaquer et attaque le monstre, si le monstre n'est pas mort il riposte
	 * 
	 * @param donjon        : le donjon
	 * @param salleCourante : la piece où se situe le joueur
	 */
	public void attaquer(Salle salleCourante) {
		if (salleCourante != null) {
			int choix;
			IhmSaisie ihmSaisie = new IhmSaisie();
			PersonnageService personnageService = new PersonnageService();
			MonstreService monstreService = new MonstreService();

			if (salleCourante instanceof Piece) {
				List<String> listeMonstre = new ArrayList<>();

				for (Monstre monstre : ((Piece) salleCourante).getListeMonstres()) {
					listeMonstre.add(monstre.toString());
				}
				affichageListes(listeMonstre);
				choix = ihmSaisie.saisieInt(0, listeMonstre.size() - 1);
				effacerConsole();
				personnageService.attaquer(((Piece) salleCourante).getJoueur(),
						((Piece) salleCourante).getListeMonstres().get(choix));

				if (monstreService.monstreMort(((Piece) salleCourante).getListeMonstres().get(choix), salleCourante)) {
					System.out.println("Vous avez tué le monstre");
				} else {
					personnageService.attaquer(((Piece) salleCourante).getListeMonstres().get(choix),
							((Piece) salleCourante).getJoueur());
				}
			}
		}
	}

	/**
	 * Execution de l'action regarder, demande à l'utilsateur quelle piece il veut
	 * voir le détails et affiche sont détails
	 * 
	 * @param donjon        : le donjon pour avoir le details des pieces adjacentes
	 * @param salleCourante : la pièce où se situe le joueur
	 */
	public void regarder(Donjon donjon, Salle salleCourante) {
		if (donjon != null && salleCourante != null) {
			JeuService jeuService = new JeuService();
			SalleService salleService = new SalleService();
			int choix;
			IhmSaisie ihmSaisie = new IhmSaisie();
			List<String> directionSalle = jeuService.deplacementPossibleJoueur((Piece) salleCourante);
			directionSalle.add("Salle Courante");
			affichageListes(directionSalle);
			choix = ihmSaisie.saisieInt(0, directionSalle.size() - 1);
			effacerConsole();
			System.out.println(salleService.regarderSalle(donjon, (Piece) salleCourante, directionSalle.get(choix)));
		}
	}

	/**
	 * Fonction génerique permettant l'affichage de listes de choix pour l'ihm du
	 * joueur
	 * 
	 * @param list
	 */
	public void affichageListes(List<String> list) {
		int compteur = 0;
		System.out.println("Que souhaitez vous faire ?");
		for (String element : list) {
			System.out.println(compteur + " : " + element);
			compteur++;
		}

	}

	/**
	 * methode de creation d'un joueur, demande à l'utilisateur de saisir son nom
	 * 
	 * @return : le nom du joueur
	 */
	public String creationJoueur() {
		IhmSaisie ihmSaisie = new IhmSaisie();
		System.out.println("Entrer le nom du joueur : ");
		return ihmSaisie.SaisieNom();
	}

	/**
	 * Affichage du top 10 des scores de tous les joueurs
	 */
	public void affichageTop10() {
		JeuService jeuService = new JeuService();
		List<Score> topScores = jeuService.topScores(10, System.getenv("DONJON"));
		int topScore = 0;
		StringBuilder builder = new StringBuilder();

		builder.append("*************** Top Scores *************\n");

		builder.append(String.format("%-2s", "") + "| " + String.format("%-8s", "SCORE") + "| "
				+ String.format("%-10s", "NOM") + "| " + "DATE" + "\n");
		builder.append("----------------------------------------\n");
		for (Score score : topScores) {
			builder.append(String.format("%-2s", ++topScore) + "| " + String.format("%-8s", score.getScore()) + "| "
					+ String.format("%-10s", score.getNom()) + "| " + score.getDate() + "\n");
		}
		builder.append("****************************************\n");
		System.out.println(builder);
	}
/**
 * Fonction permettant d'effacer la console apès chaque action, et d'afficher ce qui a été choisi par l'utilisateur
 */
	public void effacerConsole() {
		try {
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		} catch (InterruptedException e) {
			System.out.println("Effacement console echouée");
		} catch (IOException e) {
			System.out.println("Effacement console echouée");
		}
	}
}
