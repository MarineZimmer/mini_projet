package fr.afpa.donjonnicosammarine.ientites;

import fr.afpa.donjonnicosammarine.entites.Personnage;

public interface IUtiliser {
	/**
	 * Utilisation d'un objet, augmente les points correspondant à l'objet
	 * @param personnage : le Personnage qui utilse
	 * @return true si l'objet a été utilisé
	 */
boolean utiliserObjet( Personnage personnage);

}
