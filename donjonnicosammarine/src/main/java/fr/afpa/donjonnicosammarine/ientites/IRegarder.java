package fr.afpa.donjonnicosammarine.ientites;

public interface IRegarder {
	/**
	 * afiche les informations détaillée de la classe regardée
	 * 
	 * @return : une chaine de caractère représentant les information de la classe
	 */
	String regarder();

}
