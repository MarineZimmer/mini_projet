package fr.afpa.donjonnicosammarine.iservices;

import fr.afpa.donjonnicosammarine.entites.Donjon;
import fr.afpa.donjonnicosammarine.entites.Salle;

public interface IDeplacer {

	/**
	 * service de deplacement d'un joueur
	 * 
	 * @param donjon        : le donjon
	 * @param salleCourante : la salle où se situe le joueur
	 * @param deplacement   : le deplacement effectué
	 * @return la nouvelle salle où se situe le joueur
	 */
	Salle deplacer(Donjon donjon, Salle salleCourante, String deplacement);
}
